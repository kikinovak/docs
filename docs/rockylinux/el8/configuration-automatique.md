
# Configuration automatique d'un serveur Rocky&nbsp;Linux&nbsp;8

Installer Git si ce n'est pas déjà fait&nbsp;:

```
# dnf install -y git
```

Récupérer mon script de configuration post-installation&nbsp;:

```
# git clone https://gitlab.com/kikinovak/rocky-8-server
# cd rocky-8-server/
```

Configurer Bash et Vim et passer le système en anglais&nbsp;:

```
# ./rocky-setup.sh --shell
```

Configurer les dépôts de paquets officiels et tiers&nbsp;:

```
# ./rocky-setup.sh --repos
```

Activer DeltaRPM et mettre à jour le système&nbsp;:

```
# ./rocky-setup.sh --fresh
```

Installer les groupes de paquets `Core` et `Base` et une panoplie d'outils
courants&nbsp;:

```
# ./rocky-setup.sh --extra
```

Permettre à l'utilisateur initial d'afficher les logs&nbsp;:

```
# ./rocky-setup.sh --logs
```

Désactiver l'IPv6 et reconfigurer les services de base en conséquence&nbsp;:

```
# ./rocky-setup.sh --ipv4
```

Activer la persistance du mot de passe pour `sudo`&nbsp;:

``` 
# ./rocky-setup.sh --sudo
``` 

Effectuer toutes les tâches ci-dessus d'une traite&nbsp;:

```
# ./rocky-setup.sh --setup
```

Afficher tous les paquets qui ne font pas partie du système de base
amélioré&nbsp;:

```
# ./rocky-setup.sh --reset
```

Réactiver l'IPv6 en cas de besoin&nbsp;:

```
# ./rocky-setup.sh --ipv6
```

Afficher l'aide en ligne de la commande&nbsp;:

```
# ./rocky-setup.sh --help
```

Afficher le détail des opérations dans un deuxième terminal&nbsp;:

```
# tail -f /tmp/rocky-setup.log
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
