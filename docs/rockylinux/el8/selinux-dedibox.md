
# Activer SELinux sur un serveur Scaleway Dedibox

SELinux est désactivé dans la configuration par défaut chez Scaleway.

Dans un premier temps, activer SELinux en mode permissif&nbsp;:

```
# /etc/selinux/config
SELINUX=permissive
SELINUXTYPE=targeted
```

Procéder à un réétiquetage du système de fichiers&nbsp;:

```
# touch /.autorelabel
# reboot && exit
```

> Compter cinq bonnes minutes pour un réétiquetage complet.

Lancer un audit SELinux&nbsp;:

```
# sealert -a /var/log/audit/audit.log
100% done
found 0 alerts in /var/log/audit/audit.log
```

Passer en mode renforcé par défaut&nbsp;:

```
# /etc/selinux/config
SELINUX=enforcing
SELINUXTYPE=targeted
```

Activer le mode renforcé&nbsp;:

```
# setenforce 1
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

