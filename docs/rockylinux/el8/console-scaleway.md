
# Lancer la console de secours Scaleway

Sur un [serveur dédié Scaleway](https://www.scaleway.com/fr/dedibox), on pourra
démarrer le système de secours&nbsp;:

* Se connecter à la [console Online](https://console.online.net).

* Ouvrir le menu `Serveur` > `Liste des serveurs`.

* Sélectionner la machine > `Administrer` > `Secours`.

* `Sélectionner un système d'exploitation` > `Ubuntu 18.04`.

* Cliquer sur `Lancer le système de secours sur votre Dedibox`.

* Attendre cinq minutes.

Ouvrir une connexion SSH avec les paramètres affichés&nbsp;:

```
$ ssh microlinux@51.158.146.161
```

Devenir root&nbsp;:

```
$ sudo -s
```

Une fois que l'intervention est terminée, cliquer sur `Repasser en mode normal`.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
