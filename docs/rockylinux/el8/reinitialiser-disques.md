
# Réinitialiser un ou plusieurs disques durs

Démarrer la [console de secours](console-de-secours.md).

Afficher les disques et les partitions&nbsp;:

```
# lsblk
```

Désactiver les assemblages RAID provenant d'une installation antérieure&nbsp;:

```
# mdadm --stop --scan
```

Effacer les métadonnées RAID persistantes sur les partitions&nbsp;:

```
# mdadm --zero-superblock /dev/sda1
# mdadm --zero-superblock /dev/sda2
# mdadm --zero-superblock /dev/sda3
# mdadm --zero-superblock /dev/sdb1
# mdadm --zero-superblock /dev/sdb2
# mdadm --zero-superblock /dev/sdb3
...
```

Supprimer les tables de partitions&nbsp;:

```
# sgdisk --zap-all /dev/sda
# sgdisk --zap-all /dev/sdb
...
```

Effacer les signatures des systèmes de fichiers&nbsp;:

```
# wipefs --all /dev/sda
# wipefs --all /dev/sdb
...
```

Détruire les données d'un disque conformément au standard *5220-22M US Dept. of
Defense*&nbsp;:

```
# shred -vzn 8 /dev/sda
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
