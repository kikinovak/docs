
# Postfix minimal sur un serveur dédié sous Rocky&nbsp;Linux&nbsp;8

Vérifier si le serveur n'est pas blacklisté quelque part&nbsp;:

* [https://mxtoolbox.com/blacklists.aspx](https://mxtoolbox.com/blacklists.aspx)

Vérifier si Postfix et la commande `mail` sont installés&nbsp;:

```
# rpm -q postfix mailx
postfix-3.5.8-4.el8.x86_64
mailx-12.5-29.el8.x86_64
```

Éditer le fichier `/etc/postfix/main.cf`&nbsp;:

```
# /etc/postfix/main.cf
# 
# Minimal Postfix configuration for Internet-facing servers

# Disable backwards compatibility
compatibility_level = 2

# Disable IPv6
inet_protocols = ipv4

# Outbound mail only
inet_interfaces = localhost
mailbox_size_limit = 0

# Host
myhostname = sd-155842.dedibox.fr

# Domain
mydomain = dedibox.fr

# Authorize local machine only
mynetworks = 127.0.0.0/8

# Local aliasing
alias_maps = hash:/etc/aliases

# Debugging
debugger_command =
  PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
  ddd $daemon_directory/$process_name $process_id & sleep 5

# Command paths
sendmail_path = /usr/sbin/sendmail.postfix
newaliases_path = /usr/bin/newaliases.postfix
mailq_path = /usr/bin/mailq.postfix

# Documentation
manpage_directory = /usr/share/man
sample_directory = /usr/share/doc/postfix/samples
readme_directory = /usr/share/doc/postfix/README_FILES
```

Éditer la table de correspondance `/etc/aliases`&nbsp;:

```
# Person who should get root's mail
root: info@microlinux.fr
```

Construire le fichier indexé&nbsp;:

```
# newaliases
```

Activer et démarrer Postfix&nbsp;:

```
# systemctl enable postfix --now
```

Envoyer un mail de test vers un compte externe&nbsp;:

```
# mail info@microlinux.fr
Subject: Test Postfix
Ceci est un test.
.
EOT
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


