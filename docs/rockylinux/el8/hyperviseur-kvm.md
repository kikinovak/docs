
# Installer un hyperviseur KVM sous Rocky&nbsp;Linux&nbsp;8

## Prérequis

Vérifier si le processeur supporte la virtualisation matérielle&nbsp;:

```
# grep -E 'svm|vmx' /proc/cpuinfo
flags : ... vmx ...
```

Alternativement&nbsp; :

```
# lscpu | grep Virtual
Virtualization:      VT-x
```


## Installation

Installer KVM et les outils correspondants&nbsp;:

```
# dnf install -y qemu-kvm libvirt virt-install bridge-utils
```


## Mise en service

Vérifier si les modules KVM sont chargés&nbsp; :

```
# lsmod | grep kvm
kvm_intel             339968  0
kvm                   905216  1 kvm_intel
...
```

Lancer les services `libvirtd` et `libvirt-guests`&nbsp;:

```
# systemctl enable libvirtd --now
# systemctl enable libvirt-guests --now
```


## Configuration d'un bridge

Afficher la configuration réseau et noter les paramètres&nbsp;:

```
# ip --brief addr show
lo               UNKNOWN        127.0.0.1/8
enp1s0           UP             51.158.146.161/24
virbr0           DOWN           192.168.122.1/24

# ip route show
default via 51.158.146 ...
```

> L'interface réseau de l'hôte est `enp1s0` dans l'exemple. Bien évidemment, il
> faudra adapter la configuration en conséquence.

Lancer NetworkManager TUI&nbsp;:

```
# nmtui
```

Configurer le *bridge* `br0`&nbsp;:

* Sélectionner `Edit a connection`.

* `Add` > `Bridge` > `Create`

* `Profile name` : `BRIDGE`

* `Device` : `br0`

* `Bridge Slaves` > `Add`

* `Slave connection` > `Ethernet` > `Create`

    - `Profile name` : `WAN` (ou `LAN` pour une machine locale)

    - `Device` : `enp1s0`

    - Confirmer par `OK`.

* Décocher `Enable STP (Spanning Tree Protocol)`.

* Passer `IPv4 CONFIGURATION` de `Automatic` à `Manual`.

* Cliquer sur Show pour afficher les détails.

* Fournir l'adresse IP du serveur en notation CIDR, par exemple
  `51.158.146.161/24`.

* Renseigner l'adresse IP de la passerelle dans le champ `Gateway`.

* Ne rien indiquer dans les champs `DNS server` et `Search domains`.

* Passer `IPv6 CONFIGURATION` de `Automatic` à `Ignore`.

* Confirmer par `OK`.

* Revenir à la fenêtre principale.

* Sélectionner `Activate a connection`.

* Les connexions doivent être actives (`*`).

* Revenir à la fenêtre principale.

* Quitter NetworkManager TUI.

Vérifier la configuration du *bridge*&nbsp;:

```
# ip --brief addr show
lo               UNKNOWN        127.0.0.1/8
enp1s0           UP             51.158.146.161/24
virbr0           DOWN           192.168.122.1/24
br0              DOWN           51.158.146.161/24
```

Si l'interface `br0` dispose bien de sa propre adresse IP, on peut retourner
dans NetworkManagerTUI et supprimer toutes les autres connexions comme `Wired
connection 1`, `System eth0` ou `virbr0`.

Au final, on doit obtenir quelque chose comme ceci&nbsp;:

```
# nmcli con show
NAME    UUID                                  TYPE      DEVICE
BRIDGE  8b995c87-6208-48e7-a9fe-5ebe110a469f  bridge    br0
WAN     46c392d8-8a09-4571-8ca7-ceeb30b6de21  ethernet  enp1s0

# ip --brief addr show
lo               UNKNOWN        127.0.0.1/8
enp1s0           UP
virbr0           DOWN
br0              UP             51.158.146.161/24
```

> Lors de la première connexion avec Virtual Machine Manager, on pourra
> supprimer le réseau `default` (`virbr0`).

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
