
# Mises à jour automatiques avec DNF Automatic sous Rocky&nbsp;Linux&nbsp;8

Installer DNF Automatic&nbsp;:

```
# dnf install -y dnf-automatic
```

Sauvegarder le fichier de configuration par défaut&nbsp;:

```
# cd /etc/dnf/
# cp -v automatic.conf automatic.conf.orig
'automatic.conf' -> 'automatic.conf.orig'
```

Adapter la configuration suivante&nbsp;:

```
# /etc/dnf/automatic.conf
  
[commands]
upgrade_type = default
random_sleep = 0
network_online_timeout = 60
download_updates = yes
apply_updates = yes

[emitters]
system_name = sd-155842.dedibox.fr
emit_via = email

[email]
email_from = root@localhost
email_to = info@microlinux.fr
email_host = localhost
```

> Le fichier `automatic.conf` d'origine est amplement commenté.

Activer le service&nbsp;:

```
# systemctl enable dnf-automatic.timer --now
```

Afficher les infos&nbsp;:

```
# systemctl list-timers dnf-*
```

Tester manuellement&nbsp;:

```
# /usr/bin/dnf-automatic /etc/dnf/automatic.conf --timer
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


