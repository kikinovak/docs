
# Protéger SSH avec Fail2ban sous Rocky&nbsp;Linux&nbsp;8

Installer Fail2ban&nbsp;:

```
# dnf install -y fail2ban-server fail2ban-firewalld
```

Créer un fichier `/etc/fail2ban/jail.d/custom.conf` pour protéger le service SSH
contre les attaques par force brute&nbsp;:

```
# /etc/fail2ban/jail.d/custom.conf

[DEFAULT]
bantime = 24h
ignoreip = 88.161.127.222

[sshd]
enabled = true
```

Éventuellement, créer une exception pour plusieurs adresses IP&nbsp;:

```
ignoreip = 78.197.22.147 163.172.63.88
```

Activer et démarrer le service&nbsp;:

```
# systemctl enable fail2ban --now
```

Afficher les adresses IP bannies&nbsp;:

```
# fail2ban-client status sshd
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


