
# Monter un disque RAID

Après la défaillance matérielle d'un disque dur, on peut se retrouver face à un
système qui ne démarre plus et une grappe RAID incomplète constituée d'un ou
plusieurs disques. 

Démarrer la [console de secours](console-de-secours.md) et identifier le ou les
disques&nbsp;:

```
# lsblk
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda           8:0    0 931.5G  0 disk
|-sda1        8:1    0     8G  0 part
|-sda2        8:2    0   501M  0 part
`-sda3        8:3    0   923G  0 part
sdb           8:16   1   3.6G  0 disk
|-sdb1        8:17   1   973M  0 part
`-sdb2        8:18   1   8.6M  0 part
...
```

Le disque `sda` apparaît bien comme membre d'une grappe RAID&nbsp;:

```
# fdisk -l /dev/sda

Disk /dev/sda: 1000.2 GB, 1000204886016 bytes, 1953525168 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disk label type: dos
Disk identifier: 0x000281ab

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1            2048    16795647     8396800   fd  Linux raid autodetect
/dev/sda2   *    16795648    17821695      513024   fd  Linux raid autodetect
/dev/sda3        17821696  1953523711   967851008   fd  Linux raid autodetect
```

> Les partitions de type RAID ne peuvent pas être montées directement. 

Créer les assemblages RAID&nbsp;:

```
# mdadm --assemble --run /dev/md1 /dev/sda1
mdadm: /dev/md1 has been started with 1 drive (out of 2).
# mdadm --assemble --run /dev/md2 /dev/sda2
mdadm: /dev/md2 has been started with 1 drive (out of 2).
# mdadm --assemble --run /dev/md3 /dev/sda3
mdadm: /dev/md3 has been started with 1 drive (out of 2).
```

Afficher les assemblages RAID&nbsp;:

```
# cat /proc/mdstat
Personalities : [raid0] [raid1] [raid6] [raid5] [raid4] [raid10] [linear]
md3 : active raid1 sda3[0]
      967719936 blocks super 1.2 [2/1] [U_]
      bitmap: 2/8 pages [8KB], 65536KB chunk

md2 : active raid1 sda2[0]
      512704 blocks super 1.2 [2/1] [U_]
      bitmap: 0/1 pages [0KB], 65536KB chunk

md1 : active raid1 sda1[0]
      8388608 blocks super 1.2 [2/1] [U_]

unused devices: <none>
```

À partir de là, on peut monter les partitions individuelles&nbsp;:

```
# mount -v /dev/md3 /mnt/
mount: /dev/md3 mounted on /mnt.
# mount -v /dev/md2 /mnt/boot/
mount: /dev/md2 mounted on /mnt/boot.
```

Une fois qu'elles sont montées, on peut lancer un [`chroot`](chroot-secours.md)
depuis le système de secours.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
