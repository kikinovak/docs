
# Installer Rocky Linux 8 sur un serveur HP&nbsp;Proliant&nbsp;ML110&nbsp;Gen9

## Restaurer les paramètres UEFI

Allumer le serveur.

Lorsque l'écran d'accueil de l'UEFI s'affiche, appuyer sur ++f9++ et attendre
que l'UEFI termine de charger.

`System Configuration` > `BIOS/Platform Configuration (RBSU)` > `System Default
Options` > `Restore Default System Settings`&nbsp;:

* Appuyer deux fois sur ++enter++

* Sélectionner `Yes, restore the default settings`

* Confirmer par ++enter++


## Configurer un assemblage RAID 1

Appuyer sur ++f9++ à l'écran d'accueil de l'UEFI.

`System Configuration` > `Smart Array P440 Controller` > `Exit and launch HP
Smart Storage Administrator (HPSSA)` > `HP Smart Storage Administrator`.

`Smart Array P440` > `Configure` > `Clear Configuration` > `Clear` > `Finish`.

`Create Array` > `Select Physical Drives for the New Array` > `Select All` >
`Create Array`&nbsp;:

* `RAID Level`&nbsp;: `RAID 1`

* `Strip Size / Full Stripe Size`&nbsp;: `256 KiB / 256 KiB`

* `Sectors / Track`&nbsp;: `32`

* `Size`&nbsp;: `Maximum Size`

Confirmer par `Create Logical Drive`.

> Ignorer l'avertissement `The logical drive full stripe size exceeds the
> transformation capabilities of this controller`.

Confirmer par `Yes` et `Finish`.

Quitter HPSSA et redémarrer.


## Installer Rocky Linux 8

Appuyer sur ++f11++ à l'écran d'accueil de l'UEFI.

Sélectionner la clé USB et démarrer dessus.

L'assemblage RAID apparaît comme un disque logique `/dev/sda` qu'on pourra
partitionner comme ceci&nbsp;:

* `/boot`&nbsp;: 500 MiB `ext4` (label&nbsp;: `boot`)

* `/boot/efi`&nbsp;: 200 MiB (label&nbsp;: `EFI`)

* `swap`&nbsp;: 2 GiB (label&nbsp;: `swap`)

* partition principale&nbsp;: espace disponible en `ext4` (label&nbsp;: `root`)

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
