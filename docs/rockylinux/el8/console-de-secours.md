
# Lancer la console de secours sous Rocky&nbsp;Linux&nbsp;8

* Démarrer sur la clé USB ou le DVD.

* Au démarrage, sélectionner `Troubleshooting`.

* Mettre l'entrée `Rescue a Rocky Linux system` en surbrillance.

* Appuyer sur ++tab++ (++e++ sur un système UEFI) et ajouter les options `nomodeset vga=791`.

* Confirmer par ++enter++ (++ctrl+x++ sur un système UEFI) pour démarrer.

* Une fois que le système a démarré, choisir l'option `3) Skip to shell`.

* Basculer vers le clavier suisse romand : `loadkeys fr_CH-latin1`.

Afficher les interfaces réseau reconnues :

```
# nmcli dev
DEVICE  TYPE     STATE        CONNECTION
enp3s0  ethernet disconnected --
lo      loopback unmanaged    --
```

Activer l'interface réseau en question, par exemple :

```
# nmcli con up enp3s0
```

Lancer un serveur SSH

```
# cd /etc/ssh
# cp sshd_config.anaconda sshd_config
# systemctl start sshd
```

La connexion se fait sans mot de passe.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
