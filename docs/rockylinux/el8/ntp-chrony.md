
# Synchronisation NTP avec Chrony sous Rocky&nbsp;Linux&nbsp;8

La suite Chrony est utilisée comme outil de gestion NTP par défaut depuis Red
Hat Enterprise Linux 7.0.

Chrony est composé de&nbsp;:

* `chronyd` – un démon exécuté dans l’espace utilisateur

* `chronyc` – un outil en ligne de commande pour gérer `chronyd`

Effectuer une première synchronisation manuelle si la machine est complètement
déréglée&nbsp;:

```
# systemctl stop chronyd
# chronyd -q 'pool pool.ntp.org iburst'
# systemctl start chronyd
```

> Certaines applications digèrent très mal les modifications abruptes de
> l'horloge système. Ce n'est pas une mauvaise idée de redémarrer la machine
> après une telle opération.

Configurer Chrony&nbsp;:

```
# /etc/chrony.conf
server 0.fr.pool.ntp.org iburst
server 1.fr.pool.ntp.org iburst
server 2.fr.pool.ntp.org iburst
server 3.fr.pool.ntp.org iburst
driftfile /var/lib/chrony/drift
makestep 1.0 3
rtcsync
logdir /var/log/chrony
```

* Le paramètre `iburst` réduit l'intervalle entre les quatre premières requêtes
  envoyées au serveur spécifié à moins de deux secondes, ce qui permet à Chrony
  de synchroniser l'horloge immédiatement après le lancement.

* Le fichier spécifié par la directive `driftfile` enregistre les dérives de
  l'horloge.

* En temps normal, Chrony corrige peu à peu tout décalage horaire, en
  ralentissant ou en accélérant l'horloge selon les besoins. Dans certaines
  situations, l'horloge système peut avoir dérivé au point où le processus de
  correction risque de prendre un long moment avant de corriger l'horloge
  système. Ici, la directive `makestep 1.0 3` force Chrony à modifier l'horloge
  système si l'ajustement était plus important qu'une seconde (`1.0`), mais
  seulement lors des trois (`3`) premières mises à jour horaires. Une valeur
  négative peut être utilisée pour désactiver cette limite.

* La directive `rtcsync` active la synchronisation de l'horloge système avec
  l'horloge RTC (*Real-Time Clock*).

* Quant à la directive `logdir`, elle spécifie le répertoire où les fichiers
  journaux sont écrits.

Alternativement, synchroniser la machine avec une source locale&nbsp;:

```
# /etc/chrony.conf
server 192.168.2.1 iburst
driftfile /var/lib/chrony/drift
makestep 1.0 3
rtcsync
logdir /var/log/chrony
```

Éventuellement, autoriser les connexions locales au serveur&nbsp;:

```
# /etc/chrony.conf
server 0.fr.pool.ntp.org iburst
server 1.fr.pool.ntp.org iburst
server 2.fr.pool.ntp.org iburst
server 3.fr.pool.ntp.org iburst
driftfile /var/lib/chrony/drift
makestep 1.0 3
rtcsync
allow 192.168.2.0/24
local stratum 10
logdir /var/log/chrony
```

* La directive `allow` spécifie l'autorisation d'accès pour un hôte ou un
  réseau particulier.

* La directive `local stratum 10` est utilisée pour que Chrony puisse apparaître
  comme synchronisé avec le temps réel du point de vue des interrogations des
  clients, même s'il ne possède pas de source de synchronisation actuellement.

Ouvrir le port 123 en UDP pour autoriser les requêtes des postes clients&nbsp;:

```
# firewall-cmd --permanent --add-service=ntp
# firewall-cmd --reload
```

Prendre en compte les modifications&nbsp;:

```
# systemctl restart chronyd
```

Afficher le suivi de la synchronisation&nbsp;:

```
# chronyc tracking
Reference ID    : 5362C986 (ntp1.mediamatic.nl)
Stratum         : 3
Ref time (UTC)  : Sat Feb 29 08:28:52 2020
System time     : 0.000001028 seconds slow of NTP time
Last offset     : +0.001116573 seconds
RMS offset      : 0.010313899 seconds
Frequency       : 21.756 ppm fast
Residual freq   : +0.000 ppm
Skew            : 0.143 ppm
Root delay      : 0.014591943 seconds
Root dispersion : 0.018667171 seconds
Update interval : 516.2 seconds
Leap status     : Normal
```

* `Reference ID`, c'est la référence hexadécimale ainsi que le nom ou l'adresse
  IP du serveur avec lequel l'ordinateur est actuellement synchronisé.

* `Stratum` indique à combien de sauts d'un ordinateur avec une horloge de
  référence attachée nous nous trouvons.

* Toute erreur dans l'horloge système (`System time`) est corrigée en
  l'accélérant ou en la ralentissant légèrement jusqu'à ce que l'erreur soit
  résolue, puis celle-ci retourne à la vitesse normale.

Afficher la liste des serveurs auxquels on est connecté&nbsp;:

```
# watch chronyc sources
210 Number of sources = 4
MS Name/IP address     Stratum Poll Reach LastRx Last sample
===========================================================================
^- ntp2k.versadns.com        2   6    17    23  -1162us[  +85us] +/-  127ms
^- reinhardt.pointpro.nl     2   6    17    24  +4042us[+5290us] +/-   33ms
^* 46-243-26-34.tangos.nl    1   6    17    23  -2618us[-1371us] +/- 8551us
^+ debmirror.tuxis.net       2   6    17    24  +3501us[+4749us] +/-   20ms
```

* Le petit astérisque `*` en début de ligne signifie que la machine est
  correctement synchronisée avec le serveur distant.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


