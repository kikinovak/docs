
# Partitionner un disque dur sous Rocky&nbsp;Linux&nbsp;8

## BIOS traditionnel  + table de partitions MBR

Utiliser `fdisk` dans la [console de secours](console-de-secours.md) :

```
# fdisk -l /dev/sda
...
Device     Boot   Start       End   Sectors  Size Id Type
/dev/sda1  *       2048   1026047   1024000  500M 83 Linux
/dev/sda2       1026048   9414655   8388608    4G 82 Linux swap / Solaris
/dev/sda3       9414656 125829119 116414464 55.5G 83 Linux
```

Formater la partition `/boot`&nbsp;:

* Sélectionner `/dev/sda1`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `boot`.

* Définir le point de montage `/boot`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `swap`&nbsp;:

* Sélectionner `/dev/sda2`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `swap` et l'étiquette `swap`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition principale&nbsp;:

* Sélectionner `/dev/sda3`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `root`.

* Définir le point de montage `/`.

* Confirmer `Mise à jour des paramètres`, puis `Fait`.

## BIOS traditionnel + table de partitions GPT

Utiliser `gdisk` dans la [console de secours](console-de-secours.md) :

```
# gdisk -l /dev/sda
...
Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048            4095   1024.0 KiB  EF02  BIOS boot partition
   2            4096         1028095   500.0 MiB   8300  Linux filesystem
   3         1028096         9416703   4.0 GiB     8200  Linux swap
   4         9416704       125829086   55.5 GiB    8300  Linux filesystem
```

Configurer la partition d'amorçage BIOS&nbsp;:

* Sélectionner `/dev/sda1`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `BIOS Boot`.

* Confirmer `Mise à jour des paramètres`.

> La partition n'est pas formatée à proprement parler.

Formater la partition `/boot`&nbsp;:

* Sélectionner `/dev/sda2`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `boot`.

* Définir le point de montage `/boot`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `swap`&nbsp;:

* Sélectionner `/dev/sda3`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `swap` et l'étiquette `swap`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition principale&nbsp;:

* Sélectionner `/dev/sda4`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `root`.

* Définir le point de montage `/`.

* Confirmer `Mise à jour des paramètres`, puis `Fait`.

## UEFI + table de partitions GPT

Utiliser `gdisk` dans la [console de secours](console-de-secours.md) :

```
# gdisk -l /dev/sda
...
Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048          411647   200.0 MiB   EF00  EFI System Partition
   2          411648         1435647   500.0 MiB   8300  Linux filesystem
   3         1435648         9824255   4.0 GiB     8200  Linux swap
   4         9824256       125829086   55.3 GiB    8300  Linux filesystem
```

Formater la partition EFI&nbsp;:

* Sélectionner `/dev/sda1`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `EFI System Partition` et l'étiquette `EFI`.

* Définir le point de montage `/boot/efi`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `/boot`&nbsp;:

* Sélectionner `/dev/sda2`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `boot`.

* Définir le point de montage `/boot`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `swap`&nbsp;:

* Sélectionner `/dev/sda3`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `swap` et l'étiquette `swap`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition principale&nbsp;:

* Sélectionner `/dev/sda4`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `root`.

* Définir le point de montage `/`.

* Confirmer `Mise à jour des paramètres`, puis `Terminé`.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
