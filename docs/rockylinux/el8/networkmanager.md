
# Configurer NetworkManager sur un serveur Rocky&nbsp;Linux&nbsp;8

## Serveur LAN

Afficher les interfaces réseau gérées par NetworkManager&nbsp;:

```
# nmcli dev
DEVICE  TYPE      STATE      CONNECTION
enp2s0  ethernet  connected  enp2s0
lo      loopback  unmanaged  --
```

Configurer la connexion&nbsp;:

```
# nmtui
```

* Sélectionner `Edit a connection`.

* Éditer la connexion.

* Remplacer le nom de profil par `LAN`.

* Passer `IPv4 CONFIGURATION` de `Automatic` à `Manual`.

* Cliquer sur `Show` pour afficher les détails.

* Fournir l'adresse IP du serveur en notation CIDR, par exemple
  `192.168.2.3/24`.

* Renseigner l'adresse IP de la passerelle dans le champ `Gateway`.

* Ne rien indiquer dans les champs `DNS server` et `Search domains`.

* Passer `IPv6 CONFIGURATION` de `Automatic` à `Ignore`.

* Confirmer par `OK`.

* Revenir à la fenêtre principale.

* Activer la connexion `LAN`&nbsp;: `Activate a connection` > `LAN`

* Quitter NetworkManager TUI&nbsp;: `Quit`

Renseigner le ou les serveurs DNS&nbsp;:

```
# /etc/resolv.conf
search microlinux.lan
nameserver 192.168.2.1
```

Corriger la configuration du nom d'hôte&nbsp;:

```
# /etc/hosts
127.0.0.1   localhost.localdomain localhost
192.168.2.3 sandbox.microlinux.lan sandbox
```

Le fichier `/etc/hostname` devra juste contenir le nom d'hôte simple sans le
domaine&nbsp;:

```
sandbox
```


## Routeur

Afficher les interfaces réseau gérées par NetworkManager&nbsp;:

```
# nmcli dev
DEVICE  TYPE      STATE         CONNECTION
enp2s0  ethernet  connected     enp2s0
enp3s1  ethernet  disconnected  --
lo      loopback  unmanaged     --
```

Configurer la connexion&nbsp;:

```
# nmtui
```

* Sélectionner `Edit a connection`.

* Éditer la connexion active.

* Remplacer le nom de profil par `WAN`.

* Passer `IPv4 CONFIGURATION` de `Automatic` à `Manual`.

* Cliquer sur `Show` pour afficher les détails.

* Fournir l'adresse IP du serveur en notation CIDR, par exemple `192.168.2.5/24`.

* Renseigner l'adresse IP de la passerelle dans le champ `Gateway`.

* Ne rien indiquer dans les champs `DNS server` et `Search domains`.

* Passer `IPv6 CONFIGURATION` de `Automatic` à `Ignore`.

* Confirmer par `OK`.

* Éditer le profil correspondant à la deuxième interface.

* Remplacer le nom de profil par `LAN`.

* Renseigner la rubrique `Device`, par exemple `enp3s1`.

* Passer `IPv4 CONFIGURATION` de `Automatic` à `Manual`.

* Cliquer sur `Show` pour afficher les détails.

* Fournir l'adresse IP de la deuxième interface, par exemple `192.168.3.1/24`.

* Ne rien indiquer dans les champs `Gateway`, `DNS server` et `Search domains`.

* Passer `IPv6 CONFIGURATION` de `Automatic` à `Ignore`.

* Confirmer par `OK`.

* Revenir à la fenêtre principale.

* Activer les deux connexions `WAN` et `LAN`.

* Quitter NetworkManager TUI&nbsp;: `Quit`

Renseigner le ou les serveurs DNS&nbsp;:

``` 
# /etc/resolv.conf
nameserver 192.168.2.1
``` 

Corriger la configuration du nom d'hôte&nbsp;:

```
# /etc/hosts
127.0.0.1   localhost.localdomain localhost
192.168.3.1 amandine.sandbox.lan amandine
```

Le fichier `/etc/hostname` devra juste contenir le nom d'hôte simple&nbsp;:

```
amandine
```


## Scaleway Dedibox

Afficher les interfaces réseau gérées par NetworkManager&nbsp;:

```
# nmcli dev
DEVICE  TYPE      STATE      CONNECTION
enp1s0  ethernet  connected  Wired connection 1
lo      loopback  unmanaged  --
```

Configurer la connexion&nbsp;:

```
# nmtui
```

* Éditer la connexion existante.

* Remplacer le nom de profil par `WAN`.

* Passer `IPv4 CONFIGURATION` de `Automatic` à `Manual`.

* Cliquer sur `Show` pour afficher les détails.

* Fournir l'adresse IP du serveur en notation CIDR, par exemple `51.158.146.161/24`.

* Renseigner l'adresse IP de la passerelle dans le champ `Gateway`.

* Ne rien indiquer dans les champs `DNS servers` et `Search domains`.

* Passer `IPv6 CONFIGURATION` de `Automatic` à `Ignore`.

* Confirmer par `OK`.

* Revenir à la fenêtre principale.

* Activer la connexion `WAN`&nbsp;: `Activate a connection` > `WAN`.

* Quitter NetworkManager TUI&nbsp;: `Quit`

Renseigner les deux serveurs DNS&nbsp;:

```
# /etc/resolv.conf
nameserver 62.210.16.6
nameserver 62.210.16.7
```

Corriger la configuration du nom d'hôte&nbsp;:

```
# /etc/hosts
127.0.0.1       localhost.localdomain localhost
51.158.146.161  sd-155842.dedibox.fr sd-155842
```

Le fichier `/etc/hostname` devra juste contenir le nom d'hôte simple&nbsp;:

```
sd-155842
```


---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
