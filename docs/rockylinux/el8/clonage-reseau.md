
# Cloner un disque via le réseau

Écrire des zéros sur la partie non utilisée du disque pour réduire la taille de
l'image&nbsp;:

```
# dd if=/dev/zero of=/0bits bs=20M; rm -f /0bits
```
> Sur un système Windows 10, c’est l’utilitaire
> [`nullfile.exe`](http://www.feyrer.de/g4u/nullfile-1.01_64bit.exe) qui permet
> de réduire l’image du disque.

Sauvegarder une machine locale depuis la [console de
secours](console-de-secours.md)&nbsp;:

```
# dd if=/dev/sdb status=progress | gzip --fast - | \
  ssh microlinux@nestor dd of=rocky-8-server-image.gz
```

* `status=progress` affiche la progression pour `dd`.

* `--fast` spécifie un algorithme de compression plus rapide pour `gzip`.

Restaurer cette machine&nbsp;:

```
# ssh microlinux@nestor dd if=rocky-8-server-image.gz | \
  gunzip --fast - | dd of=/dev/sdb status=progress
```

Sauvegarder un serveur dédié depuis une [session de
secours](console-scaleway.md)&nbsp;:

```
# dd if=/dev/sda status=progress | gzip --fast - | \
  ssh microlinux@backup.microlinux.fr dd of=rocky-8-server-image.gz
```

Restaurer cette machine&nbsp;:

```
# ssh microlinux@backup.microlinux.fr dd if=rocky-8-server-image.gz | \
  gunzip --fast - | dd of=/dev/sda status=progress
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
