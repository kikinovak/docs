
# Installer Rocky Linux 8 sur un serveur

Télécharger l'ISO&nbsp;:

* Aller sur la [page du projet](https://rockylinux.org).

* Suivre le lien [*Download*](https://rockylinux.org/download).

* Repérer l'architecture `x86_64`.

* Télécharger l'ISO [Minimal](https://download.rockylinux.org/pub/rocky/8/isos/x86_64/Rocky-8.6-x86_64-minimal.iso).

L'image ISO est hybride et peut s'écrire directement sur une clé USB&nbsp;:

```
# dd if=Rocky-8.6-x86-64-minimal.iso of=/dev/sdX bs=512k status=progress
```

> Si l'on installe un système virtualisé dans VirtualBox, ajouter le paramètre
> de démarrage `vga=791` pour augmenter la résolution de l'installateur.

Dans l'écran de bienvenue, sélectionner la langue (`Français`) et la localisation
(`Français` > `France`).

> La disposition du clavier `AZERTY` par défaut pourra éventuellement être
> modifiée par le biais de l'écran principal de l'installateur.

Configurer le réseau et le nom d'hôte&nbsp;:

* Activer le réseau.

* Vérifier l'obtention d'une adresse IP dans le réseau local.

* Choisir un nom d'hôte simple en remplacement de `localhost.localdomain` par
  défaut.

Configurer une adresse IP fixe&nbsp;:

* Cliquer sur `Configurer`.

* Ouvrir l'onglet `Ethernet`.

* Sélectionner le `Périphérique`, par exemple `enp0s3 (08:00:27:00:00:02)`.

* Ouvrir l'onglet `Paramètres IPv4`.

* Passer la `Méthode` de `Automatique` à `Manuel`.

* Cliquer sur `Ajouter` et renseigner l'`Adresse`, le `Masque de réseau` et la
  `Passerelle`.

* Renseigner un ou plusieurs `Serveurs DNS`.

* Cliquer sur `Enregistrer`.

Configurer l'installation depuis le réseau&nbsp;:

* Cliquer sur `Source d'Installation`.

* Utiliser la source `Sur le réseau`.

* Indiquer la source `http://dl.rockylinux.org/pub/rocky/8/BaseOS/x86_64/os`.

* Type d'URL&nbsp;: `URL du dépôt`.

* Cliquer sur `Fait`.

* Vérifier si la `Sélection de Logiciels` est actualisée correctement.

* Sélectionner `Installation minimale`.

Configurer le fuseau horaire (`Europe/Paris`).

Kdump est un mécanisme de capture lors du plantage d'un noyau. Il peut être
désactivé.

Procéder au partitionnement et au formatage&nbsp;:

* [Partitionner un disque](partitionnement-simple.md)

* [RAID 1 avec deux disques](partitionnement-raid1.md)

* [RAID 6 avec quatre disques](partitionnement-raid6.md)

Choix des paquets&nbsp;:

* Opter pour `Installation minimale`.

Configurer les utilisateurs&nbsp;:

* Définir le mot de passe `root`.

* Créer un utilisateur normal, par exemple `microlinux`.

* Cocher la case `Faire de cet utilisateur un administrateur`. 

> L'utilisateur sera ajouté au groupe `wheel` et pourra se servir de la commande
> `sudo`.

Configurer l'authentification par clé SSH&nbsp;:

```
$ ssh-copy-id -i .ssh/id_rsa.pub root@sandbox.microlinux.lan
$ ssh-copy-id -i .ssh/id_rsa.pub microlinux@sandbox.microlinux.lan
```

Supprimer le fichier Kickstart&nbsp;:

```
# rm -f anaconda-ks.cfg
```

Accélérer la synchronisation inititale de la grappe RAID&nbsp;:

```
# echo 50000 > /proc/sys/dev/raid/speed_limit_min
```

Configuration post-installation&nbsp;:

* [Configuration automatique](configuration-automatique.md)

* [Configurer NetworkManager](networkmanager.md)

* [Configurer FirewallD](firewalld.md)

* [Synchronisation NTP](ntp-chrony.md)

* [Postfix minimal](postfix-minimal-lan.md)

* [Mises à jour automatiques](dnf-automatic.md)

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
