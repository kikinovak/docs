
# Postfix minimal sur un serveur LAN sous Rocky&nbsp;Linux&nbsp;8

Vérifier si Postfix et la commande `mail` sont installés&nbsp;:

```
# rpm -q postfix mailx
postfix-3.5.8-4.el8.x86_64
mailx-12.5-29.el8.x86_64
```

Éditer `/etc/postfix/main.cf` en adaptant la configuration&nbsp;:

```
# /etc/postfix/main.cf
#
# Minimal Postfix configuration for LAN servers

# Disable backwards compatibility
compatibility_level = 2

# Disable IPv6
inet_protocols = ipv4

# Outbound mail only
inet_interfaces = localhost
mailbox_size_limit = 0

# Host
myhostname = sandbox.microlinux.lan

# Domain
mydomain = microlinux.lan

# Authorize local machine only
mynetworks = 127.0.0.0/8

# Deliver via Microlinux
relayhost = mail.microlinux.fr:465
smtp_sasl_auth_enable = yes
smtpd_sasl_auth_enable = yes
smtp_sasl_security_options = noanonymous
smtp_sasl_tls_security_options = noanonymous
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_use_tls = yes
smtp_tls_wrappermode = yes
smtp_tls_security_level = encrypt
smtp_tls_note_starttls_offer = yes
smtp_sasl_mechanism_filter = login, plain

# Local aliasing
alias_maps = hash:/etc/aliases

# Debugging
debugger_command =
  PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
  ddd $daemon_directory/$process_name $process_id & sleep 5

# Command paths
sendmail_path = /usr/sbin/sendmail.postfix
newaliases_path = /usr/bin/newaliases.postfix
mailq_path = /usr/bin/mailq.postfix

# Documentation
manpage_directory = /usr/share/man
```

Renseigner les paramètres de connexion dans `/etc/postfix/sasl_passwd`&nbsp;:

```
mail.microlinux.fr:465 login:password
```

Générer le fichier `sasl_passwd.db`&nbsp;:

```
# postmap sasl_passwd
```

Supprimer le fichier `sasl_passwd` et restreindre les permissions du fichier
`sasl_passwd.db`&nbsp;:

```
# rm sasl_passwd
# chmod 0400 sasl_passwd.db
```

Éditer la table de correspondance&nbsp;:

```
# /etc/aliases
...
# Person who should get root's mail
root: info@microlinux.fr
```

Construire le fichier indexé&nbsp;:

```
# newaliases
```

Prendre en compte les modifications&nbsp;:

```
# systemctl restart postfix
```

Envoyer un mail de test&nbsp;:

```
$ mail info@microlinux.fr
Subject: Test Postfix
Ceci est un test.
.
EOT
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
