
# Installer Rocky Linux 8 sur un serveur dédié Scaleway Dedibox

Procéder au choix de la machine et du système d'exploitation&nbsp;:

* Se connecter à la [console Online](https://console.online.net).

* Ouvrir le menu `Serveur` > `Liste des serveurs`.

* Sélectionner la machine > `Administrer` > `Installer`.

* `Distributions serveur` > `Rocky` > `Rocky 8 64bits` > `Installer Rocky`.

Modifier le schéma de partitionnement proposé par défaut&nbsp;:

* Réduire la taille de la partition principale pour avoir un peu de marge.

* Augmenter la taille de la partition d'échange en fonction de la RAM
  disponible.

* Remplir l'espace disponible pour la partition principale.

> Alternativement, on peut très bien accepter le schéma de partitionnement par
> défaut.

Définir les utilisateurs&nbsp;:

* Choisir le mot de passe `root`.

* Définir un utilisateur normal, par exemple `microlinux`.

* Choisir un mot de passe pour cet utilisateur.

> L'installateur impose une limite de 15 caractères alphanumériques.

L'interface affiche un récapitulatif des paramètres réseau de la machine&nbsp;:

* Nom d'hôte&nbsp;: `sd-155842`

* Adresse IP&nbsp;: `51.158.146.161`

* Masque de sous-réseau&nbsp;: `255.255.255.0`

* IP de la passerelle&nbsp;: `51.158.146.1`

* DNS primaire&nbsp;: `62.210.16.6`

* DNS secondaire&nbsp;: `62.210.16.7`

Cliquer sur `Effacer l'intégralité de mes disques durs` pour procéder à
l'installation.

> L'installation du système initial dure entre une demi-heure et une heure. 

Configurer l'authentification par clé SSH&nbsp;:

```
$ ssh-copy-id -i .ssh/id_rsa.pub root@sd-155842.dedibox.fr
$ ssh-copy-id -i .ssh/id_rsa.pub microlinux@sd-155842.dedibox.fr
```

Supprimer les fichiers Kickstart&nbsp;:

```
# rm -f anaconda-ks.cfg original-ks.cfg
```

L'utilisateur initial créé par l'installateur ne peut pas utiliser la commande
`sudo`&nbsp;:

```
# usermod -aG wheel microlinux
```

Redéfinir les mots de passe pour l'utilisateur initial et pour `root`&nbsp;:

```
# passwd
# passwd microlinux
```

Configuration post-installation&nbsp;:

* [Configuration automatique](configuration-automatique.md)

* [Activer SELinux](selinux-dedibox.md)

* [Configurer NetworkManager](networkmanager.md)

* [Configurer FirewallD](firewalld.md)

* [Synchronisation NTP](ntp-chrony.md)

* [Postfix minimal](postfix-minimal-dedibox.md)

* [Protéger SSH avec Fail2ban](fail2ban-ssh.md)

* [Mises à jour automatiques](dnf-automatic.md)

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
