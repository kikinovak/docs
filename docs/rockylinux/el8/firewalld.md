
# Configurer FirewallD sur un serveur Rocky&nbsp;Linux&nbsp;8

## Serveur LAN

Le serveur dispose d'une interface réseau `enp3s0` associée à la zone
`public`&nbsp;:

```
# nmcli con show
NAME  UUID                                  TYPE      DEVICE
LAN   3c01cf84-8ef3-4475-9702-4a2078c7eab7  ethernet  enp3s0
# firewall-cmd --get-active-zones
public
  interfaces: enp2s0
```

Définir la zone appropriée `internal` pour l'interface&nbsp;:

```
# firewall-cmd --permanent --zone=internal --change-interface=enp3s0
# firewall-cmd --set-default-zone=internal
```

Supprimer quelques services prédéfinis&nbsp;:

```
# firewall-cmd --permanent --remove-service=cockpit
# firewall-cmd --permanent --remove-service=dhcpv6-client
# firewall-cmd --permanent --remove-service=mdns
# firewall-cmd --permanent --remove-service=samba-client
# firewall-cmd --reload
```


## Routeur

Le serveur dispose de deux interfaces `enp2s0` (`WAN`) et `enp3s1` (`LAN`)
associées à la zone `public`&nbsp;:

```
# nmcli con show
NAME  UUID                                  TYPE      DEVICE 
WAN   a46043b5-c71d-3795-9548-96e5c136f942  ethernet  enp2s0 
LAN   9667c35b-5f84-4de8-9300-f4144a6db1c6  ethernet  enp3s1
# firewall-cmd --get-active-zones
public
  interfaces: enp2s0 enp3s1
```

Associer la zone `external` à l'interface `enp2s0`&nbsp;:

```
# firewall-cmd --permanent --zone=external --change-interface=enp2s0
```

Associer la zone `internal` à l'interface `enp3s1`&nbsp;:

```
# firewall-cmd --permanent --zone=internal --change-interface=enp3s1
```

Définir la zone `internal` comme zone par défaut&nbsp;:

```
# firewall-cmd --set-default-zone=internal
```

Activer le relais des paquets&nbsp;:

```
# firewall-cmd --permanent --add-masquerade
```

Supprimer quelques services prédéfinis&nbsp;:

```
# firewall-cmd --permanent --remove-service=cockpit
# firewall-cmd --permanent --remove-service=dhcpv6-client
# firewall-cmd --permanent --remove-service=mdns
# firewall-cmd --permanent --remove-service=samba-client
# firewall-cmd --reload
```

Redémarrer le serveur et vérifier successivement la configuration des
zones&nbsp;:

```
# firewall-cmd --list-all --zone=internal
# firewall-cmd --list-all --zone=external
```


## Scaleway Dedibox

Le serveur dispose d'une interface réseau `enp1s0` associée à la zone
`public`&nbsp;:

```
# nmcli con show
NAME         UUID                                  TYPE      DEVICE
WAN          50a944d6-1df2-31a0-9230-5fb6579897c4  ethernet  enp1s0
System eth0  40b1f19e-576b-45fb-86ff-127516262475  ethernet  --
# firewall-cmd --get-active-zones
public
  interfaces: enp1s0
```

Inscrire la zone explicitement dans la configuration de l'interface
réseau&nbsp;:

```
# echo "ZONE=public" >> /etc/sysconfig/network-scripts/ifcfg-WAN
```

Afficher la configuration de la zone&nbsp;:

```
# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth0
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

Supprimer `cockpit` et `dhcpv6-client` pour ne garder que le seul service
`ssh`&nbsp;:

```
# firewall-cmd --permanent --remove-service=cockpit
# firewall-cmd --permanent --remove-service=dhcpv6-client
success
# firewall-cmd --reload
success
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
