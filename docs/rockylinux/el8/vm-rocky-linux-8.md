
# Installer des VMs Rocky&nbsp;Linux&nbsp;8 sur un hyperviseur Scaleway Dedibox

**Objectif**&nbsp;: installer une série de machines virtuelles minimales
Rocky&nbsp;Linux&nbsp;8 ayant chacune une adresse IP publique sur un
[hyperviseur KVM](hyperviseur-kvm.md) qui tourne sur un serveur dédié Dedibox
de la gamme Start chez Scaleway.

> Cette page ne détaille que les points spécifiques à l'installation d'une
> machine virtuelle. L'installation de Rocky&nbsp;Linux&nbsp;8 sur un serveur
> est [décrite en détail ici](installation-serveur.md).

## Téléchargement

Télécharger le support d'installation sur l'hyperviseur&nbsp;:

```
# cd /var/lib/libvirt/images/
# wget -c \
  http://dl.rockylinux.org/pub/rocky/8.6/isos/x86_64/Rocky-8.6-x86_64-boot.iso
```

> Le choix du support d'installation réseau permet d'économiser de la place sur
> l'hyperviseur.

## Adresse IPv4 publique 

Commander une adresse IP *failover* pour la VM&nbsp;:

* Se connecter à la [console Online](https://console.online.net).

* Ouvrir le menu **Serveur** > **Liste des serveurs**.

* Sélectionner la machine > **Administrer** > **Failover**.

* Cliquer sur **Commander des adresses IP** > **Adresses IP seules**.

* Choisir l'adresse IP dans la liste et cliquer sur **Commander**.

Associer l'adresse IP au serveur&nbsp;:

* Revenir à la page d'administration du serveur > **Failover**.

* Glisser-déposer l'adresse IP vers le serveur correspondant.

* Cliquer sur **Mise à jour** en bas de la page.

* Attendre 5 minutes.

Associer une adresse MAC virtuelle à l'IP *failover*&nbsp;:

* **Serveur** > **Liste des serveurs** > Sélectionner la machine >
  **Administrer**.

* **Réseau** > **Adresses secondaires** > **+ d'infos** > **Ajouter une adresse
  MAC**.

* **Adresse MAC virtuelle** > Déplier le menu > **Ajouter une adresse MAC
  virtuelle pour KVM**.

* Noter l'adresse MAC, par exemple `52:54:00:00:E4:AC`.

## Virtual Machine Manager

La connexion au gestionnaire de machines virtuelles s'effectue par SSH. On va
donc mettre en place l'authentification par clé SSH&nbsp;:

```
$ ssh-copy-id -i ~/.ssh/id_rsa.pub root@sd-155842.dedibox.fr
```

* Démarrer *Virtual Machine Manager*.

* **Fichier** > **Ajouter une connexion**.

* Hyperviseur&nbsp;: **QEMU/KVM**.

* Cocher **Se connecter à l'hôte distant via SSH**.

* Nom d'utilisateur&nbsp;: `root`.

* Nom de l'hôte&nbsp;: `sd-155842.dedibox.fr`.

* Cocher **Connexion automatique**.

* Cliquer sur **Connecter**.

## Démarrer l'installation

* Mettre l'hyperviseur en surbrillance&nbsp;: `QEMU/KVM: sd-155842.dedibox.fr`.

* **Fichier** > **Nouvelle machine virtuelle**.

* Sélectionner **Média d'installation local**.

* Cliquer sur **Parcourir** et actualiser la liste des volumes.

* Sélectionner `Rocky-8.6-x86_64-boot.iso`.

* Allouer `1024 Mo` de mémoire et `2` cœurs de CPU à la VM.

* Créer une image disque de `6 Gio`.

* Nom&nbsp;: `sandbox-01.microlinux.fr`

* Cocher `Personnaliser la configuration avant l'installation`.

* Sélectionner le réseau&nbsp;: **Bridge device** > `br0`.

* Cliquer sur **Terminer**.

* **NIC :aa:bb:cc** > **Adresse MAC** > remplacer par l'adresse allouée par
  Scaleway, par exemple `52:54:00:00:E4:AC`.

* Appliquer les changements et cliquer sur **Commencer l'installation**.

> Utiliser le raccourci ++ctrl+++++alt++ pour détacher le curseur de la console.

## Partitionnement

Une fois qu'on se retrouve dans la fenêtre principale de l'installateur
Anaconda, on peut basculer vers une console virtuelle pour effectuer un
partitionnement manuel en ligne de commande&nbsp;:

* **Touche d'envoi** > **Ctrl-Alt-F5**.

Éventuellement, configurer la disposition du clavier dans la console&nbsp;:

```
# loadkeys fr_CH-latin1
```

Partitionner le disque virtuel&nbsp;:

```
# fdisk /dev/vda
```

On utilisera un schéma de partitionnement minimaliste&nbsp;:

* une partition d'échange `/dev/vda1` de 1 Go

* une partition principale `/dev/vda2` de 5 Go

> Le disque virtuel pourra être facilement redimensionné en cas de besoin.

```
# fdisk -l /dev/vda
Disk /dev/vda: 6 GiB, 6442450944 bytes, 12582912 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xd00dcb2e

Device     Boot   Start      End  Sectors Size Id Type
/dev/vda1          2048  2099199  2097152   1G 82 Linux swap / Solaris
/dev/vda2  *    2099200 12582911 10483712   5G 83 Linux
```

Retourner dans l'installateur graphique&nbsp;:

* **Touche d'envoi** > **Ctrl-Alt-F6**.

* **Partitionnement** > **Personnalisé** > **Rafraîchir** > **Réanalyser les
  disques**

* Mettre `/dev/vda1` en surbrillance > cliquer sur **Reformater** > Système de
  fichiers&nbsp;: `swap` > Étiquette&nbsp;: `swap` > cliquer sur **Mise à jour
  des paramètres**.

* Mettre `/dev/vda2` en surbrillance > cliquer sur **Reformater** > Système de
  fichiers&nbsp;: `ext4` > Point de montage&nbsp;: `/` > Étiquette&nbsp;:
  `root` > cliquer sur **Mise à jour des paramètres**.

## Réseau &amp; nom d'hôte

Configurer le réseau&nbsp;:

* Cliquer sur **Configurer**.

* Nom de la connexion&nbsp;: `WAN`

* Périphérique : `enp1s0 (52:54:00:00:E4:AC)`

* Ouvrir l'onglet **Paramètres IPv4**.

* Méthode : **Manuel**.

* Adresses : cliquer sur **Ajouter**.

* Adresse : `163.172.226.12`.

* Masque de réseau : `255.255.255.0`.

* Passerelle : `62.210.0.1`

* `Serveurs DNS` : `62.210.16.6`, `62.210.16.7`

* Ouvrir l'onglet **Paramètres IPv6**.

* Méthode : **Ignorer**.

* Cliquer sur **Enregistrer**.

* Nom d'hôte : `sandbox-01`.

* Activer le réseau.

> L'adresse IP `62.210.0.1` n'est pas une erreur. Elle correspond à la
> passerelle unique fournie par Scaleway pour toutes les machines virtuelles.

## Configuration post-installation

Configurer l'authentification par clé SSH&nbsp;:

```
$ ssh-copy-id -i .ssh/id_rsa.pub root@sandbox-01.microlinux.fr
$ ssh-copy-id -i .ssh/id_rsa.pub microlinux@sandbox-01.microlinux.fr
```

Supprimer le fichier Kickstart&nbsp;:

```
# rm -f anaconda-ks.cfg 
```

Supprimer le délai d'attente du chargeur de démarrage&nbsp;:

```
# /etc/default/grub
GRUB_DEFAULT=0
GRUB_TIMEOUT=0
...
```

Prendre en compte les modifications&nbsp;:

```
# grub2-mkconfig -o /boot/grub2/grub.cfg
```

Procéder à la configuration post-installation comme pour un serveur
classique&nbsp;:

* [Configuration automatique](configuration-automatique.md)

* [Configurer NetworkManager](networkmanager.md)

* [Configurer FirewallD](firewalld.md)

* [Synchronisation NTP](ntp-chrony.md)

* [Postfix minimal](postfix-minimal-dedibox.md)

* [Protéger SSH avec Fail2ban](fail2ban-ssh.md)

* [Mises à jour automatiques](dnf-automatic.md)

## Créer un template

Une fois que la VM est correctement configurée, je vais m'en servir pour créer
un *template* dans un premier temps&nbsp;:

* Éteindre la VM.

* Mettre la VM en surbrillance.

* Clic droit > **Cloner**.

* Nom&nbsp;: `sandbox-template`.

* Cliquer sur **Cloner**.

* Mettre le *template* `sandbox-template` en surbrillance.

* **Édition** > **Détails de la machine virtuelle**.

* **Afficher** > **Détails**.

* **NIC :aa:bb:cc** > Clic droit > **Enlever un matériel**.

## Cloner une VM

À partir du *template* `sandbox-template`, je vais créer une machine virtuelle
clonée `sandbox-02.microlinux.fr` qui dispose de sa propre adresse IPv4
publique&nbsp;:

* Mettre le *template* `sandbox-template` en surbrillance.

* Clic droit > **Cloner**.

* Nom&nbsp;: `sandbox-02.microlinux.fr`.

* Cliquer sur **Cloner**.

* Mettre la VM `sandbox-02.microlinux.fr` en surbrillance.

* **Édition** > **Détails de la machine virtuelle**.

* **Afficher** > **Détails**.

* **Ajouter un matériel** > **Réseau**.

* **Source du réseau** > **Bridge device** > Device Name&nbsp;: `br0`.

* **Adresse MAC**&nbsp;: renseigner l'adresse MAC allouée par Scaleway pour
  l'adresse IPv4 correspondante.

* Cliquer sur **Terminer**.

* Démarrer la VM `sandbox-02.microlinux.fr` en affichant la console.

Renseigner le nom d'hôte correct dans `/etc/hostname`&nbsp;:

```
sandbox-02
```

Rectifier l'adresse IP et le nom d'hôte dans `/etc/hosts`&nbsp;:

```
# /etc/hosts
127.0.0.1     localhost.localdomain localhost
212.129.5.42  sandbox-02.microlinux.fr sandbox-02 
```

Redémarrer&nbsp;:

```
# reboot
```

Corriger la configuration de NetworkManager&nbsp;:

```
# nmtui
```

* Sélectionner `Edit a connection`.

* Supprimer `Wired connection 1`.

* Éditer la connexion `WAN`.

* Renseigner l'adresse MAC correcte dans la rubrique `Device`.

* Renseigner l'adresse IPv4 correcte.

* Activer la connexion `WAN`.

Tester la connexion&nbsp;:

```
# ping -c 4 google.fr
```

Éditer `/etc/postfix/main.cf` pour corriger la configuration de Postfix&nbsp;:

```
# Host
myhostname = sandbox-02.microlinux.fr
```

Prendre en compte les modifications&nbsp;:

```
# systemctl restart postfix
```

Éditer `/etc/dnf/automatic.conf` pour corriger la configuration des mises à
jour automatiques&nbsp;:

```
[emitters]
system_name = sandbox-02.microlinux.fr
emit_via = email
```

Procéder de même pour cloner la machine virtuelle `sandbox-03.microlinux.fr`.

## Agrandir une VM

Éteindre la VM et redimensionner l'image dans l'hyperviseur&nbsp;:

```
# cd /var/lib/libvirt/images/
# qemu-img resize sandbox-03.microlinux.fr.qcow2 +5G
```

Démarrer la VM, se connecter et installer l'outil `growpart`&nbsp;:

```
# dnf install -y cloud-utils-growpart
```

Afficher les disques&nbsp;:

```
# lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sr0     11:0    1 1024M  0 rom
vda    253:0    0   11G  0 disk
├─vda1 253:1    0    1G  0 part [SWAP]
└─vda2 253:2    0    5G  0 part /
```

Agrandir la partition principale `/dev/vda2`&nbsp;:

```
# growpart -v /dev/vda 2
update-partition set to true
resizing 2 on /dev/vda using resize_sfdisk_dos
...
# lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sr0     11:0    1 1024M  0 rom
vda    253:0    0   11G  0 disk
├─vda1 253:1    0    1G  0 part [SWAP]
└─vda2 253:2    0   10G  0 part /
```

Redimensionner le système de fichiers `ext4` de la partition principale avec
`resize2fs`&nbsp;:

```
# resize2fs /dev/vda2
resize2fs 1.45.6 (20-Mar-2020)
Filesystem at /dev/vda2 is mounted on /; on-line resizing required
old_desc_blocks = 1, new_desc_blocks = 2
The filesystem on /dev/vda2 is now 2621179 (4k) blocks long.
# df -h | grep vda2
/dev/vda2       9.8G  2.3G  7.1G  25% /
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
