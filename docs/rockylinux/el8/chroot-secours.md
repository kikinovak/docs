
# Chroot depuis un système de secours

Dans certains cas de figure, la réparation cohérente d'un système endommagée
nécessite de l'utiliser&nbsp;:

* Effectuer un *downgrade* d'un paquet problématique.

* Réinitialiser un mot de passe oublié.

* Réinstaller le chargeur de démarrage.

* Reconstruire le disque mémoire initial (`initrd`). 

> La commande `chroot` (*change root*) permet de changer le répertoire racine
> vers un nouvel emplacement.

Démarrer la console de secours...&nbsp;:

* ... sur un [serveur local](console-de-secours.md).

* ... sur un [serveur dédié](console-scaleway.md).

Identifier les différentes partitions&nbsp;:

```
# lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
...
sda      8:0    0 931,5G  0 disk
├─sda1   8:1    0   500M  0 part
├─sda2   8:2    0   927G  0 part
└─sda3   8:3    0     4G  0 part [SWAP]
```

Le système sera monté sur `/mnt`&nbsp;:

```
# mount -v /dev/sda2 /mnt/
mount: /dev/sda2 mounted on /mnt.
# mount -v /dev/sda1 /mnt/boot/
mount: /dev/sda1 mounted on /mnt/boot.
```

Lier les systèmes de fichiers virtuels `/proc`, `/dev` et `/sys` de
l'environnement de secours au système installé&nbsp;:

```
# mount -v -t proc proc /mnt/proc/
mount: proc mounted on /mnt/proc.
# mount -v -t sysfs sys /mnt/sys/
mount: sys mounted on /mnt/sys.
# mount -v -o bind /dev /mnt/dev/
mount: /dev bound on /mnt/dev.
# mount -v -t devpts pts /mnt/dev/pts/
mount: pts mounted on /mnt/dev/pts.
```

Basculer vers le système installé en spécifiant l'interpréteur de
commandes&nbsp;:

```
# chroot /mnt /bin/bash
```

Partant de là, on peut procéder aux opérations de maintenance du système
défaillant. 

Une fois que l'intervention est terminée, quitter le système installé pour
revenir dans le système de secours&nbsp;:

```
# exit
```

Démonter une à une les partitions du système installé, dans l'ordre inverse du
montage&nbsp;:

```
# umount -v /mnt/dev/pts/
umount: /mnt/dev/pts/ unmounted
# umount -v /mnt/dev/
umount: /mnt/dev/ unmounted
# umount -v /mnt/sys/
umount: /mnt/sys/ unmounted
# umount -v /mnt/proc/
umount: /mnt/proc/ unmounted
# umount -v /mnt/boot/
umount: /mnt/boot/ unmounted
# umount -v /mnt/
umount: /mnt/ unmounted
```

Il ne reste plus qu'à arrêter la console de secours.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
