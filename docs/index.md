
# La documentation de Microlinux

Ce site regroupe ma propre documentation technique, celle que j'utilise au
quotidien pour toutes mes configurations. Ce n'est pas une formation technique,
plutôt une série de recettes éprouvées qui vont droit au but. Je les publie
pour en faire profiter mes collègues qui administrent des serveurs et des
postes de travail au quotidien, et qui y trouveront peut-être des réponses à
leurs questions.

> Si vous souhaitez vous former à Linux, [regardez plutôt
> ici](https://formation-linux.microlinux.fr).

## Rocky Linux 8

### Maintenance &amp; dépannage

* [Lancer la console de secours locale](rockylinux/el8/console-de-secours.md)

* [Lancer la console de secours Scaleway](rockylinux/el8/console-scaleway.md)

* [Chroot depuis un système de secours](rockylinux/el8/chroot-secours.md)

* [Monter un disque RAID](rockylinux/el8/montage-raid.md)

* [Cloner un disque via le réseau](rockylinux/el8/clonage-reseau.md)

* [Réinitialer un ou plusieurs disques durs](rockylinux/el8/reinitialiser-disques.md)

### Installation &amp; Configuration

* [Installer Rocky Linux 8 sur un serveur](rockylinux/el8/installation-serveur.md)

* [Partitionner un disque dur](rockylinux/el8/partitionnement-simple.md)

* [RAID 1 avec deux disques](rockylinux/el8/partitionnement-raid1.md)

* [RAID 6 avec quatre disques](rockylinux/el8/partitionnement-raid6.md)

* [Installer Rocky Linux 8 sur un serveur dédié](rockylinux/el8/installation-dedibox.md)

* [Configuration automatique d'un serveur Rocky Linux 8](rockylinux/el8/configuration-automatique.md)

* [Configurer NetworkManager](rockylinux/el8/networkmanager.md)

### Matériel

* [Installer Rocky Linux sur un serveur HP Proliant ML110 Gen9](rockylinux/el8/proliant-ml110-gen9.md)

### Sécurité

* [Activer SELinux sur un serveur Scaleway Dedibox](rockylinux/el8/selinux-dedibox.md)

* [Configurer FirewallD](rockylinux/el8/firewalld.md)

* [Protéger SSH avec Fail2ban](rockylinux/el8/fail2ban-ssh.md)

* [Mises à jour automatiques avec DNF Automatic](rockylinux/el8/dnf-automatic.md)

### Client/serveur NTP

* [Synchronisation NTP avec Chrony](rockylinux/el8/ntp-chrony.md)

### Serveur mail

* [Postfix minimal sur un serveur LAN](rockylinux/el8/postfix-minimal-lan.md)

* [Postfix minimal sur un serveur dédié](rockylinux/el8/postfix-minimal-dedibox.md)

### Virtualisation

* [Installer un hyperviseur KVM](rockylinux/el8/hyperviseur-kvm.md)

* [Installer Rocky Linux 8 sur un hyperviseur KVM](rockylinux/el8/vm-rocky-linux-8.md)


## OpenSUSE Tumbleweed

### Installation &amp; Configuration

* [Télécharger OpenSUSE Tumbleweed](opensuse/tumbleweed/telechargement.md)

* [Installer OpenSUSE Tumbleweed](opensuse/tumbleweed/installation.md)

* [Mettre à jour OpenSUSE Leap vers Tumbleweed](opensuse/tumbleweed/mise-a-jour.md)

* [Partitionner un disque](opensuse/tumbleweed/partitionnement-simple.md)

* [RAID 1 avec deux disques](opensuse/tumbleweed/partitionnement-raid1.md)

* [Configurer OpenSUSE Tumbleweed KDE](opensuse/tumbleweed/configuration-automatique.md)

* [Gérer les applications Flatpak ](opensuse/tumbleweed/flatpak.md)

* [Installer et configurer Citrix Workspace](opensuse/tumbleweed/citrix.md)

### Matériel

* [Configurer une carte graphique NVidia](opensuse/tumbleweed/nvidia.md)

* [Configurer une imprimante/scanner HP](opensuse/tumbleweed/imprimante-hp.md)

### Virtualisation

* [Installer et configurer VirtualBox](opensuse/tumbleweed/virtualbox.md)

* [Utiliser Vagrant avec VirtualBox](opensuse/tumbleweed/vagrant.md)

* [Installer et configurer un hyperviseur KVM](opensuse/tumbleweed/hyperviseur-kvm.md)


## Microsoft Windows 10

### Installation &amp; Configuration

* [Télécharger Windows 10](misc/windows/telechargement.md)

* [Installer Windows 10](misc/windows/installation.md)

* [Configuration initiale de Windows 10](misc/windows/configuration.md)

* [Activer Windows 10 et installer MS Office](misc/windows/activation.md)

* [Installer et gérer des applications sous Windows 10](misc/windows/applications.md)

### Serveur de fichiers

* [Connecter un poste client Windows 10 à un serveur Samba](misc/windows/client-samba.md)


---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

