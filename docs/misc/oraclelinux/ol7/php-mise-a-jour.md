
Arrêter le serveur Apache&nbsp;:

```
$ sudo systemctl stop httpd
```

Si l’on utilise PHP 7.2 en provenance des dépôts SCL, arrêter le service
`php-fpm` correspondant&nbsp;:

```
$ sudo systemctl stop rh-php72-php-fpm
```

Faire la liste de tous les paquets PHP installés&nbsp;:

```
$ rpm -qa | grep php > ~/php.txt
```

Supprimer PHP 5.4&nbsp;:

```
$ sudo yum remove php-common
```

Alternativement, supprimer PHP 7.2&nbsp;:

```
$ sudo yum remove rh-php72-php-common rh-php72-runtime
```

Faire le ménage dans les fichiers de configuration&nbsp;:

```
$ sudo rm -rf /etc/opt/rh/rh-php7*
```

Supprimer le lien symbolique `/usr/bin/php` qui pointe vers
`/opt/rh/rh-php72/root/usr/bin/php`&nbsp;:

```
$ sudo rm /usr/bin/php
```

À partir de là, on peut [installer PHP 7.3](php-73.md).

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
