
Le serveur dispose d'une interface réseau `enp2s0` associée à la zone
`public`&nbsp;:

```
$ nmcli con show
NAME  UUID                                  TYPE      DEVICE 
LAN   2cbbaa36-a0c3-3134-b8ad-8b993258e548  ethernet  enp2s0
$ firewall-cmd --get-active-zones
public
  interfaces: enp2s0
```

Définir la zone appropriée `internal` pour l'interface&nbsp;:

```
$ sudo firewall-cmd --permanent --zone=internal --change-interface=enp2s0
$ sudo firewall-cmd --set-default-zone=internal
```

Supprimer quelques services prédéfinis&nbsp;:

```
$ sudo firewall-cmd --permanent --remove-service=dhcpv6-client
$ sudo firewall-cmd --permanent --remove-service=mdns
$ sudo firewall-cmd --permanent --remove-service=samba-client
$ sudo firewall-cmd --reload
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
