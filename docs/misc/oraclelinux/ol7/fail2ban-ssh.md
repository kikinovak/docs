
Installer Fail2ban&nbsp;:

```
$ sudo yum install fail2ban-server fail2ban-firewalld
```

Créer un fichier `/etc/fail2ban/jail.d/custom.conf` pour protéger le service SSH
contre les attaques par force brute&nbsp;:

```
# /etc/fail2ban/jail.d/custom.conf

[DEFAULT]
bantime = 24h
ignoreip = 88.161.127.222

[sshd]
enabled = true
```

Éventuellement, créer une exception pour plusieurs adresses IP&nbsp;:

```
ignoreip = 78.197.22.147 163.172.63.88
```

Basculer SELinux en mode permissif&nbsp;:

```
$ sudo setenforce 0
```

Activer et démarrer le service&nbsp;:

```
$ sudo systemctl enable fail2ban --now
```

Régler un problème relatif à SELinux&nbsp;:

```
$ sudo ausearch -c 'f2b/f.sshd' --raw | sudo audit2allow -M my-f2bfsshd
$ sudo semodule -i my-f2bfsshd.pp
$ sudo rm -f my-f2bfsshd.*
```

Alternativement&nbsp;:

```
$ sudo ausearch -c 'f2b/server' --raw | sudo audit2allow -M my-f2bserver
$ sudo semodule -i my-f2bserver.pp
$ sudo rm -f my-f2bserver.*
```

Vérifier si le problème est réglé&nbsp;:

```
$ echo | sudo tee /var/log/audit/audit.log
$ sudo systemctl restart fail2ban
$ sudo sealert -a /var/log/audit/audit.log
100% done
found 0 alerts in /var/log/audit/audit.log
```

Repasser en mode renforcé&nbsp;:

```
$ sudo setenforce 1
```

Afficher les adresses IP bannies&nbsp;:

```
$ sudo fail2ban-client status sshd
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*


