
Vérifier si Postfix et la commande `mail` sont installés&nbsp;:

```
$ rpm -q postfix mailx
postfix-2.10.1-9.el7.x86_64
mailx-12.5-19.el7.x86_64
```

Vider `/etc/postfix/main.cf` et adapter la configuration suivante&nbsp;:

```
# /etc/postfix/main.cf
#
# Minimal Postfix configuration for Internet-facing servers

# Disable IPv6
inet_protocols = ipv4

# Outbound mail only
mailbox_size_limit = 0
inet_interfaces = localhost

# Banner
smtpd_banner = $myhostname ESMTP 

# Host
myhostname = sd-100246.dedibox.fr

# Domain
mydomain = dedibox.fr

# Domain that appears in mail posted on this machine
myorigin = $myhostname

# Authorize local machine only
mynetworks = 127.0.0.0/8

# Deliver mail directly to the Internet
relayhost =

# Local aliasing
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases

# Debugging
debugger_command =
  PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
  ddd $daemon_directory/$process_name $process_id & sleep 5

# Command paths
sendmail_path = /usr/sbin/sendmail.postfix
newaliases_path = /usr/bin/newaliases.postfix
mailq_path = /usr/bin/mailq.postfix

# Documentation
manpage_directory = /usr/share/man
sample_directory = /usr/share/doc/postfix-2.10.1/samples
readme_directory = /usr/share/doc/postfix-2.10.1/README_FILES
```

Éditer la table de correspondance&nbsp;:

```
# /etc/aliases
...
decode:         root
# Person who should get root's mail
root:           info@microlinux.fr
```

Construire le fichier indexé&nbsp;:

```
$ sudo newaliases
```

Redémarrer le service&nbsp;:

```
$ sudo systemctl restart postfix
```

Vérifier si Postfix tourne correctement&nbsp;:

```
$ systemctl status postfix
$ sudo cat /var/log/maillog
```

Envoyer un mail de test&nbsp;:

```
$ mail root
Subject: Test Postfix
Ceci est un test.
.
EOT
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

