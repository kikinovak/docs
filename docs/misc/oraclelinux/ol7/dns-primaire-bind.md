
Configurer BIND comme serveur maître primaire pour les deux domaines
`slackbox.fr` et `unixbox.fr`.

Réserver les noms de domaine chez [BookMyName.com](https://www.bookmyname.com).

Autoriser les requêtes DNS dans le pare-feu&nbsp;:

```
$ sudo firewall-cmd --permanent --add-service=dns
$ sudo firewall-cmd --reload
```

Installer BIND et quelques outils&nbsp;:

```
$ sudo yum install bind bind-utils
```

Sauvegarder la configuration existante&nbsp;:

```
$ cd /etc
$ sudo mv named.conf named.conf.orig
```

Éditer `/etc/named.conf`&nbsp;:

```
// /etc/named.conf

options {
  directory "/var/named";
  filter-aaaa-on-v4 yes;
  forwarders {
    62.210.16.6;
    62.210.16.7;
  };
};

logging {
  channel single_log {
    file "/var/log/named/named.log" versions 3 size 2m;
    severity info;
    print-time yes;
    print-severity yes;
    print-category yes;
  };
  category default {
    single_log;
  };
};

zone "." IN {
  type hint;
  file "named.ca";
};

include "/etc/named.rfc1912.zones";
include "/etc/named.conf.local";
```

Éditer `/etc/named.conf.local`&nbsp;:

```
// /etc/named.conf.local

zone "slackbox.fr" {
  type master;
  allow-transfer { 62.210.16.8; };
  file "zone.slackbox.fr";
};

zone "unixbox.fr" {
  type master;
  allow-transfer { 62.210.16.8; };
  file "zone.unixbox.fr";
};
```

Désactiver l'IPv6 dans `/etc/sysconfig/named`&nbsp;:

```
# /etc/sysconfig/named
OPTIONS="-4"
```

Régler les permissions&nbsp;:

```
$ sudo chown root:named /etc/named.conf*
$ sudo chmod 0640 /etc/named.conf*
```

Créer le fichier log à l'endroit approprié&nbsp;:

```
$ sudo mkdir /var/log/named
$ sudo touch /var/log/named/named.log
$ sudo chown -R named:named /var/log/named/
$ sudo chmod 0770 /var/log/named
```

Rectifier le contexte SELinux&nbsp;:

```
$ sudo restorecon -Rv /var/log/named
```

Éditer les fichiers zone comme ceci, en adaptant la configuration&nbsp;:

```
; /var/named/zone.slackbox.fr
$TTL 86400
$ORIGIN slackbox.fr.
@ IN SOA ns.slackbox.fr. hostmaster.slackbox.fr. (
   2021011801   ; sn
        10800   ; refresh (3 heures)
          600   ; retry (10 minutes)
      1814400   ; expiry (3 semaines)
        10800 ) ; minimum (3 heures)
        IN          NS      ns.slackbox.fr.
        IN          NS      nssec.online.net.
        IN          MX      10 mail.slackbox.fr.
slackbox.fr.        A       163.172.220.174
ns      IN          A       163.172.220.174
mail    IN          A       163.172.220.174
www     CNAME               slackbox.fr.
```

Définir les permissions appropriées&nbsp;:

```
$ sudo chown root:named /var/named/zone.slackbox.fr
$ sudo chmod 0640 /var/named/zone.slackbox.fr
```

Vérifier la définition correcte de la zone&nbsp;:

```
$ sudo named-checkzone slackbox.fr /var/named/zone.slackbox.fr
zone slackbox.fr/IN: loaded serial 2020031101
OK
```

Activer et démarrer BIND&nbsp;:

```
$ sudo systemctl enable named --now
```

Utiliser le serveur DNS local&nbsp;:

```
# /etc/resolv.conf
nameserver 127.0.0.1
```

Ouvrir l'interface web de BookMyName > `Gérer` > `Domaine` > `Modifier` > `Vos
DNS` et renseigner le serveur&nbsp;:

* `Serveur DNS 1` : `ns.slackbox.fr` / `Adresse IPv4` : `163.172.220.174`

* `Serveur DNS 2` : `nssec.online.net`

Définir les DNS secondaires dans la [console
Online](https://console.online.net) > `Liste de vos serveurs` >
`sd-xxxxxx.dedibox.fr` > `DNS secondaires`&nbsp;:

* `Domaine` : `slackbox.fr`

* `Adresse principale` : `163.172.220.174`

* `Serveur secondaire` : `62.210.16.8` (`nssec.online.net`)

Renseigner la recherche DNS inverse dans `Liste de vos serveurs` >
`sd-xxxxxx.dedibox.fr` > `Réseau` > `Modifier les reverses`&nbsp;:

* `Adresse` : `163.172.220.174`

* `Reverse` : `sd-100246.dedibox.fr`

Tester le serveur DNS en effectuant une série de requêtes depuis une machine
externe&nbsp;:

```
$ host slackbox.fr
slackbox.fr has address 163.172.220.174
slackbox.fr mail is handled by 10 mail.slackbox.fr.
$ host 163.172.220.174
174.220.172.163.in-addr.arpa domain name pointer 
sd-100246.dedibox.fr.
$ host -t mx slackbox.fr
slackbox.fr mail is handled by 10 mail.slackbox.fr.
$ host mail.slackbox.fr
mail.slackbox.fr has address 163.172.220.174
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*


