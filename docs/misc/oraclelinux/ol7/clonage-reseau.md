
Écrire des zéros sur la partie non utilisée du disque pour réduire la taille de
l'image&nbsp;:

```
# dd if=/dev/zero of=/0bits bs=20M; rm -f /0bits
```

Sauvegarder une machine locale depuis un LiveCD ou une [console de
secours](console-de-secours.md)&nbsp;:

```
# dd if=/dev/sda status=progress | gzip --fast - | \
  ssh microlinux@nestor dd of=oracle-7.9-server-image.gz
```

* `status=progress` affiche la progression pour `dd`

* `--fast` spécifie un algorithme de compression moins compact et plus rapide
pour `gzip`

Restaurer cette machine depuis un LiveCD :

```
# ssh microlinux@nestor dd if=oracle-7.9-server-image.gz | \
  gunzip --fast - | dd of=/dev/sda status=progress
```

Sauvegarder un serveur dédié Scaleway depuis une session de secours :

* Se connecter à la [console Online](https://console.online.net).

* Ouvrir le menu `Serveur` > `Liste des serveurs`.

* Sélectionner la machine > `Administrer` > `Secours`.

* `Sélectionner un système d'exploitation` > `Ubuntu 18.04`.

* Cliquer sur `Lancer le système de secours sur votre Dedibox`.

* Attendre 5 minutes.

* Ouvrir une connexion SSH avec les paramètres affichés.

```
$ sudo -s
# dd if=/dev/sda status=progress | gzip --fast - | \
  ssh microlinux@backup.microlinux.fr dd of=oracle-7.9-server-image.gz
```

Restaurer cette machine depuis la session de secours :

```
$ sudo -s
# ssh microlinux@backup.microlinux.fr dd if=oracle-7.9-server-image.gz | \
  gunzip --fast - | dd of=/dev/sda status=progress
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

