
Postfix peut être la cible d'une attaque de déni de service :

```
$ sudo tail -f /var/log/maillog
... postfix/smtpd[7786]: connect from unknown[45.148.10.190]
... postfix/smtpd[8536]: connect from unknown[106.38.58.138]
... postfix/smtpd[7786]: warning: unknown[45.148.10.190]: SASL LOGIN authentication failed
... postfix/smtpd[7786]: disconnect from unknown[45.148.10.190]
... postfix/smtpd[7786]: connect from unknown[212.70.149.55]
... postfix/smtpd[8536]: warning: unknown[106.38.58.138]: SASL LOGIN authentication failed
```

Éditer `/etc/fail2ban/jail.d/custom.conf` et ajouter une stance pour
Postfix&nbsp;:

```
# /etc/fail2ban/jail.d/custom.conf

[DEFAULT]
bantime = 24h
ignoreip = 88.161.127.222 163.172.81.8

[sshd]
enabled = true

[postfix]
enabled = true
mode    = extra
```

Prendre en compte les modifications&nbsp;:

```
$ sudo systemctl restart fail2ban
```

Afficher la liste des adresses IP bannies&nbsp;:

```
$ sudo fail2ban-client status postfix
Status for the jail: postfix
|- Filter
|  |- Currently failed: 1
|  |- Total failed:     11
|  `- Journal matches:  _SYSTEMD_UNIT=postfix.service
`- Actions
   |- Currently banned: 13
   |- Total banned:     13
   `- Banned IP list:   91.191.209.149 91.191.209.164 45.148.10.190
   91.191.209.150 212.70.149.55 91.191.209.163 91.191.209.162 91.191.209.130
   91.191.209.166 109.237.103.13 87.246.7.227 91.191.209.123 45.227.253.115
```

Vérifier si les tentatives de connexion suspectes ont disparu&nbsp;:

```
$ sudo tail -f /var/log/maillog
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
