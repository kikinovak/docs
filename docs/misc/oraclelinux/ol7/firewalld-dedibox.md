
Le serveur dispose d'une interface réseau `eth0` associée à la zone
`public`&nbsp;:

```
$ nmcli con show
NAME  UUID                                  TYPE      DEVICE 
WAN   f1f2025e-dbed-3596-b314-371972966e30  ethernet  eth0
$ firewall-cmd --get-active-zones
public
  interfaces: eth0
```

Inscrire la zone explicitement dans la configuration de l'interface
réseau&nbsp;:

```
$ echo "ZONE=public" | sudo tee -a /etc/sysconfig/network-scripts/ifcfg-eth0
```

Afficher la configuration de la zone&nbsp;:

```
$ sudo firewall-cmd --list-all
public (active)
target: default
icmp-block-inversion: no
interfaces: eth0
sources:
services: dhcpv6-client ssh
ports:
protocols:
masquerade: no
forward-ports:
source-ports:
icmp-blocks:
rich rules:
```

Supprimer `dhcpv6-client` pour ne garder que le seul service `ssh`&nbsp;:

```
$ sudo firewall-cmd --permanent --remove-service=dhcpv6-client
success
$ sudo firewall-cmd --reload
success
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

