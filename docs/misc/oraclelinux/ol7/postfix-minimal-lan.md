
Vérifier si Postfix et la commande `mail` sont installés&nbsp;:

```
$ rpm -q postfix mailx
postfix-2.10.1-7.el7.x86_64
mailx-12.5-19.el7.x86_64
```

Éditer `/etc/postfix/main.cf` en adaptant la configuration&nbsp;:

```
# /etc/postfix/main.cf
#
# Minimal Postfix configuration for LAN servers

# Disable IPv6
inet_protocols = ipv4

# Outbound mail only
mailbox_size_limit = 0
inet_interfaces = localhost

# Banner
smtpd_banner = $myhostname ESMTP 

# Host
myhostname = sandbox.microlinux.lan

# Domain
mydomain = microlinux.lan

# Domain that appears in mail posted on this machine
myorigin = $myhostname

# Authorize local machine only
mynetworks = 127.0.0.0/8

# Deliver via Gmail
relayhost = smtp.gmail.com:587
smtp_use_tls = yes
smtp_sasl_auth_enable = yes
smtp_sasl_security_options =
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_tls_CAfile = /etc/ssl/certs/ca-bundle.crt

# Local aliasing
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases

# Debugging
debugger_command =
  PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
  ddd $daemon_directory/$process_name $process_id & sleep 5

# Command paths
sendmail_path = /usr/sbin/sendmail.postfix
newaliases_path = /usr/bin/newaliases.postfix
mailq_path = /usr/bin/mailq.postfix

# Documentation
manpage_directory = /usr/share/man
sample_directory = /usr/share/doc/postfix-2.10.1/samples
readme_directory = /usr/share/doc/postfix-2.10.1/README_FILES
```

Renseigner les paramètres de connexion dans `/etc/postfix/sasl_passwd`&nbsp;:

```
smtp.gmail.com:587 username@gmail.com:password
```

Générer le fichier `sasl_passwd.db`&nbsp;:

```
$ sudo postmap sasl_passwd
```

Supprimer le fichier `sasl_passwd` et restreindre les permissions du fichier
`sasl_passwd.db`&nbsp;:

```
$ sudo rm sasl_passwd
$ sudo chmod 0400 sasl_passwd.db
```

Éditer la table de correspondance&nbsp;:

```
# /etc/aliases
...
# Person who should get root's mail
root:           info@microlinux.fr
```

Construire le fichier indexé&nbsp;:

```
$ sudo newaliases
```

Prendre en compte les modifications&nbsp;:

```
$ sudo systemctl restart postfix
```

Ouvrir le tableau de bord Gmail > `Sécurité` > `Accès aux applications moins
sécurisées` > `Activer`.

Envoyer un mail de test&nbsp;:

```
$ mail root
Subject: Test Postfix
Ceci est un test.
.
EOT
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*


