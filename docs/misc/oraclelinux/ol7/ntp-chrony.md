
Effectuer une première synchronisation manuelle si la machine est complètement
déréglée&nbsp;:

```
$ sudo systemctl stop chronyd
$ sudo chronyd -q 'pool pool.ntp.org iburst'
$ sudo systemctl start chronyd
```

Configurer Chrony&nbsp;:

```
# /etc/chrony.conf
server 0.fr.pool.ntp.org iburst
server 1.fr.pool.ntp.org iburst
server 2.fr.pool.ntp.org iburst
server 3.fr.pool.ntp.org iburst
driftfile /var/lib/chrony/drift
makestep 1.0 3
rtcsync
logdir /var/log/chrony
```

Alternativement, synchroniser la machine avec une source locale&nbsp;:

```
# /etc/chrony.conf
server 192.168.2.1 iburst
driftfile /var/lib/chrony/drift
makestep 1.0 3
rtcsync
logdir /var/log/chrony
```

Éventuellement, autoriser les connexions locales au serveur&nbsp;:

```
# /etc/chrony.conf
server 0.fr.pool.ntp.org iburst
server 1.fr.pool.ntp.org iburst
server 2.fr.pool.ntp.org iburst
server 3.fr.pool.ntp.org iburst
driftfile /var/lib/chrony/drift
makestep 1.0 3
rtcsync
allow 192.168.2.0/24
local stratum 10
logdir /var/log/chrony
```

Prendre en compte les modifications&nbsp;:

```
$ sudo systemctl restart chronyd
```

Vérifier la synchronisation&nbsp;:

```
$ watch chronyc sources
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
