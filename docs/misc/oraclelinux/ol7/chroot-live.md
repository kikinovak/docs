
Démarrer la [console de secours](console-de-secours.md) si l'on dispose d'un
accès physique à la machine.

Sur un serveur dédié Scaleway, on pourra démarrer le système de secours&nbsp;:

* Se connecter à la [console Online](https://console.online.net).

* Ouvrir le menu `Serveur` > `Liste des serveurs`.

* Sélectionner la machine > `Administrer` > `Secours`.

* `Sélectionner un système d'exploitation` > `Ubuntu 18.04`.

* Cliquer sur `Lancer le système de secours sur votre Dedibox`.

* Attendre 5 minutes.

* Ouvrir une connexion SSH avec les paramètres affichés.

Le système sera monté sur `/mnt` :

```
$ sudo -s
# mount /dev/sda2 /mnt
# mount /dev/sda1 /mnt/boot
# mount -t proc proc /mnt/proc
# mount -t sysfs sys /mnt/sys
# mount -o bind /dev /mnt/dev
# mount -t devpts pts /mnt/dev/pts
# chroot /mnt
$ su - root
```

Démonter le système une fois que l'intervention est terminée :

```
# exit
# umount /mnt/dev/pts
# umount /mnt/dev
# umount /mnt/sys
# umount /mnt/proc
# umount /mnt/boot
# umount /mnt
```
Dans la console Online, cliquer sur `Repasser en mode normal`.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
