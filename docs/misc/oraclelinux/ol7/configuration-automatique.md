
Installer Git si ce n'est pas déjà fait&nbsp;:

```
$ sudo yum install git
```

Récupérer et lancer mon script de configuration post-installation&nbsp;:

```
$ git clone https://gitlab.com/kikinovak/oracle.git
$ cd oracle/
$ sudo ./linux-setup.sh --setup
```

Éventuellement, procéder à un élagage initial&nbsp;:

```
$ sudo ./linux-setup.sh --reset
```

Passer le système en anglais pour l'utilisateur initial&nbsp;:

```
# ~/.bashrc
LANG=en_US.utf8
export LANG
```
