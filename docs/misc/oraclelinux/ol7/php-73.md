
Vérifier si le dépôt des *Oracle Software Collections* est bien
configuré&nbsp;:

```
$ yum repolist | grep software_collections
ol7_software_collections/x86_64  [ol7_software_collections]   11,352+4,851
```

Installer PHP 7.3&nbsp;:

```
$ sudo yum install rh-php73 rh-php73-php-fpm rh-php73-php-mysqlnd
```

Éditer un fichier `/etc/httpd/conf.d/php-fpm.conf`&nbsp;:

```
# /etc/httpd/conf.d/php-fpm.conf
AddType text/html .php
DirectoryIndex index.php
<FilesMatch \.php$>
  SetHandler "proxy:fcgi://127.0.0.1:9000"
</FilesMatch>
```

Créer un lien symbolique `/usr/bin/php`&nbsp;:

```
$ cd /usr/bin/
$ sudo ln -s /opt/rh/rh-php73/root/usr/bin/php .
```

Créer un fichier `/etc/opt/rh/rh-php73/php.d/20-date.ini` pour configurer le
fuseau horaire&nbsp;:

```
[Date]
; Defines the default timezone used by the date functions
; ; http://php.net/date.timezone
date.timezone = Europe/Paris
```

Prendre en compte les modifications&nbsp;:

```
$ sudo systemctl enable rh-php73-php-fpm --now
$ sudo systemctl restart httpd
```

Éditer un fichier `/var/www/default/html/phpinfo.php` pour tester PHP&nbsp;:

```
<?php
  echo phpinfo();
?>
```

Afficher la page dans un navigateur web.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
