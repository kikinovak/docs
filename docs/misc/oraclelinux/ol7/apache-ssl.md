
Ouvrir les ports 80 et 443 dans le pare-feu&nbsp;:

```
$ sudo firewall-cmd --permanent --add-service=http
$ sudo firewall-cmd --permanent --add-service=https
$ sudo firewall-cmd --reload
```

Installer Apache, le module SSL/TLS et Certbot&nbsp;:

```
$ sudo yum install httpd mod_ssl certbot
```

Mettre en place la page par défaut du serveur&nbsp;:

```
$ cd /var/www/
$ sudo mkdir -pv default/html
mkdir: created directory ‘default’
mkdir: created directory ‘default/html’
$ sudo chown -R microlinux:microlinux default/
$ cp -v /usr/share/httpd/noindex/index.html default/html/
‘/usr/share/httpd/noindex/index.html’ -> ‘default/html/index.html’
```

Éditer la page `index.html` pour la différencier de la page de bienvenue du
serveur&nbsp;:

```
<body>
<h1>sd-100246.dedibox.fr</h1>
```

Ranger le script de génération de certificats `letsencrypt.sh` dans un endroit
approprié avec les permissions qui vont bien&nbsp;:

```
$ cd
$ mkdir -v bin
mkdir: created directory ‘bin’
$ cp -v oracle/el7/certbot/letsencrypt.sh bin/
‘oracle/el7/certbot/letsencrypt.sh’ -> ‘bin/letsencrypt.sh’
$ chmod 0700 bin/letsencrypt.sh
```

Éditer le script et renseigner l’adresse mail de notification&nbsp;:

```
EMAIL='info@microlinux.fr'
```

Renseigner le nom d’hôte du serveur pour la page par défaut et supprimer les
autres entrées&nbsp;:

```
DOMAIN[1]='sd-100246.dedibox.fr'
WEBDIR[1]='default'
```

Tester la génération du certificat&nbsp;:

```
$ sudo ./letsencrypt.sh --test
```

Générer le certificat&nbsp;:

```
$ sudo ./letsencrypt.sh --cert
```

Sauvegarder la configuration d’Apache&nbsp;:

```
$ cd /etc/httpd/conf/
$ sudo mv -v httpd.conf httpd.conf.orig
‘httpd.conf’ -> ‘httpd.conf.orig’
$ cd /etc/httpd/conf.d/
$ sudo mv -v ssl.conf ssl.conf.orig
‘ssl.conf’ -> ‘ssl.conf.orig’
```

Mettre en place la configuration par défaut&nbsp;:

```
$ sudo cp -v oracle/el7/httpd/httpd.conf /etc/httpd/conf/
‘oracle/el7/httpd/httpd.conf’ -> ‘/etc/httpd/conf/httpd.conf’
$ sudo cp -v oracle/el7/httpd/ssl.conf /etc/httpd/conf.d/
‘oracle/el7/httpd/ssl.conf’ -> ‘/etc/httpd/conf.d/ssl.conf’
```

Adapter la configuration&nbsp;:

```
# /etc/httpd/conf/httpd.conf
ServerRoot "/etc/httpd"
Listen 80
KeepAlive on
Include conf.modules.d/*.conf
User apache
Group apache
ServerAdmin info@microlinux.fr
ServerName sd-100246.dedibox.fr
...
```

Configurer l’hôte virtuel pour la page par défaut du serveur&nbsp;:

```
# /etc/httpd/conf.d/00-sd-100246.dedibox.fr-ssl.conf
#
# http://sd-100246.dedibox.fr -> https://sd-100246.dedibox.fr
<VirtualHost *:80>
  ServerName sd-100246.dedibox.fr
  Redirect / https://sd-100246.dedibox.fr
</VirtualHost>

# https://sd-100246.dedibox.fr
<VirtualHost _default_:443>
  ServerAdmin info@microlinux.fr
  DocumentRoot "/var/www/default/html"
  ServerName sd-100246.dedibox.fr:443
  SSLEngine on
  SSLCertificateFile /etc/letsencrypt/live/sd-100246.dedibox.fr/cert.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/sd-100246.dedibox.fr/privkey.pem
  SSLCertificateChainFile /etc/letsencrypt/live/sd-100246.dedibox.fr/fullchain.pem
  BrowserMatch "MSIE [2-5]" \
    nokeepalive ssl-unclean-shutdown \
    downgrade-1.0 force-response-1.0
  ErrorLog logs/sd-100246.dedibox.fr-error_log
  CustomLog logs/sd-100246.dedibox.fr-access_log common
</VirtualHost>
```

Tester la configuration&nbsp;:

```
$ sudo apachectl configtest
Syntax OK
```

Activer et lancer Apache&nbsp;:

```
$ sudo systemctl enable httpd --now
```

Vérifier le bon fonctionnement du serveur&nbsp;:

```
$ systemctl status httpd
```

* Ouvrir la page par défaut dans un navigateur web.

* Lancer un [audit de qualité](https://www.ssllabs.com/ssltest/) pour le
  chiffrement.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

