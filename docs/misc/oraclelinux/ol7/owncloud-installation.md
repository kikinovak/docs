
Créer le sous-domaine `cloud.slackbox.fr`&nbsp;:

```
; /var/named/zone.slackbox.fr
$TTL 86400
$ORIGIN slackbox.fr.
@ IN SOA ns.slackbox.fr. hostmaster.slackbox.fr. (
   2021021101   ; sn
...
www     CNAME               slackbox.fr.
cloud   CNAME               slackbox.fr.
```

Prendre en compte les modifications&nbsp;:

```
$ sudo systemctl restart named
```

Préparer l'arborescence de l'hébergement&nbsp;:

```
$ cd /var/www
$ sudo mkdir -pv slackbox-owncloud/html
mkdir: created directory ‘slackbox-owncloud'
mkdir: created directory ‘slackbox-owncloud/html'
$ sudo chown -R microlinux:microlinux slackbox-owncloud/
$ cp -v /usr/share/httpd/noindex/index.html slackbox-owncloud/html/
‘/usr/share/httpd/noindex/index.html' -> ‘slackbox-owncloud/html/index.html'
```

Éditer `slackbox-owncloud/html/index.html`&nbsp;:

```
  <body>
  <h1>cloud.slackbox.fr</h1>
```
  
Éditer `~/bin/letsencrypt.sh`&nbsp;:

```
DOMAIN[1]='sd-155842.dedibox.fr'
WEBDIR[1]='default'

DOMAIN[2]='cloud.slackbox.fr'
WEBDIR[1]='slackbox-owncloud'
```

Générer le certificat&nbsp;:

```
$ sudo ./letsencrypt.sh --test
$ sudo ./letsencrypt.sh --cert
```

Configurer l'hôte virtuel&nbsp;:

```
# /etc/httpd/conf.d/10-cloud.slackbox.fr-ssl.conf

# http://cloud.slackbox.fr -> https://cloud.slackbox.fr
<VirtualHost *:80>
  ServerName cloud.slackbox.fr
  Redirect / https://cloud.slackbox.fr
</VirtualHost>

# https://cloud.slackbox.fr
<VirtualHost _default_:443>
  ServerAdmin info@microlinux.fr
  DocumentRoot "/var/www/slackbox-owncloud/html"
  <Directory "/var/www/slackbox-owncloud/html">
    Options +FollowSymlinks
    AllowOverride All
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
  ServerName cloud.slackbox.fr:443
  SSLEngine on
  SSLCertificateFile /etc/letsencrypt/live/sd-155842.dedibox.fr/cert.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/sd-155842.dedibox.fr/privkey.pem
  SSLCertificateChainFile /etc/letsencrypt/live/sd-155842.dedibox.fr/fullchain.pem
  BrowserMatch "MSIE [2-5]" \
    nokeepalive ssl-unclean-shutdown \
    downgrade-1.0 force-response-1.0
  ErrorLog logs/cloud.slackbox.fr-error_log
  CustomLog logs/cloud.slackbox.fr-access_log common
</VirtualHost>
```

* On autorise les directives placées dans le fichier `.htaccess` à la racine de
  l'installation.

* WebDAV doit être désactivé, car OwnCloud utilise son propre serveur interne
  SabreDAV.

Tester la page statique&nbsp;:

```
$ sudo apachectl configtest
Syntax OK
$ sudo systemctl reload httpd
```

Afficher la page `https://cloud.slackbox.fr` dans un navigateur.

Tester la sécurité de l'hébergement&nbsp;:
[https://www.ssllabs.com/ssltest/](https://www.ssllabs.com/ssltest/) 

OwnCloud doit pouvoir envoyer des mails via Postfix&nbsp;:

```
$ sudo setsebool -P httpd_can_sendmail on
```

Apache possède les droits d'écriture sur une partie de l'arborescence
OwnCloud&nbsp;:

```
$ sudo setsebool -P httpd_unified on
```

OwnCloud doit pouvoir se connecter au serveur Redis&nbsp;:

```
$ sudo setsebool -P httpd_can_network_connect on
```

Installer les modules PHP&nbsp;:

* `rh-php73-php-gd`

* `rh-php73-php-mbstring`

* `rh-php73-php-opcache`

* `rh-php73-php-intl`

* `rh-php73-php-pecl-apcu`

* `sclo-php73-php-pecl-redis5`

Relancer PHP-FPM et Apache&nbsp;:

```
$ sudo systemctl restart rh-php73-php-fpm httpd
```

Télécharger OwnCloud&nbsp;:

```
$ mkdir -pv ~/webapps/owncloud
mkdir: created directory ‘/home/microlinux/webapps'
mkdir: created directory ‘/home/microlinux/webapps/owncloud'
$ cd ~/webapps/owncloud/
$ wget -c https://download.owncloud.org/community/owncloud-10.5.0.tar.bz2
```

Créer la base de données&nbsp;:

```
$ mysql -u root -p
mysql> create database `slackbox-owncloud`;
mysql> grant all on `slackbox-owncloud`.*
    -> to slackboxuser@localhost 
    -> identified by '********';
mysql> flush privileges;
mysql> quit;
```

Décompresser l'archive téléchargée&nbsp;:

```
$ cd /var/www/slackbox-owncloud/
$ rm -rf html/
$ tar -xjf ~/webapps/owncloud/owncloud-10.5.0.tar.bz2 
$ ls
owncloud
$ mv -v owncloud/ html
‘owncloud/' -> ‘html'
```

Ranger les scripts de configuration à l'endroit approprié&nbsp;:

```
$ cd
$ cp -v oracle/el7/owncloud/10.5.0/*.sh bin/
‘oracle/el7/owncloud/10.5.0/instance.sh' -> ‘bin/instance.sh'
‘oracle/el7/owncloud/10.5.0/owncloud_prep.sh' -> ‘bin/owncloud_prep.sh'
```

Définir les permissions&nbsp;:

```
$ cd bin/
$ chmod 0700 instance.sh owncloud_prep.sh
```

Éditer `instance.sh` en fonction de l'hébergement&nbsp;:

```
ocname='html'
ocroot='/var/www/slackbox-owncloud'

linkroot='/var/www/slackbox-owncloud'

htuser='apache'
htgroup='apache'
rootuser='microlinux'
```

Lancer le script&nbsp;:

```
$ sudo ./instance.sh 
...
Do you want to secure your .htaccess files post installing/upgrade (y/N)? n
Do you want to install a new instance (y/N)? n
Do you want to upgrade an existing installation (y/N)? n
Use links for data and apps-external directories (Y/n)? y
Do you want to chmod/chown these links (y/N)? y
```

Dans cette configuration, `data/` et `apps-external/` sont extérieurs à
l'arborescence&nbsp;:

```
$ cd /var/www/slackbox-owncloud/
$ ls -l
drwxr-x---.  2 apache     apache 4096 Feb 12 11:02 apps-external
drwxr-x---.  4 apache     apache 4096 Feb 12 11:06 data
drwxr-x---. 12 microlinux apache 4096 Feb 12 11:02 html
$ ls -l html/data
lrwxrwxrwx. ... html/data -> /var/www/slackbox-owncloud/data
$ ls -l html/apps-external
lrwxrwxrwx. ... html/apps-external -> /var/www/slackbox-owncloud/apps-external
```

Ouvrir l'assistant d'installation dans l'interface web d'OwnCloud&nbsp;:

* Définir un compte administrateur.

* Choisir `MySQL/MariaDB` comme serveur de bases de données.

* Renseigner les paramètres de connexion à la base, en précisant le port `3306`.

* Cliquer sur `Terminer l'installation`.

* Se connecter à OwnCloud avec l'identifiant et le mot de passe que l'on vient
  de définir.

Ouvrir `Paramètres` > `Administration` > `Généraux`.

Passer le canal de mise à jour de `stable` à `production`.

Éditer `config/config.php` pour configurer le cache APCu&nbsp;:

```
'dbpassword' => '********',
'logtimezone' => 'UTC',
'memcache.local' => '\OC\Memcache\APCu',
'apps_paths' => ...
```

Installer le serveur Redis&nbsp;:

```
$ sudo yum install rh-redis5-redis
```

Activer et démarrer le serveur Redis&nbsp;:

```
$ sudo systemctl enable rh-redis5-redis --now
```

Éditer `config/config.php` pour configurer le verrouillage transactionnel des
fichiers&nbsp;:

```
'logtimezone' => 'UTC',
'memcache.local' => '\OC\Memcache\APCu',
'memcache.locking' => '\OC\Memcache\Redis',
'redis' => [
  'host' => 'localhost',
  'port' => '6379',
 ],
'apps_paths' => ...
```

Éditer une tâche automatisée qui lance les tâches planifiées toutes les 15
minutes&nbsp;:

```
$ sudo crontab -u apache -e
*/15 * * * * /usr/bin/php /var/www/slackbox-owncloud/html/occ system:cron
```

Définir cette tâche dans `Paramètres` > `Généraux` > `Administration` > `Cron`
&gt; `Service cron du système`.  

Vérifier si tous les tests ont réussi dans les `Avertissements de sécurité &
configuration`.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*


