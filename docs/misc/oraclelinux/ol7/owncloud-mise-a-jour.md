
Mise à jour OwnCloud 10.5.0 vers OwnCloud 10.6.0.

Télécharger la nouvelle version&nbsp;:

```
$ cd ~/webapps/owncloud
$ wget -c https://download.owncloud.org/community/owncloud-10.6.0.tar.bz2
```

Vérifier si `data/` et `apps-external/` sont extérieurs à l'arborescence&nbsp;:

```
$ cd /var/www/slackbox-owncloud/html/
$ ls -l data apps-external
lrwxrwxrwx. ... apps-external -> ../apps-external/
lrwxrwxrwx. ... data -> ../data/
```

Le cas échéant, rectifier le tir&nbsp;:

```
$ sudo mv data/ ../
$ sudo mv apps-external/ ../
$ ln -s ../data/ .
$ ln -s ../apps-external/ .
$ sudo chown apache:apache data apps-external
```

Mettre à jour les applications dans la section `Market` de l'interface
graphique.

Désactiver les tâches automatiques&nbsp;:

```
$ sudo crontab -u apache -e
# */15 * * * * /usr/bin/php /var/www/bsco-owncloud/html/occ system:cron
```

Activer le mode maintenance&nbsp;:

```
$ sudo -u apache php occ maintenance:mode --on
```

Renommer l'installation existante et installer la mise à jour&nbsp;:

```
$ cd /var/www/slackbox-owncloud/
$ sudo mv -v html/ html.bak
‘html/' -> ‘html.bak'
$ tar -xjf ~/webapps/owncloud/owncloud-10.6.0.tar.bz2 
$ mv -v owncloud/ html
‘owncloud/' -> ‘html'
```

Copier la configuration existante vers la nouvelle installation&nbsp;:

```
$ sudo cp -av html.bak/config/config.php html/config/
‘html.bak/config/config.php' -> ‘html/config/config.php'
```

Recréer les liens vers `data/` et `apps-external/`&nbsp;:

```
$ cd html
$ ln -s ../data/ .
$ ln -s ../apps-external/ .
$ cd -
```

Définir des permissions provisoires "à la louche" pour la mise à jour&nbsp;:

```
$ sudo chown -R apache:apache html/
```

Démarrer la mise à jour interne&nbsp;:

```
$ sudo -u apache php html/occ upgrade
```

Rétablir les permissions normales&nbsp;:

```
$ cd ~/bin/
$ sudo ./instance.sh 
...
Do you want to secure your .htaccess files post installing/upgrade (y/N)? n
Do you want to install a new instance (y/N)? n
Do you want to upgrade an existing installation (y/N)? n
Use links for data and apps-external directories (Y/n)? y
Do you want to chmod/chown these links (y/N)? y
```

> Les scripts `instance.sh` et `owncloud_prep.sh` de la version 10.6.0 sont
> identiques à ceux de la version 10.5.0.

Quitter le mode maintenance&nbsp;:

```
$ cd /var/www/slackbox-owncloud/html/
$ sudo -u apache php occ maintenance:mode --off
```

Supprimer l'ancienne installation&nbsp;:

```
$ cd /var/www/slackbox-owncloud/
$ sudo rm -rf html.bak/
```

Réactiver les tâches automatiques&nbsp;:

```
$ sudo crontab -u apache -e
*/15 * * * * /usr/bin/php /var/www/bsco-owncloud/html/occ system:cron
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
