
Oracle Linux ne fait pas partie des OS proposés par Scaleway. On va donc
installer CentOS dans un premier temps, puis migrer cette installation fraîche
vers Oracle Linux.

## Choix de l'OS

Procéder au choix de la machine et du système d'exploitation&nbsp;:

* Se connecter à la [console Online](https://console.online.net).

* Ouvrir le menu `Serveur` > `Liste des serveurs`.

* Sélectionner la machine > `Administrer` > `Installer`.

* `Distributions serveur` > `CentOS` > `CentOS 7 64bits` > `Installer CentOS`.

## Partitionnement

Modifier le schéma de partitionnement proposé par défaut&nbsp;:

* Réduire la taille de la partition principale pour avoir un peu de marge.

* Augmenter la taille de la partition `/boot`&nbsp;: `500 Mo`.

* La partition `/boot` sera formatée en `ext2`.

* Augmenter la taille de la partition d'échange en fonction de la RAM
  disponible.

* Remplir l'espace disponible pour la partition principale qui reste en `ext4`.

## Définition des utilisateurs

* Choisir le mot de passe `root`.

* Définir un utilisateur normal, par exemple `microlinux`.

* Choisir un mot de passe pour cet utilisateur.

> L'installateur impose une limite de 15 caractères alphanumériques.

## Paramètres réseau

L'interface affiche un récapitulatif des paramètres réseau de la machine&nbsp;:

* `Nom d'hôte` : `sd-155842`

* `Adresse IP` : `51.158.146.161`

* `Masque de sous-réseau` : `255.255.255.0`

* `IP de la passerelle` : `51.158.146.1`

* `DNS primaire` : `62.210.16.6`

* `DNS secondaire` : `62.210.16.7`

## Installation de CentOS

Cliquer sur `Effacer l'intégralité de mes disques durs` pour procéder à
l'installation.

> L'installation du système initial dure une petite heure.

## Comptes et connexion

Configurer l'authentification par clé SSH&nbsp;:

```
$ ssh-copy-id -i .ssh/id_rsa.pub microlinux@sd-155842.dedibox.fr
```

L'utilisateur initial créé par l'installateur ne peut pas utiliser la commande
`sudo`&nbsp;:

```
$ su -
# usermod -aG wheel microlinux
# exit
$ exit
```

Redéfinir les mots de passe pour l'utilisateur initial et pour `root`&nbsp;:

```
$ passwd
$ sudo passwd root
```

## Migrer vers Oracle Linux

Installer Git&nbsp;:

```
$ sudo yum install git
```

Récupérer le script de migration&nbsp;:

```
$ git clone https://github.com/oracle/centos2ol
```

Exécuter le script&nbsp;:

```
$ cd centos2ol/
$ sudo ./centos2ol.sh -rkV
$ sudo reboot
```

* L'option `-r` remplace tous les paquets CentOS par des paquets Oracle.

* L'option `-k` n'installe pas le noyau UEK et désactive le dépôt correspondant.

* L'option `-V` vérifie les informations RPM avant et après la migration.

> Compter près de 20 minutes sur une Dedibox Start SSD.

Faire le ménage dans les dépôts&nbsp;:

```
$ sudo rm -f /etc/yum.repos.d/*.disabled
```

Afficher les paquets qui n'appartiennent à aucun dépôt d'Oracle&nbsp;:

```
$ yum list extras
```

Supprimer ces paquets&nbsp;:

```
$ sudo yum remove centos-indexhtml kpatch
```

## Configuration post-installation

* [Configuration automatique](configuration-automatique.md)

* [NetworkManager sur un serveur dédié](networkmanager-dedibox.md)

* [Activer SELinux sur un serveur dédié](selinux-dedibox.md)

* [FirewallD sur un serveur dédié](firewalld-dedibox.md)

* [Protéger SSH avec Fail2ban](fail2ban-ssh.md)

* [Synchronisation NTP avec Chrony](ntp-chrony.md)

* [Postfix minimal sur un serveur dédié](postfix-minimal-dedibox.md)

* [Mises à jour automatiques](yum-cron.md)

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
