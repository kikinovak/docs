
* Démarrer sur le DVD, le CD-Rom ou la clé USB.
* Au démarrage, sélectionner `Troubleshooting`.
* Mettre l'entrée `Rescue an Oracle system` en surbrillance.
* Appuyer sur ++tab++ (++e++ sur un système UEFI) et ajouter les options `nomodeset vga=791`.
* Confirmer par ++enter++ (++ctrl+x++ sur un système UEFI) pour démarrer.
* Une fois que le système a démarré, choisir l'option `3) Skip to shell`.
* Basculer vers le clavier suisse romand : `loadkeys fr_CH-latin1`.

Afficher les interfaces réseau reconnues :

```
# ip addr
```

Activer l'interface réseau en question, par exemple :

```
# dhclient enp2s0
# ip addr
```

Lancer un serveur SSH

```
# cd /etc/ssh
# cp sshd_config.anaconda sshd_config
# systemctl start sshd
```

La connexion se fait sans mot de passe.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

