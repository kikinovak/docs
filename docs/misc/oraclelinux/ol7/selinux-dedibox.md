
SELinux est désactivé dans la configuration par défaut chez Scaleway.

Dans un premier temps, activer SELinux en mode permissif&nbsp;:

```
# /etc/selinux/config
SELINUX=permissive
SELINUXTYPE=targeted
```

Procéder à un réétiquetage du système de fichiers&nbsp;:

```
$ sudo touch /.autorelabel
$ sudo reboot
```

> Compter cinq bonnes minutes pour un réétiquetage complet.

Lancer un audit SELinux&nbsp;:

```
$ sudo sealert -a /var/log/audit/audit.log
```

Éventuellement, restaurer le contexte par défaut comme indiqué&nbsp;:

```
$ sudo restorecon -v /etc/ld.so.cache
```

Vérifier si l’intervention a réglé le problème&nbsp;:

```
$ echo | sudo tee /var/log/audit/audit.log
$ sudo reboot
```

Relancer un audit&nbsp;:

```
$ sudo sealert -a /var/log/audit/audit.log
100% done
found 0 alerts in /var/log/audit/audit.log
```

Passer en mode renforcé par défaut&nbsp;:

```
# /etc/selinux/config
SELINUX=enforcing
SELINUXTYPE=targeted
```

Activer le mode renforcé&nbsp;:

```
$ sudo setenforce 1
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

