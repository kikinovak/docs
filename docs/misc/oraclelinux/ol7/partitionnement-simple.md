
## BIOS traditionnel  + table de partitions MBR

Utiliser `fdisk` dans la [console de secours](console-de-secours.md) :

```
# fdisk -l /dev/sda 
... 
   Device Boot   Start        End    Blocks Id  System 
/dev/sda1   *     2048    1026047    512000 83  Linux 
/dev/sda2      1026048    9414655   4194304 82  Linux swap / Solaris 
/dev/sda3      9414656  125829119  58207232 83  Linux
```

Formater la partition `/boot` :

* Sélectionner `/dev/sda1`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext2` et l'étiquette `boot`.

* Définir le point de montage `/boot`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `swap` :

* Sélectionner `/dev/sda2`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `swap` et l'étiquette `swap`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition principale :

* Sélectionner `/dev/sda3`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `root`.

* Définir le point de montage `/`.

* Confirmer `Mise à jour des paramètres`, puis `Terminé`.

## BIOS traditionnel + table de partitions GPT

Utiliser `gdisk` dans la [console de secours](console-de-secours.md) :

```
# gdisk -l /dev/sda 
...
Number  Start (sector)  End (sector)  Size       Code  Name
   1            2048          4095   1024.0 KiB  EF02  BIOS boot partition
   2            4096       1028095   500.0 MiB   8300  Linux filesystem
   3         1028096       9416703   4.0 GiB     8200  Linux swap
   4         9416704     125829086   55.5 GiB    8300  Linux filesystem
```

Configurer la partition d'amorçage BIOS :

* Sélectionner `/dev/sda1`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `BIOS Boot`.

* Confirmer `Mise à jour des paramètres`.

> La partition n'est pas formatée à proprement parler.

Formater la partition `/boot` :

* Sélectionner `/dev/sda2`.

* Cliquer sur `Reformate`.

* Choisir le système de fichiers `ext2` et l'étiquette `boot`.

* Définir le point de montage `/boot`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `swap` :

* Sélectionner `/dev/sda3`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `swap` et l'étiquette `swap`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition principale :

* Sélectionner `/dev/sda4`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `root`.

* Définir le point de montage `/`.

* Confirmer `Mise à jour des paramètres`, puis `Terminé`.

## UEFI + table de partitions GPT

Utiliser `gdisk` dans la [console de secours](console-de-secours.md) :

```
# gdisk -l /dev/sda 
...
Number  Start (sector) End (sector)  Size       Code  Name
   1            2048       411647   200.0 MiB   EF00  EFI System Partition
   2          411648      1435647   500.0 MiB   8300  Linux filesystem
   3         1435648      9824255   4.0 GiB     8200  Linux swap
   4         9824256    125829086   55.3 GiB    8300  Linux filesystem
```

Formater la partition EFI :

* Sélectionner `/dev/sda1`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `EFI System Partition` et l'étiquette `EFI`.

* Définir le point de montage `/boot/efi`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `/boot` :

* Sélectionner `/dev/sda2`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext2` et l'étiquette `boot`.

* Définir le point de montage `/boot`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `swap` :

* Sélectionner `/dev/sda3`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `swap` et l'étiquette `swap`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition principale :

* Sélectionner `/dev/sda4`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `root`.

* Définir le point de montage `/`.

* Confirmer `Mise à jour des paramètres`, puis `Terminé`.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

