
## BIOS traditionnel  + table de partitions MBR

Utiliser `fdisk` dans la [console de secours](console-de-secours.md) et
partitionner le premier disque&nbsp;:

```
# fdisk -l /dev/sda
...
   Device Boot   Start        End    Blocks  Id  System
/dev/sda1   *     2048    1026047    512000  fd  Linux raid autodetect
/dev/sda2      1026048    9414655   4194304  fd  Linux raid autodetect
/dev/sda3      9414656  125829119  58207232  fd  Linux raid autodetect
```

Cloner le schéma de partitionnement :

```
# sfdisk --dump /dev/sda | sfdisk /dev/sdb
# fdisk -l /dev/sdb
```

Assembler les grappes RAID :

```
# mdadm --create /dev/md/boot --level=1 --raid-devices=2 \
  --metadata=1.2 /dev/sda1 /dev/sdb1
mdadm: array /dev/md/boot started
# mdadm --create /dev/md/swap --level=1 --raid-devices=2 \
  --metadata=1.2 /dev/sda2 /dev/sdb2
mdadm: array /dev/md/swap started
# mdadm --create /dev/md/root --level=1 --raid-devices=2 \
  --metadata=1.2 /dev/sda3 /dev/sdb3
mdadm: array /dev/md/root started
```

Formater la partition `/boot` :

* Sélectionner la grappe RAID `boot`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext2` et l'étiquette `boot`.

* Définir le point de montage `/boot`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `swap` :

* Sélectionner la grappe RAID `swap`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `swap` et l'étiquette `swap`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition principale :

* Sélectionner la grappe RAID `root`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `root`.

* Définir le point de montage `/`.

* Confirmer `Mise à jour des paramètres`, puis `Terminé`.

## BIOS traditionnel + table de partitions GPT

Utiliser `gdisk` dans la [console de secours](console-de-secours.md) et
partitionner le premier disque&nbsp;:

```
# gdisk -l /dev/sda
...
Number  Start (sector) End (sector)  Size       Code  Name
   1            2048         4095   1024.0 KiB  EF02  BIOS boot partition
   2            4096      1028095   500.0 MiB   FD00  Linux RAID
   3         1028096      9416703   4.0 GiB     FD00  Linux RAID
   4         9416704    125829086   55.5 GiB    FD00  Linux RAID
```

Cloner le schéma de partitionnement :

```
# sgdisk /dev/sda --replicate /dev/sdb
# sgdisk --randomize-guids /dev/sdb
# gdisk -l /dev/sdb
```

Assembler les grappes RAID :

```
# mdadm --create /dev/md/boot --level=1 --raid-devices=2 \
  --metadata=1.2 /dev/sda2 /dev/sdb2
mdadm: array /dev/md/boot started
# mdadm --create /dev/md/swap --level=1 --raid-devices=2 \
  --metadata=1.2 /dev/sda3 /dev/sdb3
mdadm: array /dev/md/swap started
# mdadm --create /dev/md/root --level=1 --raid-devices=2 \
  --metadata=1.2 /dev/sda4 /dev/sdb4
mdadm: array /dev/md/root started
```

Configurer les partitions `BIOS Boot` :

* Sélectionner `/dev/sda1`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `BIOS Boot`.

* Confirmer `Mise à jour des paramètres`.

* Procéder de même pour `/dev/sdb1`.

> Les partitions ne sont pas formatées à proprement parler.

Formater la partition `/boot` :

* Sélectionner la grappe RAID `boot`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext2` et l'étiquette `boot`.

* Définir le point de montage `/boot`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `swap` :

* Sélectionner la grappe RAID `swap`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `swap` et l'étiquette `swap`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition principale :

* Sélectionner la grappe RAID `root`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `root`.

* Définir le point de montage `/`.

* Confirmer `Mise à jour des paramètres`, puis `Terminé`.

## UEFI + table de partitions GPT

Utiliser `gdisk` dans la [console de secours](console-de-secours.md) et
partitionner le premier disque&nbsp;:

```
# gdisk -l /dev/sda
...
Number  Start (sector) End (sector)  Size       Code  Name
   1            2048       411647   200.0 MiB   FD00  Linux RAID
   2          411648      1435647   500.0 MiB   FD00  Linux RAID
   3         1435648      9824255   4.0 GiB     FD00  Linux RAID
   4         9824256    125829086   55.3 GiB    FD00  Linux RAID
```

Cloner le schéma de partitionnement :

```
# sgdisk /dev/sda --replicate /dev/sdb
# sgdisk --randomize-guids /dev/sdb
# gdisk -l /dev/sdb
```

Assembler les grappes RAID :

```
# mdadm --create /dev/md/efi --level=1 --raid-devices=2 \
  --metadata=1.0 /dev/sda1 /dev/sdb1
mdadm: array /dev/md/efi started
# mdadm --create /dev/md/boot --level=1 --raid-devices=2 \
  --metadata=1.2 /dev/sda2 /dev/sdb2
mdadm: array /dev/md/boot started
# mdadm --create /dev/md/swap --level=1 --raid-devices=2 \
  --metadata=1.2 /dev/sda3 /dev/sdb3
mdadm: array /dev/md/swap started
# mdadm --create /dev/md/root --level=1 --raid-devices=2 \
  --metadata=1.2 /dev/sda4 /dev/sdb4
mdadm: array /dev/md/root started
```

Formater la partition `EFI` :

* Sélectionner la grappe RAID `efi`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `EFI System Partition` et l'étiquette `EFI`.

* Définir le point de montage `/boot/efi`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `/boot` :

* Sélectionner la grappe RAID `boot`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext2` et l'étiquette `boot`.

* Définir le point de montage `/boot`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition `swap` :

* Sélectionner la grappe RAID `swap`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `swap` et l'étiquette `swap`.

* Confirmer `Mise à jour des paramètres`.

Formater la partition principale :

* Sélectionner la grappe RAID `root`.

* Cliquer sur `Reformater`.

* Choisir le système de fichiers `ext4` et l'étiquette `root`.

* Définir le point de montage `/`.

* Confirmer `Mise à jour des paramètres`, puis `Terminé`.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

