
Démarrer la [console de secours](console-de-secours.md).

Afficher les partitions :

```
# lsblk
```

Désactiver les assemblages RAID provenant d'une installation antérieure&nbsp;:

```
# mdadm --stop --scan
```

Effacer les métadonnées RAID persistantes sur les partitions&nbsp;:

```
# mdadm --zero-superblock /dev/sda1
# mdadm --zero-superblock /dev/sda2
# mdadm --zero-superblock /dev/sda3
# mdadm --zero-superblock /dev/sdb1
# mdadm --zero-superblock /dev/sdb2
# mdadm --zero-superblock /dev/sdb3
...
```

Supprimer les tables de partitions :

```
# sgdisk --zap-all /dev/sda
# sgdisk --zap-all /dev/sdb
...
```

Alternativement :

```
# wipefs --all /dev/sda
# wipefs --all /dev/sdb
...
```

Détruire les données d'un disque conformément au standard *5220-22M US Dept. of Defense*&nbsp;:

```
# shred -vzn 8 /dev/sda
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
