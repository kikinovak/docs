
Afficher les interfaces réseau gérées par NetworkManager&nbsp;:

```
$ nmcli dev
DEVICE  TYPE      STATE     CONNECTION 
enp2s0  ethernet  connecté  enp2s0     
lo      loopback  non-géré  --     
```

Configurer la connexion&nbsp;:

```
$ sudo nmtui
```

* Sélectionner `Edit a connection`.

* Éditer la connexion.

* Remplacer le nom de profil par `LAN`.

* Passer `IPv4 CONFIGURATION` de `Automatic` à `Manual`.

* Cliquer sur `Show` pour afficher les détails.

* Fournir l'adresse IP du serveur en notation CIDR, par exemple
  `192.168.2.3/24`.

* Renseigner l'adresse IP de la passerelle dans le champ `Gateway`.

* Ne rien indiquer dans les champs `DNS server` et `Search domains`.

* Passer `IPv6 CONFIGURATION` de `Automatic` à `Ignore`.

* Confirmer par `OK`.

* Revenir à la fenêtre principale.

* Activer la connexion `LAN`&nbsp;: `Activate a connection` > `LAN`

* Quitter NetworkManager TUI&nbsp;: `Quit`

Renseigner le ou les serveurs DNS&nbsp;:

```
# /etc/resolv.conf
search microlinux.lan
nameserver 192.168.2.1
```

Corriger la configuration du nom d'hôte&nbsp;:

```
# /etc/hosts
127.0.0.1   localhost.localdomain localhost
192.168.2.3 sandbox.microlinux.lan sandbox
```

Le fichier `/etc/hostname` devra juste contenir le nom d'hôte simple sans le
domaine&nbsp;:

```
sandbox
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
