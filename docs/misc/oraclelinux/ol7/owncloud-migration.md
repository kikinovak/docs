
**Exemple pratique**&nbsp;: migrer `cloud.aujardin.com` vers
`cloud.slackbox.fr`.

Arrêter l'hébergement pour l'hôte virtuel.

Exporter la base&nbsp;:

```
$ mysqldump -u root -p aujardin-owncloud > sql/aujardin-owncloud.sql
```

Transférer le fichier de sauvegarde&nbsp;:

```
$ rsync -av sql/aujardin-owncloud.sql sandbox.microlinux.fr:/home/microlinux/sql/
```

Transférer l'arborescence&nbsp;:

```
$ cd /var/www/aujardin-owncloud/
$ sudo rsync -av html sandbox.microlinux.fr:/var/www/slackbox-owncloud/
```

> Attention&nbsp;: pas de `/` final à `html`&nbsp;!

Créer la base de données vide&nbsp;:

```
$ mysql -u root -p
mysql> create database `slackbox-owncloud`;
mysql> grant all on `slackbox-owncloud`.*
    -> to slackboxuser@localhost 
    -> identified by '********';
mysql> flush privileges;
mysql> quit;
```

Importer la base sauvegardée&nbsp;:

```
$ mysql -u root -p slackbox-owncloud < sql/aujardin-owncloud.sql
```

Éditer `config/config.php` et renseigner les valeurs correctes pour les
variables&nbsp;:

* `trusted_domains`

* `datadirectory`

* `overwrite.cli.url`

* `dbname`

* `dbuser`

* `dbpassword`

* `apps_paths`

Activer le mode maintenance&nbsp;:

```
$ cd /var/www/slackbox-owncloud/html
$ sudo -u apache php occ maintenance:mode --on
```

Corriger les chemins codés en dur dans la base de données&nbsp;:

```
$ mysql -u root -p
mysql> use `slackbox-owncloud`;
mysql> select * from oc_storages;
mysql> update oc_storages
    -> set id='local::/var/www/slackbox-owncloud/html/data/'
    -> where id='local::/var/www/aujardin-owncloud/html/data/';
mysql> select user_id, home from oc_accounts;
mysql> update oc_accounts
    -> set home = replace(home,
    -> '/var/www/aujardin-owncloud/',
    -> '/var/www/slackbox-owncloud/');
mysql> quit;
```

Désactiver le mode maintenance&nbsp;:

```
$ sudo -u apache php occ maintenance:mode --off
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*


