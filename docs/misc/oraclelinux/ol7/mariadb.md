
Installer MariaDB&nbsp;:

```
$ sudo yum install mariadb-server
```

Activer et démarrer le service&nbsp;:

```
$ sudo systemctl enable mariadb --now
```

Lancer la sécurisation de MariaDB&nbsp;:

```
$ sudo mysql_secure_installation
...
Enter current password for root (enter for none): [Entrée]
...
Set root password? [Y/n] y
New password: **********
Re-enter new password: **********
...
Remove anonymous users? [Y/n] y
...
Disallow root login remotely? [Y/n] y
...
Reload privilege tables now? [Y/n] y
```

Ouvrir la console MariaDB avec le mot de passe que l'on vient de définir&nbsp;:

```
$ mysql -u root -p
```

Utiliser la base de données `mysql`&nbsp;:

```
MariaDB [(none)]> use mysql;
Database changed
```

Garder la seule entrée pour `root@localhost` et supprimer les deux
autres&nbsp;:

```
MariaDB [mysql]> delete from user where host!='localhost';
Query OK, 2 rows affected (0.00 sec)
```

Vérifier le résultat de l'opération&nbsp;:

```
MariaDB [mysql]> select user, host, password from user;
+------+-----------+-------------------------------------------+
| user | host      | password                                  |
+------+-----------+-------------------------------------------+
| root | localhost | *6883419C147A759B04D78A2D1E4E0C5BB0CDD1B4 |
+------+-----------+-------------------------------------------+
1 row in set (0.00 sec)
```

Quitter la console&nbsp;:

```
MariaDB [mysql]> quit;
Bye
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
