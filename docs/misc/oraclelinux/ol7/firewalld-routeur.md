
Le serveur dispose de deux interfaces `enp2s0` (`WAN`) et `enp3s1` (`LAN`)
associées à la zone `public`&nbsp;:

```
$ nmcli con show
NAME  UUID                                  TYPE      DEVICE 
WAN   a46043b5-c71d-3795-9548-96e5c136f942  ethernet  enp2s0 
LAN   9667c35b-5f84-4de8-9300-f4144a6db1c6  ethernet  enp3s1
$ firewall-cmd --get-active-zones
public
  interfaces: enp2s0 enp3s1
```

Associer la zone `external` à l'interface `enp2s0`&nbsp;:

```
$ sudo firewall-cmd --permanent --zone=external --change-interface=enp2s0
```

Associer la zone `internal` à l'interface `enp3s1`&nbsp;:

```
$ sudo firewall-cmd --permanent --zone=internal --change-interface=enp3s1
```

Définir la zone `internal` comme zone par défaut&nbsp;:

```
$ sudo firewall-cmd --set-default-zone=internal
```

Supprimer quelques services prédéfinis&nbsp;:

```
$ sudo firewall-cmd --permanent --remove-service=dhcpv6-client
$ sudo firewall-cmd --permanent --remove-service=mdns
$ sudo firewall-cmd --permanent --remove-service=samba-client
$ sudo firewall-cmd --reload
```

Redémarrer le serveur et vérifier successivement la configuration des
zones&nbsp;:

```
$ sudo firewall-cmd --list-all --zone=internal
$ sudo firewall-cmd --list-all --zone=external
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
