
Mettre à jour le système&nbsp;:

```
$ sudo yum update
```

Afficher les dépôts&nbsp;:

```
$ yum repolist
```

Afficher les paquets correspondants&nbsp;:

```
$ rpm -qa | grep release
```

Supprimer tous les dépôts tiers&nbsp;:

```
$ sudo yum remove elrepo-release
$ sudo yum remove icinga-rpm-release
$ sudo yum remove epel-release
$ sudo yum remove centos-release-scl
$ sudo yum remove centos-release-scl-rh
$ sudo rm -f /etc/yum.repos.d/*.rpmsave
$ sudo rm -f /etc/yum.repos.d/docker-ce.repo
$ sudo rm -f /etc/yum.repos.d/lynis.repo
```

Vérifier si l'on ne dispose que des dépôts officiels&nbsp;:

```
$ yum repolist
...
repo id                   repo name               status
base/7/x86_64             [base]                  10,072
extras/7/x86_64           [extras]                   448
updates/7/x86_64          [updates]                1,155
```

Récupérer le script de migration&nbsp;:

```
$ git clone https://github.com/oracle/centos2ol
```

Exécuter le script&nbsp;:

```
$ cd centos2ol/
$ sudo ./centos2ol.sh -rkV
```

* L'option `-r` remplace tous les paquets CentOS par des paquets Oracle.

* L'option `-k` n'installe pas le noyau UEK et désactive le dépôt
  correspondant.

* L'option `-V` vérifie les informations RPM avant et après la migration.

> Compter entre 20 minutes et une bonne heure selon la configuration.

Faire le ménage dans les fichiers de configuration&nbsp;:

```
$ find /etc/ -name *.rpmnew 2> /dev/null | xargs sudo rm -f
$ sudo rm -f /etc/yum.repos.d/*.disabled
```

Redémarrer&nbsp;:

```
$ sudo reboot
```

Récupérer mon script de configuration post-installation&nbsp;:

```
$ git clone https://gitlab.com/kikinovak/oracle.git
$ cd oracle/
```

Configurer les dépôts officiels et tiers&nbsp;:

```
$ sudo ./linux-setup.sh --repos
```

Mettre à jour le système et installer quelques dépendances
supplémentaires&nbsp;:

```
$ sudo ./linux-setup.sh --fresh
```

Installer quelques paquets manquants pour un système de base cohérent&nbsp;:

```
$ sudo ./linux-setup.sh --extra
```

Supprimer les paquets inutiles&nbsp;:

```
$ sudo ./linux-setup.sh --strip
```

Afficher les paquets qui n'appartiennent à aucun dépôt d'Oracle&nbsp;:

```
$ yum list extras
```

Supprimer ces paquets&nbsp;:

```
$ sudo yum remove centos-indexhtml kpatch
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
