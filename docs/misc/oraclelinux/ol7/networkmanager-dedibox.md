
Afficher les interfaces réseau gérées par NetworkManager&nbsp;:

```
$ nmcli dev
DEVICE  TYPE      STATE     CONNECTION  
eth0    ethernet  connecté  System eth0 
lo      loopback  non-géré  --   
```

Configurer la connexion&nbsp;:

```
$ sudo nmtui
```

* Éditer la connexion existante.

* Remplacer le nom de profil par `WAN`.

* Passer `IPv4 CONFIGURATION` de `Automatic` à `Manual`.

* Cliquer sur `Show` pour afficher les détails.

* Fournir l'adresse IP du serveur en notation CIDR, par exemple `163.172.220.174/24`.

* Renseigner l'adresse IP de la passerelle dans le champ `Gateway`.

* Ne rien indiquer dans les champs `DNS server` et `Search domains`.

* Passer `IPv6 CONFIGURATION` de `Automatic` à `Ignore`.

* Confirmer par `OK`.

* Revenir à la fenêtre principale.

* Activer la connexion `WAN`&nbsp;: `Activate a connection` > `WAN`.

* Quitter NetworkManager TUI&nbsp;: `Quit`

> Comment empêcher NetworkManager d'écraser `/etc/resolv.conf` au premier
> redémarrage ?

Renseigner les deux serveurs DNS&nbsp;:

```
# /etc/resolv.conf
nameserver 62.210.16.6
nameserver 62.210.16.7
```

Corriger la configuration du nom d'hôte&nbsp;:

```
# /etc/hosts
127.0.0.1       localhost.localdomain localhost
163.172.220.174 sd-100246.dedibox.fr sd-100246
```

Le fichier `/etc/hostname` devra juste contenir le nom d'hôte simple&nbsp;:

```
sd-100246
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

