
Installer le service de mises à jour automatiques&nbsp;:

```
$ sudo yum install yum-cron
```

Adapter la configuration suivante&nbsp;:

```
# /etc/yum/yum-cron.conf

[commands]
update_cmd = default
update_messages = yes
download_updates = yes
apply_updates = yes
random_sleep = 0

[emitters]
system_name = sd-100246.dedibox.fr
emit_via = email
output_width = 79

[email]
email_from = root@localhost
email_to = info@microlinux.fr
email_host = localhost

[groups]
group_list = None
group_package_types = mandatory, default

[base]
debuglevel = -2
mdpolicy = group:main
```

Sur un serveur local&nbsp;:

```
[emitters]
system_name = sandbox.microlinux.lan
```

Activer et démarrer le service&nbsp;:

```
$ sudo systemctl enable yum-cron --now
```

Éventuellement, définir une tâche automatique pour redémarrer le serveur une
fois par semaine&nbsp;:

```
$ sudo crontab -l
# Reboot every monday at 4:30 AM
30 04 * * 7 /usr/sbin/shutdown -r now
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*


