
Télécharger l'ISO :

* Aller sur la page [Oracle Linux Yum Server](http://yum.oracle.com).

* Suivre les liens 
[*Get Started Here*](http://yum.oracle.com/oracle-linux-downloads.html) > 
[*ISO images*](http://yum.oracle.com/oracle-linux-isos.html).

* Repérer les images ISO de la version 7.9.

* Télécharger l'image réseau `x86_64-boot.iso` correspondante.

Donner un nom plus explicite à l'ISO :

```
$ mv x86_64-boot.iso Oracle-Linux-7.9-rhck-x86_64-netinstall.iso
```

L’image ISO est hybride et peut s’écrire directement sur une clé USB&nbsp;:

```
# dd if=Oracle-Linux-7.9-rhck-x86_64-netinstall.iso of=/dev/sdX bs=512k
```

> Si l'on installe un système virtualisé dans VirtualBox, ajouter le paramètre
de démarrage `nomodeset vga=791` pour augmenter la résolution de
l'installateur.

Dans l’écran de bienvenue, sélectionner la langue (`Français`) et la
localisation (`Français` > `France`). 

Configurer le réseau et le nom d'hôte :

* Activer le réseau.

* Vérifier l'obtention d'une adresse IP dans le réseau local.

* Choisir un nom d’hôte simple en remplacement de `localhost.localdomain` par défaut.

Configurer une adresse IP fixe :

* Cliquer sur `Configurer`.

* Ouvrir l'onglet `Ethernet`.

* Sélectionner le périphérique (`Device`), par exemple `eth0
  (52:54:00:00:E4:0C)`.

* Ouvrir l'onglet `Paramètres IPv4`.

* Passer la `Méthode` de `Automatique` à `Manuel`.

* Cliquer sur `Add` et renseigner l'adresse IP, le masque de sous-réseau et la
  passerelle.

* Renseigner un `Serveur DNS`.

* Cliquer sur `Enregistrer`.

Configurer l'installation d'un système minimal depuis le réseau&nbsp;:

* Cliquer sur `Source d'Installation`.

* Utiliser la source `Sur le réseau` par défaut.

* Indiquer la source `http://yum.oracle.com/repo/OracleLinux/OL7/latest/x86_64`.

* Ne pas cocher `Cette URL se réfère à une liste de miroirs`.

* Cliquer sur `Terminé`.

* Vérifier si la `Sélection de Logiciels` est actualisée correctement.

* Sélectionner `Installation minimale`.

Configurer la date et l'heure :

* Vérifier si le fuseau horaire (`Europe/Paris`) est correctement configuré.

* Éventuellement, activer `Heure du réseau` et vérifier les serveurs NTP.

Kdump est un mécanisme de capture lors du plantage d’un noyau. Il peut être désactivé.

Procéder au partitionnement et au formatage :

* [Partitionner un disque](partitionnement-simple.md)

* [RAID 1 avec deux disques](partitionnement-raid1.md)

* [RAID 6 avec quatre disques](partitionnement-raid6.md)

Configurer les utilisateurs :

* Créer un utilisateur normal, par exemple `microlinux`.

* Cocher la case `Faire de cet utilisateur un administrateur`. 

> L’utilisateur sera ajouté au groupe `wheel` et pourra se servir de la
commande `sudo`.

Accélérer la synchronisation inititale de la grappe RAID&nbsp;:

```
$ echo 50000 | sudo tee /proc/sys/dev/raid/speed_limit_min
```

Configuration post-installation :

* [Configuration automatique](configuration-automatique.md)

* [NetworkManager sur un serveur simple](networkmanager-lan.md)

* [NetworkManager sur un routeur](networkmanager-routeur.md)

* [FirewallD sur un serveur simple](firewalld-lan.md)

* [FirewallD sur un routeur](firewalld-routeur.md)

* [Synchronisation NTP](ntp-chrony.md)

* [Postfix minimal](postfix-minimal-lan.md)

* [Mises à jour automatiques](yum-cron.md)

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
