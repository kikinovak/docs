
# Connecter un poste client Windows&nbsp;10 à un serveur Samba

Depuis l'avènement de la version 10 de Microsoft Windows, la connexion d'un
poste client à un serveur Samba est devenue une opération inutilement
compliquée et contre-intuitive. 

> Les anciennes versions comme Windows 7 permettaient d’afficher le ou les
> serveurs Samba dans le **Voisinage Réseau** avant de se connecter très
> simplement en une paire de clics. 

* **Paramètres Windows** > **Réseau et Internet** > **Centre Réseau et
  partage**.

* **Modifier les paramètres de partage avancés** > **Activer la découverte du
  réseau** > **Enregistrer les modifications**.

* Ouvrir l'Explorateur de fichiers.

* Clic droit sur **Ce PC** > **Ajouter un emplacement réseau**.

* Cliquer sur **Suivant** dans l'Assistant d'Ajout d'un emplacement réseau.

* **Choisissez un emplacement réseau personnalisé** > **Suivant**.

* **Adresse réseau ou Internet**&nbsp;: `\\NAS\Partages`.

* Renseigner l'identifiant et le mot de passe pour la connexion au partage.

* Mémoriser les informations d'identification.

* **Entrez le nom de cet emplacement réseau**&nbsp;: `Partages(NAS)` > cliquer
  sur **Suivant**.

* Cliquer sur **Terminer**.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
