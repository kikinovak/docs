
# Installer Microsoft Windows 10

**Objectif**&nbsp;: installer Windows 10 en utilisant le support d'installation
officiel.

> Les versions plus anciennes de l'installateur Windows nécessitaient de
> réinitialiser le disque dur avant de lancer l'installation. La dernière
> version prend en charge les systèmes de fichiers et les types de partitions
> provenant d'autres systèmes d'exploitation comme Linux.

* Démarrer sur la [clé USB d'installation](telechargement.md).

* Clavier ou méthode d'entrée&nbsp;: **Français (Suisse)**

* Windows&nbsp;: **Installer maintenant**

* Activer Windows&nbsp;: **Je n'ai pas de clé de produit (Product Key)**

* Système d'exploitation à installer&nbsp;: **Windows 10 Professionnel**

* Accepter les termes du contrat de licence.

* Type d'installation&nbsp;: **installer uniquement Windows**

* Supprimer les partitions existantes.

* Cliquer sur **Nouveau** pour créer une partition `NTFS`.

* Sélectionner la partition et cliquer sur **Formater**.

* Le programme d'installation copie les fichiers sur le disque dur.

* Enlever la clé USB lors du redémarrage initial.

* Région&nbsp;: **France**

* Disposition de clavier&nbsp;: **Français (Suisse)**

* Ignorer l'ajout d'une deuxième disposition de clavier.

* Cliquer sur **Configurer pour une utilisation personnelle**.

* Ajoutez votre compte&nbsp;: **Compte hors connexion**

* Cliquer sur **Expérience limitée**.

* Qui sera amené à utiliser ce PC ? **Microlinux**

* Créer et confirmer le mot de passe pour l'utilisateur.

* Créer trois questions de sécurité pour le compte.

* Autoriser Microsoft et les applications à utiliser votre
  emplacement&nbsp;? **Non**.

* Localiser mon appareil&nbsp;? **Non**.

* Données de diagnostic&nbsp;: **Envoyer les données de diagnostic
  obligatoires**.

* Améliorer l'écriture manuscrite et la saisie&nbsp;? **Non**.

* Obtenir des expériences personnalisées avec des données de
  diagnostic&nbsp;? **Non**.

* Autoriser les applications à utiliser l'identifiant de publicité&nbsp;?
  **Non**.

* Personnaliser votre expérience utilisateur&nbsp;? **Ignorer**.

* Laisser Cortana vous aider à accomplir vos tâches&nbsp;? **Pas maintenant**.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
