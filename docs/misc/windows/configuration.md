
# Configuration initiale de Microsoft Windows&nbsp;10

## ShutUp10

Dans sa configuration par défaut, Windows 10 est un système horriblement
intrusif et bavard. L'outil `ShutUp10` permet de remédier à cela en une paire
de clics. 

* Ouvrir [la page du projet](https://www.oo-software.com/fr/shutup10). 

* Télécharger et exécuter `OOSU10.exe`.

## Windows10Debloater

Windows 10 contient une quantité de composants à l'utilité discutable, de
ActiproSoftware à Zune Music. La plupart de ces composants lancent des services
correspondants en fond de tâche, ce qui contribue à un embonpoint considérable
du système.

L'outil `Windows10Debloater` permet de se débarrasser assez efficacement de
tout ce fatras sans pour autant porter atteinte aux fonctionnalités
essentielles du système.

* Ouvrir [la page du projet](https://github.com/Sycnex/Windows10Debloater). 

* **Télécharger Code** > **Download ZIP**.

* Extraire l'archive `Windows10Debloater-master.zip` vers le répertoire
  `Downloads`.

* **Menu Démarrer** > **Windows Power Shell** > **Exécuter en tant
  qu'administrateur**.

* Autoriser l'exécution dans le *Power Shell*&nbsp;:

```
> Set-ExecutionPolicy Unrestricted -Force
```

* Aller dans le répertoire `Windows10Debloater-master` nouvellement créé&nbsp;:

```
> cd C:\Users\Microlinux\Downloads\Windows10Debloater-master\
```

* Exécuter la version graphique du script&nbsp;:

```
> ./Windows10DebloaterGUI.ps1
```

* Répondre `[O] Exécuter une fois` à l'avertissement de sécurité.

* Utiliser les options suivantes&nbsp;:

    * **Debloat Options** > **Remove all bloatware**

    * **Cortana** > **Disable**

    * **Edge PDF** > **Disable**

    * **Dark Theme** > **Disable**

    * **Uninstall OneDrive**

    * **Disable Telemetry / Tasks**

    * **Remove bloatware regkeys**

    * **Install .NET v3.5**

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

