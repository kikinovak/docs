
# Installer et gérer des applications sous Windows&nbsp;10

## Le gestionnaire de paquets Chocolatey

Pendant longtemps, la gestion des applications a été une véritable plaie dans
l'univers de Windows. Chaque application devait être téléchargée sur le site de
l'éditeur et disposait généralement de sa propre procédure d'installation, qui
dans bien des cas ne se gênait pas de spammer le système avec tout un fatras
de *freewares* intrusifs. Sans compter que toutes ces applications devaient être
mises à jour manuellement au cas par cas.

Or, nous pouvons très bien gérer nos applications intelligemment comme sous
Linux, grâce au gestionnaire de paquets Chocolatey, qui permet de rechercher,
d'installer, de mettre à jour et de supprimer des paquets logiciels sous
Windows tout comme nous avons l'habitude de le faire sous Linux.

> Chocolatey repose sur l’infrastructure `NuGet` (*New Get*) qui permet aux
> développeurs de partager du code. De `Nuget` à "nougat" il n’y a qu’un pas,
> d’où le nom de Chocolatey. Oui, les informaticiens sont des gens bizarres.

* Ouvrir [la page du projet Chocolatey](https://chocolatey.org).

* Suivre le lien [Get Started](https://chocolatey.org/install).

* Ouvrir le *Power Shell* en tant qu'administrateur.

* Copier la commande sur la page dans le presse-papier.

* Coller la commande dans le *Power Shell* et lancer l'exécution.

```
> Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

> Dans le doute, redémarrer la machine.

## Utiliser le gestionnaire de paquets

Chercher un paquet&nbsp;:

```
> choco search firefox
```

Restreindre la recherche aux noms des paquets&nbsp;:

```
> choco search --by-id-only firefox
```

Alternativement, utiliser l'interface de recherche https://community.chocolatey.org/packages.

Afficher les informations à propos d'un paquet&nbsp;:

```
> choco info libreoffice-fresh
```

Installer un paquet&nbsp;:

```
> choco install -y firefox
```

Afficher les paquets installés&nbsp;:

```
> choco list --local
```

Supprimer un paquet avec les dépendances&nbsp;:

```
> choco uninstall --remove-dependencies libreoffice-still
```

Vérifier s'il y a des mises à jour&nbsp;:

```
> choco outdated
```

Mettre à jour tous les paquets&nbsp;:

```
> choco upgrade -y all
```

## Une sélection de paquets logiciels

### Internet

* Mozilla Firefox : `firefox`

* Mozilla Thunderbird : `thunderbird`

* OwnCloud : `owncloud-client`

* Transmission : `transmission`

* KeePassXC : `keepassxc`

* AnyDesk : `anydesk`

### Bureautique

* LibreOffice : `libreoffice-fresh`

* Acrobat Reader : `adobereader`

* CherryTree : `cherrytree`

### Graphisme

* GIMP : `gimp`

* Inkscape : `inkscape`

* Scribus : `scribus`

### Multimédia

* VLC : `vlc`

* Audacious : `audacious`

* Audacity : `audacity`

* Plugin LAME pour Audacity : `audacity-lame`

* Plugin FFmpeg pour Audacity : `audacity-ffmpeg`

* Kdenlive : `kdenlive`

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
