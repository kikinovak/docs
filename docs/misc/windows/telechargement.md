
# Télécharger Microsoft Windows 10


## Récupérer l'ISO officiel

* Ouvrir la [page de
  téléchargement](https://www.microsoft.com/fr-fr/software-download/windows10ISO)
  sur le site de Microsoft.

* Sélectionner **Windows10** et cliquer sur **Confirmer**.

* Sélectionner **Français** et cliquer sur **Confirmer**.

* Cliquer sur **Téléchargement 64-bit**.

* Télécharger le fichier `Win10_21H2_French_x64.iso` (5.5 Go).


## Confectionner la clé USB d'installation

Télécharger Ventoy sur la [page de
téléchargement](https://www.ventoy.net/en/download.html) du projet.

Insérer et identifier une clé USB&nbsp;:

```
# lsblk
```

Nettoyer la clé&nbsp;:

```
# sgdisk --zap-all /dev/sdX
# wipefs --all /dev/sdX
```

Installer Ventoy sur la clé&nbsp;:

```
# tar -xzf ventoy-1.0.78-linux.tar.gz
# cd ventoy-1.0.78/
# ./Ventoy2Disk.sh -i /dev/sdX -L WINDOWS10
Create partitions on /dev/sdX by parted in MBR style ...
Done
mkfs on disk partitions ...
create efi fat fs /dev/sdX2 ...
mkfs.fat 4.2 (2021-01-31)
success
mkexfatfs 1.3.0
Creating... done.
Flushing... done.
File system created successfully.
writing data to disk ...
sync data ...
esp partition processing ...
Install Ventoy to /dev/sdX successfully finished.
```

Faire un peu de ménage&nbsp;:

```
# cd ..
# rm -rf ventoy-1.0.78
```

Monter la clé USB et copier le fichier `Win10_21H2_French_x64.iso` à la racine
de la clé.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
