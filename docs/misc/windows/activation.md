
# Activer Windows&nbsp;10 et installer MS&nbsp;Office

## Désactiver l'antivirus


* **Paramètres** > **Mise à jour et sécurité** > **Sécurité Windows** >
  **Protection contre les virus et menaces** > **Paramètres de protection
  contre les virus et menaces** > **Gérer les paramètres** > **Protection en
  temps réel** > **Désactivé**.

## Télécharger les outils KMS

* Ouvrir un site de téléchargement comme [The Pirate
  Bay](https://thepiratebay10.org).

* Chercher `Ratiborus KMS Tools`.

* Télécharger la dernière version en date de l'outil, par exemple `Ratiborus
  KMS Tools 01.07.2022 Portable`. 

## Lancer les outils KMS

* Clic droit sur `Add_Defender_Exclusion.bat` > **Exécuter en tant
  qu'administrateur**.

* Lancer `KMS Tools.exe`.

* Cliquer sur **Office 2021 Install**.

* Sélectionner **Microsoft Office 2019-2021** dans le menu déroulant.

* Sélectionner la plateforme **x64**.

* Décocher **en-US** et sélectionner la langue **fr-FR**.

* Sélectionner les applications : **Word**, **Excel**, **Access**,
  **PowerPoint**, **Publisher**, etc.

* Cliquer sur **Install Office**.

* Cliquer sur **AAct Portable**.

* Cliquer sur **Windows Activation**.

* Vérifier en cliquant sur **Windows Information**.

* Cliquer sur **Office Activation**.

* Si ça ne marche pas, ouvrir **KMSAuto Lite**.

* Cliquer sur **Office**.

* Retourner dans **AAct Portable**.

* Relancer **Office Activation**.

* Vérifier en cliquant sur **Office Information**.

## Réactiver l'antivirus

* **Paramètres** > **Mise à jour et sécurité** > **Sécurité Windows** >
  **Protection contre les virus et menaces** > **Paramètres de protection
  contre les virus et menaces** > **Gérer les paramètres** > **Protection en
  temps réel** > **Activé**.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
