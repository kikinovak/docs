
# Initialiser un NAS Synology DS214se


## Remise à zéro

* Repérer le bouton ++reset++ à l'arrière.

* Allumer le NAS et chercher un trombone.

* Appuyer quelques secondes sur le bouton ++reset++ jusqu'à ce qu'on entende un
  *bip*.

* Appuyer encore une fois sur ++reset++ jusqu'à ce qu'on entende trois *bips*.

* Attendre deux minutes.

* Ouvrir l'interface web à l'adresse [http://nas.microlinux.lan:5000](#).

* Cliquer sur **Réinstaller** > **Installer maintenant**.

* Attendre 15 minutes.

* Ouvrir une connexion sécurisée à l'adresse
  [https://nas.microlinux.lan:5001](#).

> Accepter le certificat auto-signé dans le navigateur Web.


## Compte administrateur

* **Nom serveur**&nbsp;: `Synology-DS214`

* **Nom d'utilisateur**&nbsp;: `microlinux`

* **Mot de passe**&nbsp;: `**********`

* Confirmation du mot de passe&nbsp;: `**********`


## Configuration initiale

* Passer l'étape de configuration de QuickConnect.

* Passer l'installation des paquets Synology.

* Ne pas cocher **Partager l'emplacement réseau de mon périphérique Synology**.

* **Mise à jour Smart**&nbsp;: cliquer sur **J'ai compris**.

* **Analyse de périphérique**&nbsp;: **Non merci**.


## Sécuriser l'installation

* **Menu principal** (tout en haut à gauche) > **Conseiller de sécurité** >
  ajouter au tableau de bord.

* **Conseiller de sécurité** > **Démarrer** > patienter pendant l'analyse
  initiale du système.

* **Panneau de configuration** > **Mise à jour et restauration** >
  **Télécharger** > **Mettre à jour maintenant**.

* Attendre dix minutes.


## Initialisation des disques

* **Menu principal** > **Gestionnaire de stockage** > ajouter au tableau de
  bord.

* **Gestionnaire de stockage** > **HDD/SSD**.

* **Volume** > **Supprimer**.

* **Groupe de stockage** > **Supprimer**.

* **HDD/SSD**&nbsp;: sélectionner les disques à tour de rôle > **Infos sur la
  santé**.

* **Groupe de stockage** > **Créer** > **Meilleures performances**.

* **Description du groupe de stockage**&nbsp;: laisser vide.

* **Type de RAID**&nbsp;: **RAID 1**.

* **Effectuer une vérification du disque**&nbsp;: **Non**.

* Cliquer sur **Appliquer**.

* **Volume** > **Créer** > **Personnalisé**.

* Choisir un groupe de stockage existant > **Groupe de stockage 1**.

* **Description**&nbsp;: laisser vide.

* Cliquer sur **Appliquer**.

* Patienter pendant le formatage de la grappe de disques.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
