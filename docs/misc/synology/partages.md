
# Utilisateurs et partages sur un NAS Synology&nbsp;DS214se


## Créer les utilisateurs

* **Panneau de configuration** > **Utilisateur** > **Créer**.

* **Nom**&nbsp;: `kikinovak`

* **Description**&nbsp;: `Kiki Novak`

* **Mot de passe**&nbsp;: `**********`

* Confirmer le mot de passe&nbsp;: `**********`

* Cliquer sur **Suivant**.

* Ajouter au groupe `users` par défaut.

* Accepter les paramétrages par défaut.

> Procéder de même pour les autres utilisateurs du NAS.


## Configurer les dossiers partagés

* **Panneau de configuration** > **Dossier partagé** > **Créer**.

* **Nom**&nbsp;: `Ghost`

* **Description**&nbsp;: `Images Ghost`

* Décocher **Activer la corbeille**.

* Cliquer sur **Suivant**.

* Ne pas chiffrer le dossier.

* Cliquer sur **Appliquer**.

* Autoriser en lecture/écriture pour l'utilisateur `ghost`.

* Interdire l'accès aux autres utilisateurs.

* **Dossier partagé** > **Créer**.

* **Nom**&nbsp;: `Partages`

* **Description**&nbsp;: `Partages Microlinux`

* Décocher **Activer la corbeille**.

* Cliquer sur **Suivant**.

* Ne pas chiffrer le dossier.

* Cliquer sur **Appliquer**.

* Autoriser en lecture/écriture pour `kikinovak` et les utilisateurs censés
  accéder au partage.

* Interdire l'accès aux autres utilisateurs.


## Activer l'accueil utilisateur

* **Panneau de configuration** > **Utilisateur** > Onglet **Avancé**.

* **Accueil utilisateur** > cocher **Activer le service d'accueil de
  l'utilisateur**.

* Cliquer sur **Appliquer**.


## Configurer le service FTP

* **Panneau de configuration** > **Services de fichiers** > Onglet **FTP** >
  cocher **Activer le service FTP**.

* **Paramètres avancés** > cocher **Changer les dossiers root de
  l'utilisateur**.

* Sélectionner un utilisateur > **Ajouter**.

* **Utilisateur ou groupe**&nbsp;: `ghost`

* **Changer le répertoire `root` vers** > `Dossier partagé : Ghost`

* Cliquer sur **Appliquer**.

> Tester la connexion avec un client FTP comme Filezilla.


## Configurer le service Samba

* **Panneau de configuration** > **Services de fichiers** > Onglet **SMB** >
  cocher **Activer le service SMB**.

* **Groupe de travail**&nbsp;: `WORKGROUP`

* Cliquer sur **Appliquer**.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

