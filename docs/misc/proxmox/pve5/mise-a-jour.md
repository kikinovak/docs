
Arrêter toutes les VM et déconnecter l'interface web.

Mettre à jour tous les paquets :

```
$ sudo apt-get update
$ sudo apt-get dist-upgrade
$ sudo reboot && exit
```

Effectuer un test préliminaire :

```
$ sudo pve5to6
...
= SUMMARY =

TOTAL:    17
PASSED:   15
SKIPPED:  2
WARNINGS: 0
FAILURES: 0
```

Passer les dépôts de paquets de `stretch` à `buster` :

```
$ sudo sed -i 's/stretch/buster/g' /etc/apt/sources.list
$ sudo sed -i 's/stretch/buster/g' /etc/apt/sources.list.d/pve-community.list
```

Lancer la mise à jour :

```
$ sudo apt-get update
$ sudo apt-get dist-upgrade
```

* Appuyer sur ++enter++ pour confirmer le passage à Proxmox VE 6.x.
* Lire les notes de mise à jour et quitter : ++q++
* Sélectionner la disposition clavier `German (Switzerland) - French (Switzerland)`.
* Mettre à jour `/etc/issue` : ++y++
* `Restart services during package upgrades without asking` : ++y++
* Installer `pve-enterprise.list` : ++n++

Redémarrer :
 
```
$ sudo reboot && exit
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

