
Sur une machine publique, on peut remplacer le certificat auto-signé par un certificat valide.

Créer un compte ACME&nbsp;:

```
$ sudo pvenode acme account register default info@microlinux.fr
Directory endpoints:
0) Let's Encrypt V2 
1) Let's Encrypt V2 Staging 
2) Custom
Enter selection: 0
...
Terms of Service: 
Do you agree to the above terms? [y|N]: y
...
Generating ACME account key...
Registering ACME account...
Registration successful...
Task OK
```

Se connecter à l'interface web et générer un certificat&nbsp;:

`System` > `Certificates` > `Add` >

* `Challenge Type` : `HTTP`

* `Domain` : `sd-155842.dedibox.fr`

* Cliquer sur `Create`.

* Cliquer sur `Order Certificates Now`.

* Attendre le résultat `TASK OK`.

Supprimer le certificat auto-signé dans Firefox&nbsp;: 

`Préférences` > `Vie privée et sécurité` > `Afficher les certificats` > `Serveurs` >
`sd-155842.dedibox.fr:8006` > `Supprimer`.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
