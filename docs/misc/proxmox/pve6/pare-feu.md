
Accéder à l'interface de configuration du pare-feu&nbsp;:

* `Datacenter` > `sd-155842` > `Firewall` > `Add`

Ouvrir le port 8006 en TCP pour l'interface web&nbsp;:

* `Direction` : `in`

* `Action` : `ACCEPT`

* `Protocol` : `tcp`

* `Source` : `88.161.127.222` (restrictif)

* `Dest. port` : `8006`

* `Comment` : `Web GUI`

* Cocher la case `Enable`.

Ouvrir le port 80 pour permettre la validation des certificats
LetsEncrypt&nbsp;:

* `Direction` : `in`

* `Action` : `ACCEPT`

* `Protocol` : `tcp`

* `Dest. port` : `80`

* `Comment` : `LetsEncrypt certificate validation`

* Cocher la case `Enable`.

Ouvrir le port 22 en TCP pour les connexions SSH&nbsp;:

* `Direction` : `in`

* `Action` : `ACCEPT`

* `Protocol` : `tcp`

* `Source` : `88.161.127.222` (restrictif)

* `Dest. port` : `22`

* `Comment` : `SSH`

* Cocher la case `Enable`.

Autoriser le `ping`&nbsp;:

* `Direction` : `in`

* `Action` : `ACCEPT`

* `Protocol` : `icmp`

* `Source` : `88.161.127.222` (restrictif)

* `Comment` : `Ping`

* Cocher la case `Enable`.

Activer le pare-feu :

`Datacenter` > `Firewall` > `Options` >

* `Firewall` : `Yes`

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
