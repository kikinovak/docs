Renommer une VM&nbsp;:

`Datacenter` > `sd-155842` > `Oracle-Linux-7.9` > `Options` > `Name` > `Edit`

Cloner une VM&nbsp;:

`Datacenter` > `sd-155842` > `Oracle-Linux-7.9` > `More` > `Clone`

* Surveiller l'avancement de l'opération dans les logs en bas de l'écran.

* Éventuellement rectifier l'adresse MAC du clone.

Supprimer une VM&nbsp;:

`Datacenter` > `sd-155842` > `Oracle-Linux-7.9` > `More` > `Remove`

Agrandir une VM&nbsp;:

`Datacenter` > `sd-155842` > `Oracle-Linux-7.9` > `Hardware` > `Hard disk` > `Resize disk`

* Démarrer la VM.

* Installer l'outil `growpart`.

* Afficher les partitions du disque.

* Redimensionner la partition principale avec `growpart`.

* Redimensionner le système de fichiers `ext4` avec `resize2fs`.

```
# yum install cloud-utils-growpart
# lsblk
# growpart -v /dev/sda 3
# resize2fs /dev/sda3
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

