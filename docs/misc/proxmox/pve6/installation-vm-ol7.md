
Télécharger l'ISO&nbsp;:

* `Datacenter` > `sd-155842` > `local (sd-155842)` > `ISO Images` > `Upload`

* Sélectionner `Oracle-Linux-7.9-rhck-x86_64-netinstall.iso`.

Créer la VM&nbsp;:

* Cliquer sur `Create VM`.

* `Node` : `sd-155842`

* `VM ID` : `100`

* `Name` : `Oracle-Linux-7.9`

* Sélectionner l'ISO.

* `Disk size` : `6 GiB`

* `CPU cores` : `2`

* `CPU type` : `kvm64` ou `host`

* `Memory` : `1024 MiB`

* `MAC address` : `52:54:00:00:E4:AC`

Démarrer la VM et ouvrir la console en plein écran.

Pour le partitionnement manuel, basculer dans une console virtuelle&nbsp;:

* `Monitor` : `sendkey ctrl-alt-f3`

Configurer la disposition clavier en mode console&nbsp;:

```
# loadkeys fr_CH-latin1
```

Lancer `fdisk` et créer trois partitions&nbsp;:

* une partition `/boot` de 500 Mo.

* une partition `swap` de 1 Go.

* une partition principale occupant le reste du disque virtuel.

Revenir dans la fenêtre principale de l'installateur&nbsp;:

* `Monitor` : `sendkey ctrl-alt-f6`

Procéder au formatage des partitions dans l'installateur graphique.

> Ne pas oublier de réanalyser les disques&nbsp;!

Configurer le réseau :

* Cliquer sur `Configurer`.

* `Device` : `eth0 (52:54:00:00:E4:AC)`

* Ouvrir l'onglet `Paramètres IPv4`.

* `Méthode` : `Manuel`

* `Adresses` : cliquer sur `Add`.

* `Adresse` : `163.172.226.12`

* `Masque de réseau` : `255.255.255.0`

* `Passerelle` : `62.210.0.1`

* `Serveurs DNS` : `62.210.16.6`, `62.210.16.7`

* Cliquer sur `Enregistrer`.

* Nom d'hôte : `sandbox`

* Activer le réseau.

Configurer l'installation via le réseau&nbsp;:

* Cliquer sur `Source d'Installation`.

* Utiliser la source `Sur le réseau` par défaut.

* Indiquer la source `http://yum.oracle.com/repo/OracleLinux/OL7/latest/x86_64`.

* Ne pas cocher `Cette URL se réfère à une liste de miroirs`.

* Cliquer sur `Terminé`.

* Vérifier si la `Sélection de Logiciels` est actualisée correctement.

* Sélectionner `Installation minimale`.

Au premier redémarrage, éditer `/etc/default/grub`&nbsp;:

```
GRUB_TIMEOUT=0
```

Prendre en compte les modifications :

```
# grub2-mkconfig -o /boot/grub2/grub.cfg
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

