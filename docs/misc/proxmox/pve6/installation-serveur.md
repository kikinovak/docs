Télécharger l'ISO :

* Aller sur la page du projet : [https://www.proxmox.com](https://www.proxmox.com)
* Suivre le lien [Downloads](https://www.proxmox.com/en/downloads) > 
  [Proxmox Virtual Environment](https://www.proxmox.com/en/downloads/category/proxmox-virtual-environment) > 
  [ISO Images](https://www.proxmox.com/en/downloads/category/iso-images-pve)
* Télécharger l'image `Proxmox VE 6.3 ISO Installer` (direct ou BitTorrent).

Écrire l'ISO sur une clé USB :

```
# dd if=proxmox-ve_6.3-1.iso of=/dev/sdX bs=512k status=progress
```

Lancer l'installation :

* `Install Proxmox VE`
* Accepter la licence utilisateur : `I agree`
* Sélectionner le disque dur.
* `Country` : `France`
* `Time zone` : `Europe/Paris`
* `Keyboard Layout` : `Swiss-French`
* Définir le mot de passe pour `root`.
* `Email` : `root@pve.microlinux.lan`
* `Management interface` : sélectionner la carte réseau.
* `Hostname (FQDN)` : `pve.microlinux.lan`
* `IP Address (CIDR)` : `192.168.2.3/24`
* `Gateway` : `192.168.2.1`
* `DNS Server` : `192.168.2.1`
* `Summary` : vérifier les paramètres.
* Lancer l'installation : `Install`

Se connecter en tant que `root` (directement ou via SSH).

Ajouter un utilisateur initial :

```
# adduser microlinux
Adding user `microlinux' ...
Adding new group `microlinux' (1000) ...
Adding new user `microlinux' (1000) with group `microlinux' ...
Creating home directory `/home/microlinux' ...
Copying files from `/etc/skel' ...
New password: **********
Retype new password: **********
passwd: password updated successfully
Changing the user information for microlinux
Enter the new value, or press ENTER for the default
        Full Name []: Microlinux
        Room Number []: 
        Work Phone []: 
        Home Phone []: 
        Other []: 
Is the information correct? [Y/n] y
```

Éventuellement, mettre en place l'authentification par clé SSH pour ce compte :

```
$ ssh-copy-id -i .ssh/id_rsa.pub microlinux@pve.microlinux.lan
```

Éditer `/etc/apt/sources.list` (avec `vi`) pour configurer les dépôts de paquets Debian&nbsp;:

```
# /etc/apt/sources.list
deb http://ftp.debian.org/debian buster main contrib

deb http://ftp.debian.org/debian buster-updates main contrib

# security updates
deb http://security.debian.org buster/updates main contrib
```

Renommer le fichier de configuration des dépôts de Proxmox :

```
# cd /etc/apt/sources.list.d/
# mv -v pve-enterprise.list pve-community.list 
renamed 'pve-enterprise.list' -> 'pve-community.list'
```

Éditer ce fichier (avec `vi`) pour utiliser le dépôt communautaire :

```
# /etc/apt/sources.list.d/pve-community.list
deb http://download.proxmox.com/debian/pve buster pve-no-subscription
```

Effectuer la mise à jour initiale :

```
# apt-get update
# apt-get dist-upgrade
```

Installer quelques outils supplémentaires :

```
# apt-get install vim sudo bsd-mailx
```

Désactiver l'IPv6 :

```
# /etc/sysctl.d/disable-ipv6.conf
net.ipv6.conf.all.disable_ipv6 = 1
```

Éditer `/etc/hosts` :

```
# /etc/hosts
127.0.0.1       localhost.localdomain localhost
192.168.2.3     pve.microlinux.lan pve
```

Éditer `/usr/share/perl5/PVE/API2/Subscription.pm` pour désactiver la bannière d'accueil&nbsp;:

```
  status => "Active",          # ligne 120
  message => "There is no subscription key",
```

Redémarrer :

```
# reboot && exit
```

Ouvrir l'interface web en acceptant le certificat auto-signé :

* `https://pve.microlinux.lan:8006`

Se connecter en tant que `root` avec le mot de passe correspondant.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

