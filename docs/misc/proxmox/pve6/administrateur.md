
Permettre à l'utilisateur initial d'utiliser la commande `sudo`&nbsp;:

```
$ su -
# adduser microlinux sudo
Adding user `microlinux' to group `sudo' ...
Done.
# exit
$ exit
```

Se connecter à l'interface web et créer le groupe `Admin`&nbsp;:

`Datacenter` > `Groups` > `Create` >

* `Name` : `Admin`

* Cliquer sur `Create`.

Définir les permissions de ce groupe&nbsp;:

`Datacenter` > `Permissions` > `Add` > `Group Permission` >

* `Path` : `/`

* `Group` : `Admin`

* `Role` : `Administrator`

* Cliquer sur `Add`.

Ajouter l'utilisateur initial à ce groupe&nbsp;:

`Datacenter` > `Users` > `Add` >

* `User name` : `microlinux`

* `Group` : `Admin`

* `First Name` : `Nicolas`

* `Last Name` : `Kovacs`

* `E-Mail` : `info@microlinux.fr`

* Cliquer sur `Add`.

À partir de là, on peut se reconnecter à l'interface web avec ce nouveau compte.

Désactiver le compte `root` dans la console&nbsp;:

```
$ sudo passwd -l root
passwd: password expiry information changed.
```

Le compte `root` ne pourra plus se connecter à l'interface web.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
