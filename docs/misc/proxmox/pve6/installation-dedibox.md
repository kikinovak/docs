Procéder au choix de la machine et du système d’exploitation :

* Se connecter à la console Online : [https://console.online.net](https://console.online.net)
* Ouvrir le menu `Serveur` > `Liste des serveurs`
* Sélectionner la machine > `Administrer` > `Installer`
* `Distributions virtualisation` > `Proxmox` > `Proxmox VE-6 64bits` > `Installer Proxmox`

Définir les utilisateurs :

* Choisir un mot de passe provisoire pour `root`.
* Définir un utilisateur normal, par exemple `microlinux`.
* Choisir un mot de passe provisoire pour cet utilisateur.

> L'installateur impose une limite de 15 caractères alphanumériques.

L’interface affiche un récapitulatif des paramètres réseau de la machine :

* `Nom d’hôte` : `sd-155842`
* `Adresse IP` : `51.158.146.161`
* `Masque de sous-réseau` : `255.255.255.0`
* `IP de la passerelle` : `51.158.146.1`
* `DNS primaire` : `62.210.16.6`
* `DNS secondaire` : `62.210.16.7`

Cliquer sur `Effacer l’intégralité de mes disques durs pour procéder à l’installation`.

> L’installation de Proxmox dure près de 20 minutes sur une Dedibox Start-2-S-SSD.

Mettre en place l'authentification par clé SSH :

```
$ ssh-copy-id -i .ssh/id_rsa.pub microlinux@sd-155842.dedibox.fr
```

Se connecter à la machine et définir des mots de passe plus solides :

```
$ ssh microlinux@sd-155842.dedibox.fr
$ su -
# passwd
# passwd microlinux
```

Désactiver le dépôt Proxmox Entreprise :

```
# cd /etc/apt/sources.list.d/
# rm -f pve-enterprise.list
```

Renommer le fichier du dépôt communautaire :

```
# mv -v pve-install-repo.list pve-community.list
renamed 'pve-install-repo.list' -> 'pve-community.list'
```

Installer quelques outils supplémentaires :

```
# apt-get update
# apt-get install vim sudo bsd-mailx
```

Désactiver l'IPv6 si l'on ne l'utilise pas :

```
# /etc/sysctl.d/disable-ipv6.conf
net.ipv6.conf.all.disable_ipv6 = 1
```

Éditer `/etc/hosts` :

```
# /etc/hosts
127.0.0.1       localhost.localdomain localhost
51.158.146.161  sd-155842.dedibox.fr sd-155842
```

Éditer `/usr/share/perl5/PVE/API2/Subscription.pm` pour désactiver la bannière
d'accueil :

```
  status => "Active",          # ligne 120
  message => "There is no subscription key",
```

Redémarrer :

```
# reboot && exit
```

Ouvrir l'interface web en acceptant le certificat auto-signé :

* `https://sd-155842.dedibox.fr:8006`

Se connecter en tant que `root` avec le mot de passe correspondant.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
