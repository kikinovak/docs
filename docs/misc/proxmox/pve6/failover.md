
Commander une IP failover pour la VM&nbsp;:

* Se connecter à la [console Online](https://console.online.net).

* Ouvrir le menu `Serveur` > `Liste des serveurs`.

* Sélectionner la machine > `Administrer` > `Failover`.

* Cliquer sur `Commander des adresses IP` > `Adresses IP seules`.

* Choisir l'adresse IP dans la liste et cliquer sur `Commander`.

Associer l'adresse IP au serveur&nbsp;:

* Revenir à la page d'administration du serveur > `Failover`.

* Glisser-déposer l'adresse IP vers le serveur correspondant.

* Cliquer sur `Mise à jour` en bas de la page.

* Attendre 5 minutes.

Associer une adresse MAC virtuelle à l'IP failover&nbsp;:

* `Serveur` > `Liste des serveurs` > Sélectionner la machine > `Administrer`.

* `Réseau` > `Adresses secondaires` > `+ d'infos` > `Ajouter une adresse MAC`.

* `Adresse MAC virtuelle` > Déplier le menu > `Ajouter une adresse MAC virtuelle pour KVM`.

* Noter l'adresse MAC, par exemple `52:54:00:00:E4:AC`.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
