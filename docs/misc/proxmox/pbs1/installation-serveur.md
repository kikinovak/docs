Télécharger l'ISO :

* Aller sur la page du projet&nbsp;:
  [https://www.proxmox.com](https://www.proxmox.com).

* Suivre le lien *Downloads* > *Proxmox Backup Server* > *ISO Images*.

* Télécharger l'image `Proxmox Backup Server 1.1 ISO Installer` (direct ou BitTorrent).

Écrire l'ISO sur une clé USB :

```
# dd if=proxmox-backup-server_1.0-1.iso of=/dev/sdX bs=512k status=progress
```

Lancer l'installation&nbsp;:

* `Install Proxmox Backup Server`

* Accepter la licence utilisateur : `I agree`

* Sélectionner le disque dur.

* `Country` : `France`

* `Time zone` : `Europe/Paris`

* `Keyboard Layout` : `Swiss-French`

* Définir le mot de passe pour `root`.

* `Email` : `root@pbs.microlinux.lan`

* `Management interface` : sélectionner la carte réseau.

* `Hostname (FQDN)` : `pbs.microlinux.lan`

* `IP Address (CIDR)` : `192.168.2.6/24`

* `Gateway` : `192.168.2.1`

* `DNS Server` : `192.168.2.1`

* `Summary` : vérifier les paramètres.

* Lancer l'installation : `Install`

Se connecter en tant que `root` (directement ou via SSH).

Ajouter un utilisateur initial&nbsp;:

```
# adduser microlinux
Adding user `microlinux' ...
Adding new group `microlinux' (1000) ...
Adding new user `microlinux' (1000) with group `microlinux' ...
Creating home directory `/home/microlinux' ...
Copying files from `/etc/skel' ...
New password: **********
Retype new password: **********
passwd: password updated successfully
Changing the user information for microlinux
Enter the new value, or press ENTER for the default
        Full Name []: Microlinux
        Room Number []: 
        Work Phone []: 
        Home Phone []: 
        Other []: 
Is the information correct? [Y/n] y
```

Éventuellement, mettre en place l'authentification par clé SSH pour ce
compte&nbsp;:

```
$ ssh-copy-id -i .ssh/id_rsa.pub microlinux@pbs.microlinux.lan
```

Éditer `/etc/apt/sources.list` (avec `vi`) pour configurer les dépôts de
paquets Debian&nbsp;:

```
# /etc/apt/sources.list
deb http://ftp.debian.org/debian buster main contrib

deb http://ftp.debian.org/debian buster-updates main contrib

# security updates
deb http://security.debian.org buster/updates main contrib
```

Renommer le fichier de configuration des dépôts de Proxmox&nbsp;:

```
# cd /etc/apt/sources.list.d/
# mv -v pbs-enterprise.list pbs-community.list 
renamed 'pbs-enterprise.list' -> 'pbs-community.list'
```

Éditer ce fichier (avec `vi`) pour utiliser le dépôt communautaire&nbsp;:

```
# /etc/apt/sources.list.d/pbs-community.list
deb http://download.proxmox.com/debian/pve buster pve-no-subscription
```

Effectuer la mise à jour initiale&nbsp;:

```
# apt-get update
# apt-get dist-upgrade
```

Installer quelques outils supplémentaires&nbsp;:

```
# apt-get install vim sudo bsd-mailx
```

Désactiver l'IPv6&nbsp;:

```
# /etc/sysctl.d/disable-ipv6.conf
net.ipv6.conf.all.disable_ipv6 = 1
```

Éditer `/etc/hosts`&nbsp;:

```
# /etc/hosts
127.0.0.1       localhost.localdomain localhost
192.168.2.6     pbs.microlinux.lan    pbs
```

Éditer `/usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js` pour
désactiver la bannière d'accueil&nbsp;:

```
let res = response.result;
if (false) {                # ligne 468
    Ext.Msg.show({
        title: gettext('No valid subscription'),
```

Redémarrer&nbsp;:

```
# reboot && exit
```

Ouvrir l'interface web en acceptant le certificat auto-signé&nbsp;:

* `https://pbs.microlinux.lan:8007` 

Se connecter en tant que `root` avec le mot de passe correspondant.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
