
Procéder au choix de la machine et du système d’exploitation&nbsp;:

* Se connecter à la console Online&nbsp;:
  [https://console.online.net](https://console.online.net)

* Ouvrir le menu `Serveur` > `Liste des serveurs`.

* Sélectionner la machine > `Administrer` > `Installer`.

* `Distributions virtualisation` > `Proxmox` > `Proxmox Backup Server 64bits` >
  `Installer Proxmox`.

Définir les utilisateurs&nbsp;:

* Choisir un mot de passe provisoire pour `root`.

* Définir un utilisateur normal, par exemple `microlinux`.

* Choisir un mot de passe provisoire pour cet utilisateur.

> L'installateur impose une limite de 15 caractères alphanumériques.

L’interface affiche un récapitulatif des paramètres réseau de la machine&nbsp;:

* `Nom d’hôte` : `sd-111887`

* `Adresse IP` : `163.172.89.142`

* `Masque de sous-réseau` : `255.255.255.0`

* `IP de la passerelle` : `163.172.89.1`

* `DNS primaire` : `62.210.16.6`

* `DNS secondaire` : `62.210.16.7`

Cliquer sur `Effacer l’intégralité de mes disques durs pour procéder à
l’installation`.

> L’installation de Proxmox dure près de 40 minutes sur une Dedibox
> Start-2-S-SATA.

Mettre en place l'authentification par clé SSH&nbsp;:

```
$ ssh-copy-id -i .ssh/id_rsa.pub microlinux@sd-111887.dedibox.fr
```

Se connecter à la machine et définir des mots de passe plus solides&nbsp;:

```
$ ssh microlinux@sd-111887.dedibox.fr
$ su -
# passwd
# passwd microlinux
```

Désactiver le dépôt Proxmox Entreprise&nbsp;:

```
# cd /etc/apt/sources.list.d/
# rm -f pbs-enterprise.list
```

Renommer le fichier du dépôt communautaire&nbsp;:

```
# mv -v pbs-install-repo.list pbs-community.list
renamed 'pbs-install-repo.list' -> 'pbs-community.list'
```

Installer quelques outils supplémentaires :

```
# apt-get update
# apt-get install vim sudo bsd-mailx
```

Désactiver l'IPv6&nbsp;:

```
# /etc/sysctl.d/disable-ipv6.conf
net.ipv6.conf.all.disable_ipv6 = 1
```

Éditer `/etc/hosts`&nbsp;:

```
# /etc/hosts
127.0.0.1       localhost.localdomain localhost
163.172.89.142  sd-111887.dedibox.fr  sd-111887
```

Éditer `/usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js` pour
désactiver la bannière d'accueil&nbsp;:

```
let res = response.result;
if (false) {                # ligne 468
    Ext.Msg.show({
        title: gettext('No valid subscription'),
```

Redémarrer :

```
# reboot && exit
```

Ouvrir l'interface web en acceptant le certificat auto-signé&nbsp;:

* `https://sd-111887.dedibox.fr:8007` 

Se connecter en tant que `root` avec le mot de passe correspondant.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
