
Postfix n'est pas censé recevoir de messages. 

Éditer `/etc/postfix/main.cf` et passer la directive `inet_interfaces` à
`loopback-only`&nbsp;:

```
inet_interfaces = loopback-only
```

Puisqu'on a désactivé l'IPv6, on va modifier `inet_protocols` en
fonction&nbsp;:

```
inet_protocols = ipv4
```

Prendre en compte les modifications&nbsp;:

```
$ sudo systemctl restart postfix
$ systemctl status postfix
$ sudo cat /var/log/mail.log
```

Envoyer un mail de test&nbsp;:

```
$ mail info@microlinux.fr
Subject: Test Postfix
Ceci est un test.
.
Cc:
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

