

`Datastore` > `Add Datastore` >

* `Name` : `pbs-backup`

* `Backing Path` : `/srv/backup`

* `GC Schedule` : `daily`

* `Prune Schedule` : `daily`

* Cliquer sur `Add`.

Garder les sept dernières sauvegardes&nbsp;:

`Datastore` > `pbs-backup` > `Prune & GC` > `Keep Last`&nbsp;: `7`

Copier l'empreinte du serveur dans le presse-papier&nbsp;:

`Dashboard` > `Show Fingerprint` > `Copy` > `OK`

Ouvrir l'interface web du serveur PVE.

`Datacenter` > `Storage` > `Add` > `Proxmox Backup Server`&nbsp;:

* `ID` : `pbs`

* `Server` : `sd-111887.dedibox.fr`

* `Username` : `root@pam`

* `Password` : `**********`

* `Datastore` : `pbs-backup`

* `Fingerprint` : coller l'empreinte PBS.

* Cliquer sur `Add`.

Effectuer une sauvegarde manuelle pour tester la configuration&nbsp;:

`Datacenter` > `sd-155842` > `Oracle-Linux-7.9` > `Backup` > `Backup now`

Si tout fonctionne correctement, on peut mettre en place une sauvegarde
automatique&nbsp;:

`Datacenter` > `Backup` > `Add` > 

* `Node` : `sd-155842`

* `Storage` : `pbs`

* `Day of week` : `Monday` - `Sunday`

* `Start Time` : `03:00`

* `Selection mode` : `All`

* `Send email to` : `info@microlinux.fr`

* `Email notification` : `Always`

* `Mode` : `Snapshot`

* Cliquer sur `Create`.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

