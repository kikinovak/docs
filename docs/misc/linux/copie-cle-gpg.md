
Dans l'exemple, je copie la paire de clés GPG de mon PC `alphamule` vers mon
portable `macbook`.

Afficher la clé&nbsp;:

```
[kikinovak@alphamule:~] $ gpg --list-key info@microlinux.fr
pub rsa4096 2018-04-09 [SCA]
9C4708738E37651B867DA97451F1EF3EC56945AE
uid [ ultime ] Nicolas Kovacs <info@microlinux.fr>
uid [ ultime ] [jpeg image of size 15912]
sub rsa4096 2018-04-09 [E]
```

Exporter la clé publique vers un fichier `cle-publique.asc`&nbsp;:

```
[kikinovak@alphamule:~] $ gpg --export --armor info@microlinux.fr > ~/cle-publique.asc
```

Exporter la clé privée vers un fichier `cle-privee.asc`&nbsp;:

```
[kikinovak@alphamule:~] $ gpg --export-secret-keys --armor --output ~/cle-privee.asc
```

> Cette opération nécessite de fournir le mot de passe.

Copier la paire de clés vers la nouvelle machine&nbsp;:

```
[kikinovak@alphamule:~] $ scp ~/cle-*.asc macbook:/home/kikinovak/
Password:
cle-privee.asc          100%   29KB   7.3MB/s   00:00
cle-publique.asc        100%   26KB   7.5MB/s   00:00
```

Importer d'abord la clé publique&nbsp;:

```
[kikinovak@macbook:~] $ gpg --import ~/cle-publique.asc
```

Procéder de même avec la clé privée&nbsp;

```
[kikinovak@macbook:~] $ gpg --import ~/cle-privee.asc
```

> Il vaut mieux effectuer cette opération localement et permettre à KDE
> d'afficher le dialogue de mot de passe en mode graphique. Dans le cas
> contraire, on risque d'avoir une `erreur pinentry` à l'ouverture de KWallet. 

Afficher les détails de la clé&nbsp;:

```
[kikinovak@macbook:~] $ gpg --list-key info@microlinux.fr
pub rsa4096 2018-04-09 [SCA]
9C4708738E37651B867DA97451F1EF3EC56945AE
uid [ inconnue] Nicolas Kovacs <info@microlinux.fr>
uid [ inconnue] [jpeg image of size 15912]
sub rsa4096 2018-04-09 [E]
```

Il ne reste plus qu'à se faire confiance à soi-même&nbsp;:

```
[kikinovak@macbook:~] $ gpg --edit-key info@microlinux.fr
gpg (GnuPG) 2.2.5; Copyright (C) 2018 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

La clef secrète est disponible.

sec  rsa4096/51F1EF3EC56945AE
     créé : 2018-04-09  expire : jamais      utilisation : SCA
     confiance : inconnu       validité : inconnu
ssb  rsa4096/1F0E86B7D8190FE0
     créé : 2018-04-09  expire : jamais      utilisation : E
[ inconnue] (1). Nicolas Kovacs <info@microlinux.fr>
[ inconnue] (2)  [jpeg image of size 15912]

gpg> trust
sec  rsa4096/51F1EF3EC56945AE
     créé : 2018-04-09  expire : jamais      utilisation : SCA
     confiance : inconnu       validité : inconnu
ssb  rsa4096/1F0E86B7D8190FE0
     créé : 2018-04-09  expire : jamais      utilisation : E
[ inconnue] (1). Nicolas Kovacs <info@microlinux.fr>
[ inconnue] (2)  [jpeg image of size 15912]

Décidez maintenant de la confiance que vous portez en cet utilisateur pour
vérifier les clefs des autres utilisateurs (en regardant les passeports, en
vérifiant les empreintes depuis diverses sources, etc.)

  1 = je ne sais pas ou n'ai pas d'avis
  2 = je ne fais PAS confiance
  3 = je fais très légèrement confiance
  4 = je fais entièrement confiance
  5 = j'attribue une confiance ultime
  m = retour au menu principal

Quelle est votre décision ? 5
Voulez-vous vraiment attribuer une confiance ultime à cette clef ? (o/N) o

sec  rsa4096/51F1EF3EC56945AE
     créé : 2018-04-09  expire : jamais      utilisation : SCA
     confiance : ultime        validité : inconnu
ssb  rsa4096/1F0E86B7D8190FE0
     créé : 2018-04-09  expire : jamais      utilisation : E
[ inconnue] (1). Nicolas Kovacs <info@microlinux.fr>
[ inconnue] (2)  [jpeg image of size 15912]
Veuillez remarquer que la validité affichée pour la clef n'est pas
forcément correcte avant d'avoir relancé le programme.

gpg> quit
```

À partir de là, je peux utiliser la clé GPG avec Thunderbird, KWallet,
KeePassXC, etc. 

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
