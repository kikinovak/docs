
Générer la paire de clés&nbsp;:

```
$ ssh-keygen -t rsa -b 16384
```

* Accepter l'emplacement par défaut pour le stockage de la clé.

* Laisser la zone du mot de passe vide en appuyant deux fois sur ++enter++.

* La génération de la clé peut être assez longue, même sur une machine
  performante.

Transférer la clé publique sur la machine distante&nbsp;:

```
$ ssh-copy-id -i .ssh/id_rsa.pub sandbox.microlinux.lan
```

À présent on peut se connecter à la machine distante&nbsp;:

```
$ ssh sandbox.microlinux.lan
```

On peut très bien utiliser le nom d'hôte simple pour la connexion&nbsp;:

```
$ ssh sandbox
```

> Sur la machine distante, la clé publique transférée apparaît dans le fichier
`~/.ssh/authorized_keys`.

Se connecter sous une autre identité&nbsp;:

```
$ ssh-copy-id -i .ssh/id_rsa.pub microlinux@sandbox.microlinux.fr
...
$ ssh microlinux@sandbox.microlinux.fr
```

Après la réinstallation d’une machine, on peut se retrouver confronté à
l’avertissement suivant&nbsp;:

```
/usr/bin/ssh-copy-id: WARNING: All keys were skipped because they already exist on
the remote system. (if you think this is a mistake, you may want to use -f option)
```

* On pourrait très bien utiliser l’option `-f` pour forcer le transfert de la
  clé publique.

* Il vaut mieux faire le ménage dans le fichier `~/.ssh/authorized_keys` de la
  machine distante.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*

