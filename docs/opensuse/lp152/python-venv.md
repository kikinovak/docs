
# Environnement virtuel Python

OpenSUSE Leap offre une panoplie de paquets Python installables par le biais du
gestionnaire de paquets `zypper`. Il arrive cependant qu'un module ne soit pas
disponible dans les dépôts de paquets. On pourrait être tenté de récupérer les
modules manquants par le biais de l'outil `pip`, mais l'utilisation de `pip` en
tant que `root` est une très mauvaise idée, étant donné que l'on risque
d'écraser une partie des paquets Python installés au niveau du système.

La solution consiste ici à utiliser un environnement virtuel et d'installer les
applications tierces via `pip` en tant qu'utilisateur simple, en redéfinissant
temporairement le `PATH` lorsqu'on en a besoin. 

Si l'on a déjà eu la mauvaise idée d'invoquer `pip` en tant que `root`, on peut
déjà commencer par faire le ménage dans les paquets Python :

```
# rm -rf /usr/lib/python3.6/site-packages/
# zypper install --no-recommends --download in-advance --force \
  $(rpm -qa 'python*' --queryformat '%{NAME} ')
```

Créer l'environnement virtuel dans le répertoire utilisateur&nbsp;:

```
$ cd
$ python3 -m venv .venv
$ ls .venv/
bin  include  lib  lib64  pyvenv.cfg
```

> Le répertoire est nommé `.venv` par simple convention. Rien n'empêche de lui
> donner un nom comme `python-virtual` ou `python-local`. 

Voici le `PATH` par défaut de l'utilisateur&nbsp;:

```
$ echo $PATH
/home/microlinux/bin:/usr/local/bin:/usr/bin:/bin
```

Prendre en compte l'environnement virtuel&nbsp;:

```
source .venv/bin/activate
(.venv) $
```

Le `PATH` est modifié en conséquence&nbsp;:

```
$ echo $PATH
/home/microlinux/.venv/bin:/home/microlinux/bin:/usr/local/bin:/usr/bin:/bin
```

Mettre à jour `pip`&nbsp;:

```
(.venv) $ pip install --upgrade pip
```

La commande `pip list` affiche les composants Python installés localement :

```
$ pip list
Package    Version
---------- -------
pip        21.1.1
setuptools 44.1.1
```

À partir de là, je peux utiliser `pip` pour installer des applications Python
qui ne sont pas disponibles dans les dépôts officiels et sans mettre en péril
l'intégrité de mon système&nbsp;:

```
(.venv) $ pip install mkdocs
(.venv) $ pip install mkdocs-material
(.venv) $ pip install speedtest-cli
$ pip list
Package                    Version
-------------------------- --------
click                      8.0.0
future                     0.18.2
importlib-metadata         4.0.1
Jinja2                     3.0.1
joblib                     1.0.1
livereload                 2.6.3
lunr                       0.5.8
Markdown                   3.3.4
MarkupSafe                 2.0.1
mkdocs                     1.1.2
mkdocs-material            7.1.4
mkdocs-material-extensions 1.0.1
nltk                       3.6.2
pip                        21.1.1
Pygments                   2.9.0
pymdown-extensions         8.2
PyYAML                     5.4.1
regex                      2021.4.4
setuptools                 44.1.1
six                        1.16.0
speedtest-cli              2.1.3
tornado                    6.1
tqdm                       4.60.0
typing-extensions          3.10.0.0
zipp                       3.4.1
```

La commande `deactivate` permet de quitter l'environnement virtuel&nbsp; :

```
$ which mkdocs
/home/microlinux/.venv/bin/mkdocs
(.venv) $ deactivate 
$ which mkdocs
which: no mkdocs in (/home/microlinux/bin:/usr/local/bin:/usr/bin:/bin)
```

Et si l'on n'a plus besoin de l'environnement virtuel&nbsp;:

```
$ rm -rf ~/.venv
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*





