
# OpenSUSE Leap 15.2 sur un MacBook Pro

**Modèle**&nbsp;: MacBook Pro 13" 2009

OpenSUSE Leap 15.2 est installé en remplacement de Mac OS X.

Identifier le matériel sur cette page&nbsp;:
[https://checkcoverage.apple.com/](https://checkcoverage.apple.com/)

Appuyer sur ++c++ pour démarrer sur le lecteur DVD.

J'installe un SSD de 120 Go en remplacement du disque SATA de 160 Go.

> Le remplacement du disque dur nécessite une clé spéciale de chez Apple.

J'ajoute une barrette de RAM pour passer de 2 Go à 6 Go.

Partitionnement&nbsp;:

```
# fdisk -l /dev/sda
Disk /dev/sda: 111.8 GiB, 120040980480 bytes, 234455040 sectors
Disk model: SanDisk SSD PLUS
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xd46699b6

Device     Boot    Start       End   Sectors   Size Id Type
/dev/sda1  *        2048   1026047   1024000   500M 83 Linux
/dev/sda2        1026048  17803263  16777216     8G 82 Linux swap / Solaris
/dev/sda3       17803264 234455039 216651776 103.3G 83 Linux
```

Systèmes de fichiers&nbsp;:

```
# cat /etc/fstab
UUID=03c23936-a6e2-42c5-84fa-3770ba15b0d7  /      ext4  defaults  0  1
UUID=d1573a59-ea47-4970-93ff-9fc3995602a1  /boot  ext2  defaults  0  2
UUID=a310beab-aeb7-452d-afd5-924b2b2cc407  swap   swap  defaults  0  0
```

La carte graphique est une [NVidia GeForce 9400M](nvidia-geforce-9400.md).

La carte Wi-Fi est une [Broadcom BCM 4322](broadcom-bcm4322.md).

Corriger la résolution de la console au démarrage : `1280x800`

Dans les paramètres de KDE, cocher `Désactiver le pavé tactile quand une souris
est branchée`.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*
