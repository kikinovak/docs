
# Carte Wi-Fi Broadcom BCM4322 sous Leap 15.2

Identifier la carte&nbsp;:

```
# lspci | grep -i network
03:00.0 Network controller: Broadcom Inc. and subsidiaries BCM4322 802.11a/b/g/n 
Wireless LAN Controller (rev 01)
```

Vérifier si le paquet `b43-fwcutter` est installé&nbsp;:

```
# rpm -q b43-fwcutter
b43-fwcutter-019-lp152.3.5.x86_64
```

La commande `install_bcm43xx_firmware` permet de récupérer le *firmware*
Broadcom&nbsp;:

```
# install_bcm43xx_firmware
Downloading b43 firmware
####################################################### 100,0%
Extracting b43 firmware
This file is recognised as:
  filename   :  wl_apsta.o
  version    :  784.2
  MD5        :  29c8a47094fbae342902d84881a465ff
Extracting b43/lcn1bsinitvals26.fw
Extracting b43/n0bsinitvals22.fw
Extracting b43/lcn0bsinitvals26.fw
...
Downloading b43legacy firmware
####################################################### 100,0%
Extracting b43legacy firmware
...
b43 firmware successfully installed.
b43legacy firmware successfully installed.
```

Empêcher l'installation du pilote libre `broadcom-wl` qui ne fonctionne pas
correctement avec cette carte&nbsp;:

```
# zypper addlock broadcom-wl
# zypper addlock broadcom-wl-kmp-default
```

À partir de là il suffit de redémarrer.

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*




