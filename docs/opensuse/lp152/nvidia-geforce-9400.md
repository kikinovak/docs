
# NVidia GeForce 9400M sous Leap 15.2

Le pilote propriétaire `nvidia` n'est plus officiellement maintenu par le
constructeur, mais on peut toujours l'utiliser avec un patch. 

Désactiver l'environnement graphique&nbsp;:

```
# systemctl set-default multi-user.target
# systemctl isolate multi-user.target
```

Identifier la carte graphique&nbsp;:

```
# lspci | grep -i vga
02:00.0 VGA compatible controller: NVIDIA Corporation C79 [GeForce 9400M] (rev b1)
```

Le pilote propriétaire `nvidia` est incompatible avec le pilote libre
`nouveau`. Il faut désactiver le chargement de ce pilote, vu que sa simple
présence sur le système peut causer des problèmes&nbsp;:

```
# /etc/modprobe.d/nvidia.conf
blacklist nouveau
```

Étant donné que le pilote `nouveau` utilise le *Kernel Mode Setting*, il faut
également empêcher son chargement au démarrage du système en ajoutant
`nomodeset` aux paramètres du noyau pour GRUB&nbsp;

```
nomodeset quiet mitigations=off
```

Redémarrer pour prendre en compte toutes ces modifications.

Sur le [site de NVidia](https://www.nvidia.com/fr-fr/), suivre les liens
`Pilotes` > `Pilotes GeForce` et sélectionner le pilote approprié dans la
succession de menus déroulants.

* `Type de produit` : `GeForce`

* `Série de produits` : `GeForce 9M Series (Notebooks)`

* `Système d'exploitation` : `Linux 64-bit`

Télécharger l'installateur du pilote `NVIDIA-Linux-x86_64-340.108.run` et le
transférer sur la machine. 

```
# mkdir nvidia
# mv NVIDIA-Linux-x86_64-340.108.run nvidia/
# cd nvidia/
```

Installer les prérequis de construction nécessaires&nbsp;:

```
# zypper install --no-recommends gcc patch kernel-default-devel libglvnd-devel
```

Télécharger un patch nécessaire à la construction et au chargement du
pilote&nbsp;:

```
# wget https://www.microlinux.fr/download/nvidia-340.108.patch
```

> On trouve toute une série de patchs un peu partout pour ce pilote. [Cette
> page](https://www.if-not-true-then-false.com/2015/fedora-nvidia-guide/4/)
> offre des explications relativement claires sur la question. Elle héberge un
> installateur modifié qui fonctionne, mais sans les patchs qui vont avec. Du
> coup j’ai recréé un patch propre à partir de cet installateur, et je
> l’héberge sur mon serveur.

Rendre l'installateur exécutable&nbsp;:

```
# chmod +x NVIDIA-Linux-x86_64-340.108.run
```

Extraire les fichiers de l'installateur et appliquer le patch&nbsp;

```
# ./NVIDIA-Linux-x86_64-340.108.run --extract-only
Verifying archive integrity... OK
Uncompressing NVIDIA Accelerated Graphics Driver for Linux-x86_64 340.108...
# cd NVIDIA-Linux-x86_64-340.108/
# patch -p1 < ../nvidia-340.108.patch
patching file kernel/conftest.sh
patching file kernel/dkms.conf
patching file kernel/Makefile
patching file kernel/nv.c
patching file kernel/nv-drm.c
patching file kernel/nv-linux.h
patching file kernel/nv-procfs.c
patching file kernel/nv-time.h
patching file kernel/os-interface.c
patching file kernel/uvm/conftest.sh
patching file kernel/uvm/Makefile
patching file kernel/uvm/nvidia_uvm_lite.c
```

Lancer l'installateur du pilote&nbsp;:

```
# ./nvidia-installer
```

* Accepter la licence d'utilisateur.

* Le pilote libre `nouveau` est déjà désactivé&nbsp;:
  `Continue installation`

* `Install NVIDIA's 32-bit compatibility libraries?`&nbsp;: `Yes`

* `Run the nvidia-xconfig utility?`&nbsp;: `No`

Redémarrer et effectuer un premier test&nbsp;:

```
# systemctl isolate graphical.target
```

Dans le bureau KDE, il faut corriger l’affichage des polices&nbsp;:
`Configuration du système` > `Polices` > `Forcer le PPP de la police`&nbsp;:
`96`. 

> Cette opération nécessite de relancer le bureau.

Une fois que l'affichage semble correct, on peut basculer vers le démarrage en
mode graphique par défaut&nbsp;:

```
# systemctl set-default graphical.target
```

---

*[Cliquez ici](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
pour offrir un café au rédacteur de cette documentation.*








