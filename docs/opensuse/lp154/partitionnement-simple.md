
# Partitionnement sous OpenSUSE Leap 15.4

## BIOS traditionnel  + table de partitions MBR

Passer en mode manuel&nbsp;:

* `Partitionnement en mode expert` > `Démarrer avec des partitions existantes`.

Créer une table de partitions MBR&nbsp;:

* Mettre le disque (`sda` ou autre) en surbrillance.

* `Périphérique` > `Créer une nouvelle table de partitions` > `MS-DOS`.

Créer la partition `/boot`&nbsp;:

* `Ajouter une partition` > `Partition primaire` > `Taille
  personnalisée`&nbsp;: `500 MiB` > `Rôle`&nbsp;: `Système d'exploitation` >
  `Système de fichiers`&nbsp;: `ext4` > `Point de montage`&nbsp;: `/boot` >
  `Options fstab` > `Label de volume`&nbsp;: `boot`.

Créer la partition `swap`&nbsp;:

* `Ajouter une partition` > `Partition primaire` > `Taille
  personnalisée`&nbsp;: dimensionner en fonction de la RAM disponible >
  `Rôle`&nbsp;: `Swap` > `Options fstab` > `Label de volume`&nbsp;: `swap`. 

Créer la partition `/`&nbsp;:

* `Ajouter une partition` > `Partition primaire` > `Taille maximale` >
  `Rôle`&nbsp;: `Système d'exploitation` > `Système de fichiers`&nbsp;: `ext4`
  &gt; `Point de montage`&nbsp;: `/` > `Options fstab` > `Label de
  volume`&nbsp;: `root`.

```
# lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda      8:0    0   60G  0 disk
├─sda1   8:1    0  500M  0 part /boot
├─sda2   8:2    0    4G  0 part [SWAP]
└─sda3   8:3    0 55,5G  0 part /
sr0     11:0    1 1024M  0 rom

# cat /etc/fstab
UUID=a0d15681-425b-4d36-910b-0d069305af2e  /      ext4  defaults      0  1
UUID=e895cf5f-c8df-425e-8e0b-d3533ba651c5  /boot  ext4  data=ordered  0  2
UUID=108d8b78-69de-4a17-9b7b-0e72a3c7fe73  swap   swap  defaults      0  0

# ls -l /dev/disk/by-label/
total 0
lrwxrwxrwx 1 root root 10 10 juin  06:40 boot -> ../../sda1
lrwxrwxrwx 1 root root 10 10 juin  06:40 root -> ../../sda3
lrwxrwxrwx 1 root root 10 10 juin  06:40 swap -> ../../sda2
```

## BIOS traditionnel + table de partitions GPT

Passer en mode manuel&nbsp;:

* `Partitionnement en mode expert` > `Démarrer avec des partitions existantes`.

Créer une table de partitions GPT&nbsp;:

* Mettre le disque (`sda` ou autre) en surbrillance.

* `Périphérique` > `Créer une nouvelle table de partitions` > `GPT`.

Créer la partition de démarrage BIOS&nbsp;:

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: `8 MiB` >
  `Rôle`&nbsp;: `Volume brut (non formaté)` > `ID de partition`&nbsp;:
  `Partition de démarrage BIOS`.

Créer la partition `/boot`&nbsp;:

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: `500 MiB` >
  `Rôle`&nbsp;: `Système d'exploitation` > `Système de fichiers`&nbsp;: `ext4`
  &gt; `Point de montage`&nbsp;: `/boot` > `Options fstab` > `Label de
  volume`&nbsp;: `boot`.

Créer la partition `swap`&nbsp;:

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: dimensionner en
  fonction de la RAM disponible > `Rôle`&nbsp;: `Swap` > `Options fstab` >
  `Label de volume`&nbsp;: `swap`. 

Créer la partition `/`&nbsp;:

* `Ajouter une partition` > `Taille maximale` > `Rôle`&nbsp;: `Système
  d'exploitation` > `Système de fichiers`&nbsp;: `ext4` &gt; `Point de
  montage`&nbsp;: `/` > `Options fstab` > `Label de volume`&nbsp;: `root`.

```
# lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda      8:0    0   60G  0 disk
├─sda1   8:1    0    8M  0 part
├─sda2   8:2    0  500M  0 part /boot
├─sda3   8:3    0    4G  0 part [SWAP]
└─sda4   8:4    0 55,5G  0 part /
sr0     11:0    1 1024M  0 rom

# cat /etc/fstab
UUID=32efdffa-982c-477b-bda3-520848181cfe  /      ext4  defaults      0  1
UUID=739071b1-b2c8-454a-8764-4d38be5579ca  /boot  ext4  data=ordered  0  2
UUID=a2820e7d-d219-496c-8ff5-fb71e84df189  swap   swap  defaults      0  0

# ls -l /dev/disk/by-label/
total 0
lrwxrwxrwx 1 root root 10 10 juin  07:02 boot -> ../../sda2
lrwxrwxrwx 1 root root 10 10 juin  07:02 root -> ../../sda4
lrwxrwxrwx 1 root root 10 10 juin  07:02 swap -> ../../sda3
```

## UEFI + table de partitions GPT

Passer en mode manuel&nbsp;:

* `Partitionnement en mode expert` > `Démarrer avec des partitions existantes`.

Créer une table de partitions GPT&nbsp;:

* Mettre le disque (`sda` ou autre) en surbrillance.

* `Périphérique` > `Créer une nouvelle table de partitions` > `GPT`.

Créer la partition EFI&nbsp;:

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: `200 MiB` >
  `Rôle`&nbsp;: `Partition de démarrage EFI` > `Options fstab` > `Label de
  volume`&nbsp;: `EFI`.

Créer la partition `/boot`&nbsp;:

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: `500 MiB` >
  `Rôle`&nbsp;: `Système d'exploitation` > `Système de fichiers`&nbsp;: `ext4`
  &gt; `Point de montage`&nbsp;: `/boot` > `Options fstab` > `Label de
  volume`&nbsp;: `boot`.

Créer la partition `swap`&nbsp;:

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: dimensionner en
  fonction de la RAM disponible > `Rôle`&nbsp;: `Swap` > `Options fstab` >
  `Label de volume`&nbsp;: `swap`. 

Créer la partition `/`&nbsp;:

* `Ajouter une partition` > `Taille maximale` > `Rôle`&nbsp;: `Système
  d'exploitation` > `Système de fichiers`&nbsp;: `ext4` &gt; `Point de
  montage`&nbsp;: `/` > `Options fstab` > `Label de volume`&nbsp;: `root`.

```
# lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda      8:0    0   60G  0 disk
├─sda1   8:1    0  200M  0 part /boot/efi
├─sda2   8:2    0  500M  0 part /boot
├─sda3   8:3    0    4G  0 part [SWAP]
└─sda4   8:4    0 55,3G  0 part /
sr0     11:0    1 1024M  0 rom

# cat /etc/fstab
UUID=ab1ba407-acb4-48ab-ae66-3bc28c860629  /          ext4  defaults      0  1
UUID=b33b4d56-ba6f-4cf9-ae1e-2559e5bee286  /boot      ext4  data=ordered  0  2
UUID=90ef85a8-8459-448a-a572-a6d96b3df790  swap       swap  defaults      0  0
UUID=28AA-1D57                             /boot/efi  vfat  utf8          0  2

# ls -l /dev/disk/by-label/
total 0
lrwxrwxrwx 1 root root 10 10 juin  07:20 boot -> ../../sda2
lrwxrwxrwx 1 root root 10 10 juin  07:20 EFI -> ../../sda1
lrwxrwxrwx 1 root root 10 10 juin  07:20 root -> ../../sda4
lrwxrwxrwx 1 root root 10 10 juin  07:20 swap -> ../../sda3
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
