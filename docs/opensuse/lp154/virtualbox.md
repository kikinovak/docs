
# VirtualBox sous OpenSUSE Leap 15.4

## Prérequis

Vérifier si la virtualisation matérielle est activée dans le BIOS/UEFI de la
machine&nbsp;:

* `VT-x` pour les processeurs Intel

* `AMD-v` pour les processeurs AMD

```
$ lscpu | grep Virtual
Virtualisation :        VT-x
```


## Installation

VirtualBox est fourni par les dépôts officiels de la distribution&nbsp;:

```
# zypper install virtualbox-qt
```

## Configuration

Ajouter l'utilisateur au groupe système `vboxusers`&nbsp;:

```
# usermod -aG vboxusers microlinux
```

> Il faudra quitter la session et se reconnecter pour que cet ajout prenne
> effet.

Créer le répertoire qui hébergera les machines virtuelles&nbsp;:

```
$ mkdir ~/VirtualBox
```

Lancer VirtualBox et configurer l'utilisation par défaut de ce
répertoire&nbsp;:

* `Fichier` > `Paramètres` > `Dossier par défaut des machines`&nbsp;:
  `~/VirtualBox`.

Éditer un fichier `/etc/vbox/networks.conf` pour autoriser la création de
réseaux privés virtuels&nbsp;:

```
# /etc/vbox/networks.conf
* 0.0.0.0/0 ::/0
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
