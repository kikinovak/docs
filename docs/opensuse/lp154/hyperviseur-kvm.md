
# Hyperviseur KVM sous OpenSUSE Leap 15.4

## Prérequis

Vérifier si le processeur supporte la virtualisation matérielle&nbsp;:

```
# grep -E 'svm|vmx' /proc/cpuinfo
flags : ... vmx ...
```

Alternativement&nbsp; :

```
# lscpu | grep Virtual
Virtualization:      VT-x
```


## Installation

Installer KVM et les outils correspondants&nbsp; :

```
# zypper install qemu-kvm virt-manager libvirt
```

Alternativement&nbsp;:

```
# zypper install -t pattern kvm_server kvm_tools
```


## Mise en service

Vérifier si les modules KVM sont chargés&nbsp; :

```
# lsmod | grep kvm
kvm_intel             327680  0
kvm                  1019904  1 kvm_intel
...
```

Lancer les services `libvirtd` et `libvirt-guests`&nbsp;:

```
# systemctl enable libvirtd --now
# systemctl enable libvirt-guests --now
```

Ajouter l'utilisateur au groupe système `libvirt`&nbsp;:

```
# usermod -aG libvirt microlinux
```

## Configuration d'un bridge

Afficher la configuration réseau et noter les paramètres&nbsp;:

```
# ip --brief address show
lo               UNKNOWN        127.0.0.1/8 ...
p4p1             UP             192.168.2.3/24 ...

# ip route show
default via 192.168.2.1 ...
```

> L'interface réseau de l'hôte est `p4p1` dans l'exemple. Bien évidemment, il
> faudra adapter la configuration en conséquence.

Lancer NetworkManager TUI&nbsp;:

```
# LANG=en_US.UTF-8 && nmtui
```

> L'opération devra être effectuée localement, faute de quoi on scie la branche
> sur laquelle on est assis.

Configurer le *bridge* `br0`&nbsp;:

* Sélectionner `Edit a connection`.

* `Add` > `Bridge` > `Create`

* `Profile name` : `BRIDGE`

* `Device` : `br0`

* `Bridge Slaves` > `Add`

* `Slave connection` > `Ethernet` > `Create`

    - `Profile name` : `LAN`

    - `Device` : `p4p1`

    - Confirmer par `OK`.

* Décocher `Enable STP (Spanning Tree Protocol)`.

* Passer `IPv6 CONFIGURATION` de `Automatic` à `Ignore`.

* Confirmer par `OK`.

* Revenir à la fenêtre principale.

* Sélectionner `Activate a connection`.

* Les connexions `BRIDGE` et `p4p1` doivent être actives (`*`).

* Revenir à la fenêtre principale.

* Sélectionner `Edit a connection`.

* Supprimer la connexion `p4p1`.

* Revenir à la fenêtre principale.

* Quitter.

Vérifier la configuration du *bridge*&nbsp;:

```
# ip --brief address show
lo               UNKNOWN        127.0.0.1/8 ...
p4p1             UP
br0              UP             192.168.2.3/24 ...
```

Lancer Virtual Machine Manager et supprimer le réseau `default` (`virbr0`).

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
