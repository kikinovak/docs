# Installer OpenSUSE Leap 15.4

## Démarrer l'installation

L'écran d'accueil de l'installateur ne s'affiche pas de la même manière si la
machine utilise un BIOS traditionnel ou l'UEFI. La version traditionnelle
permet de choisir la langue au moment du démarrage. Si l'on utilise l'UEFI, on
fera ce choix un peu plus tard.

* Insérer la clé USB dans le PC.

* Allumer le PC en affichant le menu de démarrage.

* Sélectionner la clé USB en mode `UEFI` ou `Legacy`, peu importe.

* Sur une machine équipée d'un BIOS traditionnel, appuyer sur ++f2++ pour
  afficher les paramètres régionaux et sélectionner `Français` dans le menu
  déroulant.

* Sélectionner `Installation`.

> Sur un système UEFI, on peut très bien laisser l'option `Secure Boot`
> activée, étant donné que la distribution OpenSUSE le gère parfaitement.

### Langue et clavier

* Choisir la langue si ce n'est déjà fait&nbsp;: `French - Français`.

* Éventuellement, choisir la disposition clavier par défaut.

* Cliquer sur `Suivant`.

### Dépôts en ligne

* Activer les dépôts en ligne maintenant&nbsp;? `Oui`.

* Confirmer les dépôts par défaut en cliquant sur `Suivant`.

* Attendre la synchronisation initiale des dépôts en ligne.

### Rôle système

* Sélectionner `Bureau avec KDE Plasma`.

* Cliquer sur `Suivant`.

> J'avoue platement ma préférence marquée pour l'environnement de bureau KDE.
> Plasma&nbsp;5 me semble être le bureau le plus mature et le plus fonctionnel
> du monde du logiciel libre. C'est l'environnement graphique proposé par
> défaut par OpenSUSE. Son implémentation dans cette distribution a toujours
> été particulièrement soignée, avec une petite touche personnalisée sobre et
> sympathique.  

### Partitionnement guidé

OpenSUSE Leap utilise un partitionnement basé sur `Btrfs` par défaut. On optera
pour un schéma plus simple basé sur le système de fichiers `ext4`.

* Cliquer sur `Installation guidée`.

* Systèmes Windows existants&nbsp;: `Supprimer même si ce n'est pas
  nécessaire`.

* Partitions Linux existantes&nbsp;: `Supprimer même si ce n'est pas
  nécessaire`.

* Autres partitions&nbsp;: `Supprimer même si ce n'est pas nécessaire`.

* Cliquer sur `Suivant`.

* Ne pas cocher `Activer la gestion des volumes logiques (LVM)`.

* Ne pas cocher `Enable Disk Encryption`.

* Cliquer sur `Suivant`.

* Type de système de fichiers&nbsp;: `ext4`.

* Ne pas cocher `Proposer une partition personnelle séparée`.

* Cocher `Proposer une partition swap séparée`.

* Cocher `Agrandir jusqu'à la taille de la RAM pour la mise en veille`.

* Cliquer sur `Suivant` pour afficher le récapitulatif du partitionnement.

* Confirmer en cliquant sur `Suivant`.

### Fuseau horaire

* Vérifier la région&nbsp;: `Europe`.

* Vérifier le fuseau horaire&nbsp;: `France`.

* Adapter cette configuration selon les besoins.

* Cocher `Horloge matérielle réglée sur UTC`.

* Cliquer sur `Suivant`.

### Utilisateurs locaux

Créer un utilisateur initial&nbsp;:

* Nom complet&nbsp;: par exemple `Microlinux`.

* Nom d'utilisateur&nbsp;: par exemple `microlinux`.

* Choisir un mot de passe raisonnablement compliqué.

* Confirmer le mot de passe.

* Cocher `Utiliser ce mot de passe pour l'administrateur du système`.

* Décocher `Login automatique`.

* Cliquer sur `Suivant`.

### Paramètres d'installation

* Mitigations CPU&nbsp;: cliquer sur `Auto`.

* Dans l'écran subséquent, passer les mitigations CPU de `Auto` à `Désactivé`
  dans le menu déroulant.

* Confirmer par `OK` pour revenir à la vue d'ensemble des paramètres
  d'installation.

* Pare-feu&nbsp;: cliquer sur `désactiver`.

* Service SSH&nbsp;: cliquer sur `activer`.

* Cliquer sur `Installer` pour lancer l'installation.

* Confirmer en cliquant encore une fois sur `Installer`.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
