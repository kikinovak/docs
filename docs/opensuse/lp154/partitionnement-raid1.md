
# RAID 1 sous OpenSUSE Leap 15.4

## BIOS traditionnel  + table de partitions MBR

Passer en mode manuel&nbsp;:

* `Partitionnement en mode expert` > `Démarrer avec des partitions existantes`.

Supprimer les grappes RAID existantes.

Créer une table de partitions MBR sur chaque disque&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Périphérique` > `Créer une nouvelle table de partitions` > `MS-DOS`.

* Procéder de même pour le disque `sdb`.

Créer deux partitions de type `Linux RAID` d'une taille de 500 Mo&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Ajouter une partition` > `Partition primaire` > `Taille
  personnalisée`&nbsp;: `500 MiB` > `Volume brut (non formaté)` > `ID de
  partition`&nbsp;: `RAID Linux`.

* Procéder de même pour le disque `sdb`.

Créer deux partitions de type `Linux RAID` d'une taille équivalant à la
quantité de RAM disponible&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Ajouter une partition` > `Partition primaire` > `Taille
  personnalisée`&nbsp;: dimensionner en fonction de la RAM dispoible > `Volume
  brut (non formaté)` > `ID de partition`&nbsp;: `RAID Linux`.

* Procéder de même pour le disque `sdb`.

Créer deux partitions de type `Linux RAID` occupant l'espace restant du
disque&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Ajouter une partition` > `Partition primaire` > `Taille maximale` > `Volume
  brut (non formaté)` > `ID de partition`&nbsp;: `RAID Linux`.

* Procéder de même pour le disque `sdb`.

Assembler la grappe RAID `/dev/md/boot`&nbsp;:

* `RAID` > `Ajouter RAID` > `Type de RAID`&nbsp;: `RAID 1 (mise en miroir)` >
  `Nom du RAID`&nbsp;: `boot`.

* Sélectionner `/dev/sda1` et `/dev/sdb1` en maintenant la touche ++ctrl++
  appuyée > `Ajouter`.

* `Taille des blocs`&nbsp;: `4 KiB`.

* `Modifier` > `Rôle`&nbsp;: `Système d'exploitation` > `Système de
  fichiers`&nbsp;: `ext4` > `Point de montage`&nbsp;: `/boot` > `Options fstab`
  &gt; `Label de volume`&nbsp;: `boot`.

Assembler la grappe RAID `/dev/md/swap`&nbsp;:

* `RAID` > `Ajouter RAID` > `Type de RAID`&nbsp;: `RAID 1 (mise en miroir)` >
  `Nom du RAID`&nbsp;: `swap`.

* Sélectionner `/dev/sda2` et `/dev/sdb2` en maintenant la touche ++ctrl++
  appuyée > `Ajouter`.

* `Taille des blocs`&nbsp;: `4 KiB`.

* `Modifier` > `Rôle`&nbsp;: `Swap` > `Options fstab` > `Label de
  volume`&nbsp;: `swap`. 

Assembler la grappe RAID `/dev/md/root`&nbsp;:

* `RAID` > `Ajouter RAID` > `Type de RAID`&nbsp;: `RAID 1 (mise en miroir)` >
  `Nom du RAID`&nbsp;: `root`.

* Sélectionner `/dev/sda3` et `/dev/sdb3` en maintenant la touche ++ctrl++
  appuyée > `Ajouter`.

* `Taille des blocs`&nbsp;: `4 KiB`.

* `Modifier` > `Rôle`&nbsp;: `Système d'exploitation` > `Système de
  fichiers`&nbsp;: `ext4` > `Point de montage`&nbsp;: `/` > `Options fstab`
  &gt; `Label de volume`&nbsp;: `root`.

```
# lsblk
NAME      MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
sda         8:0    0    60G  0 disk
├─sda1      8:1    0   500M  0 part
│ └─md127   9:127  0 499,9M  0 raid1 /boot
├─sda2      8:2    0     4G  0 part
│ └─md125   9:125  0     4G  0 raid1 [SWAP]
└─sda3      8:3    0  55,5G  0 part
  └─md126   9:126  0  55,5G  0 raid1 /
sdb         8:16   0    60G  0 disk
├─sdb1      8:17   0   500M  0 part
│ └─md127   9:127  0 499,9M  0 raid1 /boot
├─sdb2      8:18   0     4G  0 part
│ └─md125   9:125  0     4G  0 raid1 [SWAP]
└─sdb3      8:19   0  55,5G  0 part
  └─md126   9:126  0  55,5G  0 raid1 /
sr0        11:0    1  1024M  0 rom

# cat /etc/fstab
UUID=fee9de89-cfff-4121-8def-922f8a8f1895  /      ext4  defaults      0  1
UUID=ec2af884-5f64-4975-b233-e6d1fd4334fb  swap   swap  defaults      0  0
UUID=e262c135-c990-423f-ae63-eaa14ce87fce  /boot  ext4  data=ordered  0  2

# ls -l /dev/disk/by-label/
total 0
lrwxrwxrwx 1 root root 11 11 juin  08:50 boot -> ../../md127
lrwxrwxrwx 1 root root 11 11 juin  08:50 root -> ../../md126
lrwxrwxrwx 1 root root 11 11 juin  08:50 swap -> ../../md125

# cat /proc/mdstat
Personalities : [raid1]
md125 : active raid1 sdb2[1] sda2[0]
      4194240 blocks super 1.0 [2/2] [UU]
      bitmap: 0/1 pages [0KB], 65536KB chunk

md126 : active raid1 sda3[0] sdb3[1]
      58207104 blocks super 1.0 [2/2] [UU]
      bitmap: 1/1 pages [4KB], 65536KB chunk

md127 : active raid1 sda1[0] sdb1[1]
      511936 blocks super 1.0 [2/2] [UU]
      bitmap: 1/1 pages [4KB], 65536KB chunk
```

## BIOS traditionnel + table de partitions GPT

Passer en mode manuel&nbsp;:

* `Partitionnement en mode expert` > `Démarrer avec des partitions existantes`.

Supprimer les grappes RAID existantes.

Créer une table de partitions GPT sur chaque disque&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Périphérique` > `Créer une nouvelle table de partitions` > `GPT`.

* Procéder de même pour le disque `sdb`.

Créer une partition de démarrage BIOS sur chaque disque&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: `8 MiB` >
  `Rôle`&nbsp;: `Volume brut (non formaté)` > `ID de partition`&nbsp;:
  `Partition de démarrage BIOS`.

* Procéder de même pour le disque `sdb`.

Créer deux partitions de type `Linux RAID` d'une taille de 500 Mo&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: `500 MiB` > `Volume
  brut (non formaté)` > `ID de partition`&nbsp;: `RAID Linux`.

* Procéder de même pour le disque `sdb`.

Créer deux partitions de type `Linux RAID` d'une taille équivalant à la
quantité de RAM disponible&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: dimensionner en
  fonction de la RAM dispoible > `Volume brut (non formaté)` > `ID de
  partition`&nbsp;: `RAID Linux`.

* Procéder de même pour le disque `sdb`.

Créer deux partitions de type `Linux RAID` occupant l'espace restant du
disque&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Ajouter une partition` > `Taille maximale` > `Volume brut (non formaté)` >
  `ID de partition`&nbsp;: `RAID Linux`.

* Procéder de même pour le disque `sdb`.

Assembler la grappe RAID `/dev/md/boot`&nbsp;:

* `RAID` > `Ajouter RAID` > `Type de RAID`&nbsp;: `RAID 1 (mise en miroir)` >
  `Nom du RAID`&nbsp;: `boot`.

* Sélectionner `/dev/sda2` et `/dev/sdb2` en maintenant la touche ++ctrl++
  appuyée > `Ajouter`.

* `Taille des blocs`&nbsp;: `4 KiB`.

* `Modifier` > `Rôle`&nbsp;: `Système d'exploitation` > `Système de
  fichiers`&nbsp;: `ext4` > `Point de montage`&nbsp;: `/boot` > `Options fstab`
  &gt; `Label de volume`&nbsp;: `boot`.

Assembler la grappe RAID `/dev/md/swap`&nbsp;:

* `RAID` > `Ajouter RAID` > `Type de RAID`&nbsp;: `RAID 1 (mise en miroir)` >
  `Nom du RAID`&nbsp;: `swap`.

* Sélectionner `/dev/sda3` et `/dev/sdb3` en maintenant la touche ++ctrl++
  appuyée > `Ajouter`.

* `Taille des blocs`&nbsp;: `4 KiB`.

* `Modifier` > `Rôle`&nbsp;: `Swap` > `Options fstab` > `Label de
  volume`&nbsp;: `swap`. 

Assembler la grappe RAID `/dev/md/root`&nbsp;:

* `RAID` > `Ajouter RAID` > `Type de RAID`&nbsp;: `RAID 1 (mise en miroir)` >
  `Nom du RAID`&nbsp;: `root`.

* Sélectionner `/dev/sda4` et `/dev/sdb4` en maintenant la touche ++ctrl++
  appuyée > `Ajouter`.

* `Taille des blocs`&nbsp;: `4 KiB`.

* `Modifier` > `Rôle`&nbsp;: `Système d'exploitation` > `Système de
  fichiers`&nbsp;: `ext4` > `Point de montage`&nbsp;: `/` > `Options fstab`
  &gt; `Label de volume`&nbsp;: `root`.

```
# lsblk
NAME      MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
sda         8:0    0    60G  0 disk
├─sda1      8:1    0     8M  0 part
├─sda2      8:2    0   500M  0 part
│ └─md126   9:126  0 499,9M  0 raid1 /boot
├─sda3      8:3    0     4G  0 part
│ └─md125   9:125  0     4G  0 raid1 [SWAP]
└─sda4      8:4    0  55,5G  0 part
  └─md127   9:127  0  55,5G  0 raid1 /
sdb         8:16   0    60G  0 disk
├─sdb1      8:17   0     8M  0 part
├─sdb2      8:18   0   500M  0 part
│ └─md126   9:126  0 499,9M  0 raid1 /boot
├─sdb3      8:19   0     4G  0 part
│ └─md125   9:125  0     4G  0 raid1 [SWAP]
└─sdb4      8:20   0  55,5G  0 part
  └─md127   9:127  0  55,5G  0 raid1 /
sr0        11:0    1  1024M  0 rom

# cat /etc/fstab
UUID=1e55ed5c-0fca-45a8-95a3-5198c0eb2184  /      ext4  defaults      0  1
UUID=a7f6ecc0-98c8-4bed-84fc-240e35bdcf56  swap   swap  defaults      0  0
UUID=d240a33f-113c-4e7a-89e0-84434005453f  /boot  ext4  data=ordered  0  2

# ls -l /dev/disk/by-label/
total 0
lrwxrwxrwx 1 root root 11 11 juin  09:27 boot -> ../../md126
lrwxrwxrwx 1 root root 11 11 juin  09:27 root -> ../../md127
lrwxrwxrwx 1 root root 11 11 juin  09:27 swap -> ../../md125

# cat /proc/mdstat
Personalities : [raid1]
md125 : active raid1 sda3[0] sdb3[1]
      4194240 blocks super 1.0 [2/2] [UU]
      bitmap: 0/1 pages [0KB], 65536KB chunk

md126 : active raid1 sda2[0] sdb2[1]
      511936 blocks super 1.0 [2/2] [UU]
      bitmap: 1/1 pages [4KB], 65536KB chunk

md127 : active raid1 sda4[0] sdb4[1]
      58198912 blocks super 1.0 [2/2] [UU]
      bitmap: 1/1 pages [4KB], 65536KB chunk
```

## UEFI + table de partitions GPT

Passer en mode manuel&nbsp;:

* `Partitionnement en mode expert` > `Démarrer avec des partitions existantes`.

Supprimer les grappes RAID existantes.

Créer une table de partitions GPT sur chaque disque&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Périphérique` > `Créer une nouvelle table de partitions` > `GPT`.

* Procéder de même pour le disque `sdb`.

Créer deux partitions de type `Linux RAID` d'une taille de 200 Mo&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: `200 MiB` > `Volume
  brut (non formaté)` > `ID de partition`&nbsp;: `RAID Linux`.

* Procéder de même pour le disque `sdb`.

Créer deux partitions de type `Linux RAID` d'une taille de 500 Mo&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: `500 MiB` > `Volume
  brut (non formaté)` > `ID de partition`&nbsp;: `RAID Linux`.

* Procéder de même pour le disque `sdb`.

Créer deux partitions de type `Linux RAID` d'une taille équivalant à la
quantité de RAM disponible&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Ajouter une partition` > `Taille personnalisée`&nbsp;: dimensionner en
  fonction de la RAM dispoible > `Volume brut (non formaté)` > `ID de
  partition`&nbsp;: `RAID Linux`.

* Procéder de même pour le disque `sdb`.

Créer deux partitions de type `Linux RAID` occupant l'espace restant du
disque&nbsp;:

* Mettre le disque `sda` en surbrillance.

* `Ajouter une partition` > `Taille maximale` > `Volume brut (non formaté)` >
  `ID de partition`&nbsp;: `RAID Linux`.

* Procéder de même pour le disque `sdb`.

Assembler la grappe RAID `/dev/md/efi`&nbsp;:

* `RAID` > `Ajouter RAID` > `Type de RAID`&nbsp;: `RAID 1 (mise en miroir)` >
  `Nom du RAID`&nbsp;: `efi`.

* Sélectionner `/dev/sda1` et `/dev/sdb1` en maintenant la touche ++ctrl++
  appuyée > `Ajouter`.

* `Taille des blocs`&nbsp;: `4 KiB`.

* `Modifier` > `Rôle`&nbsp;: `Partition de démarrage EFI` > `Options fstab`
  &gt; `Label de volume`&nbsp;: `EFI`.

Assembler la grappe RAID `/dev/md/boot`&nbsp;:

* `RAID` > `Ajouter RAID` > `Type de RAID`&nbsp;: `RAID 1 (mise en miroir)` >
  `Nom du RAID`&nbsp;: `boot`.

* Sélectionner `/dev/sda2` et `/dev/sdb2` en maintenant la touche ++ctrl++
  appuyée > `Ajouter`.

* `Taille des blocs`&nbsp;: `4 KiB`.

* `Modifier` > `Rôle`&nbsp;: `Système d'exploitation` > `Système de
  fichiers`&nbsp;: `ext4` > `Point de montage`&nbsp;: `/boot` > `Options fstab`
  &gt; `Label de volume`&nbsp;: `boot`.

Assembler la grappe RAID `/dev/md/swap`&nbsp;:

* `RAID` > `Ajouter RAID` > `Type de RAID`&nbsp;: `RAID 1 (mise en miroir)` >
  `Nom du RAID`&nbsp;: `swap`.

* Sélectionner `/dev/sda3` et `/dev/sdb3` en maintenant la touche ++ctrl++
  appuyée > `Ajouter`.

* `Taille des blocs`&nbsp;: `4 KiB`.

* `Modifier` > `Rôle`&nbsp;: `Swap` > `Options fstab` > `Label de
  volume`&nbsp;: `swap`. 

Assembler la grappe RAID `/dev/md/root`&nbsp;:

* `RAID` > `Ajouter RAID` > `Type de RAID`&nbsp;: `RAID 1 (mise en miroir)` >
  `Nom du RAID`&nbsp;: `root`.

* Sélectionner `/dev/sda4` et `/dev/sdb4` en maintenant la touche ++ctrl++
  appuyée > `Ajouter`.

* `Taille des blocs`&nbsp;: `4 KiB`.

* `Modifier` > `Rôle`&nbsp;: `Système d'exploitation` > `Système de
  fichiers`&nbsp;: `ext4` > `Point de montage`&nbsp;: `/` > `Options fstab`
  &gt; `Label de volume`&nbsp;: `root`.

> On peut sereinement ignorer l'avertissement relatif à la partition système
> EFI sur un RAID1 logiciel. Ça fonctionne très bien. 

```
# lsblk
NAME      MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
sda         8:0    0    60G  0 disk
├─sda1      8:1    0   200M  0 part
│ └─md126   9:126  0 199,9M  0 raid1 /boot/efi
├─sda2      8:2    0   500M  0 part
│ └─md125   9:125  0 499,9M  0 raid1 /boot
├─sda3      8:3    0     4G  0 part
│ └─md127   9:127  0     4G  0 raid1 [SWAP]
└─sda4      8:4    0  55,3G  0 part
  └─md124   9:124  0  55,3G  0 raid1 /
sdb         8:16   0    60G  0 disk
├─sdb1      8:17   0   200M  0 part
│ └─md126   9:126  0 199,9M  0 raid1 /boot/efi
├─sdb2      8:18   0   500M  0 part
│ └─md125   9:125  0 499,9M  0 raid1 /boot
├─sdb3      8:19   0     4G  0 part
│ └─md127   9:127  0     4G  0 raid1 [SWAP]
└─sdb4      8:20   0  55,3G  0 part
  └─md124   9:124  0  55,3G  0 raid1 /
sr0        11:0    1  1024M  0 rom

# cat /etc/fstab
UUID=a49cbb48-0c09-496e-8c54-5d0d808a9ec4  /          ext4  defaults      0  1
UUID=5b51610a-8a1f-455b-b3a8-b1c44e95bed8  swap       swap  defaults      0  0
UUID=ead483fb-e18d-4dde-b249-82801f36b163  /boot      ext4  data=ordered  0  2
UUID=9899-22CB                             /boot/efi  vfat  utf8          0  2

# ls -l /dev/disk/by-label/
total 0
lrwxrwxrwx 1 root root 11 11 juin  10:04 boot -> ../../md125
lrwxrwxrwx 1 root root 11 11 juin  10:04 EFI -> ../../md126
lrwxrwxrwx 1 root root 11 11 juin  10:04 root -> ../../md124
lrwxrwxrwx 1 root root 11 11 juin  10:04 swap -> ../../md127

# cat /proc/mdstat
Personalities : [raid1]
md124 : active raid1 sda4[0] sdb4[1]
      58002304 blocks super 1.0 [2/2] [UU]
      bitmap: 1/1 pages [4KB], 65536KB chunk

md125 : active raid1 sda2[0] sdb2[1]
      511936 blocks super 1.0 [2/2] [UU]
      bitmap: 1/1 pages [4KB], 65536KB chunk

md126 : active raid1 sda1[0] sdb1[1]
      204736 blocks super 1.0 [2/2] [UU]
      bitmap: 0/1 pages [0KB], 65536KB chunk

md127 : active raid1 sda3[0] sdb3[1]
      4194240 blocks super 1.0 [2/2] [UU]
      bitmap: 0/1 pages [0KB], 65536KB chunk
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
