
# Télécharger OpenSUSE Leap 15.4

## Le choix de la distribution

La distribution OpenSUSE est proposée en deux moutures&nbsp;:

* la version stable Leap&nbsp;;

* la version de développement Tumbleweed.

OpenSUSE Leap constitue une synthèse assez réussie entre la stabilité et
l'innovation&nbsp;:

* Le système de base est constitué des paquets directement issus de SUSE Linux
  Enterprise. 

* Sur cette base éprouvée, les mainteneurs de la distribution proposent une
  panoplie d'environnements de bureau modernes avec des applications graphiques
  récentes.

## Téléchargement BitTorrent

* Ouvrir la page de téléchargement [get.opensuse.org](https://get.opensuse.org).

* Suivre le lien [Leap 15.4](https://get.opensuse.org/leap/15.4/).

* Cliquer sur [Download](https://get.opensuse.org/leap/15.4/#download).

* Déplier le menu `x86_64` > `Download` et sélectionner `Torrent File`.

* Télécharger l'ISO (3.8 Go).

> Les anciennes versions sont également disponibles ici, même si elles sont
> EOL. Il suffit de remplacer le numéro de version dans l'URL, par exemple
> [https://get.opensuse.org/leap/15.3](https://get.opensuse.org/leap/15.3).

## Téléchargement HTTP/HTTPS

* Ouvrir la page de téléchargement
  [download.opensuse.org](https://download.opensuse.org). 

* Suivre successivement les liens `distribution` > `leap` > `15.4` > `iso`.

* Repérer le lien `openSUSE-Leap-15.4-DVD-x86_64-Media.iso`.

* Un clic sur le lien télécharge l'ISO correspondant, par exemple
  `openSUSE-Leap-15.4-DVD-x86_64-Build243.2-Media.iso`. 


## Vérifier l'intégrité du téléchargement

* Ouvrir la page de téléchargement sur
  [download.opensuse.org](https://download.opensuse.org/distribution/leap/15.4/iso/).

* Télécharger les fichiers `.iso.sha256` et `.iso.sha256.asc` qui
  correspondent à l'ISO.

```
$ ls *.sha256*
openSUSE-Leap-15.4-DVD-x86_64-Media.iso.sha256
openSUSE-Leap-15.4-DVD-x86_64-Media.iso.sha256.asc
```

Récupérer la clé GPG du projet OpenSUSE&nbsp;:

```
$ gpg --keyserver pgp.mit.edu --recv-keys \
  0x22C07BA534178CD02EFE22AAB88B2FD43DBDC284
```

Vérifier l'authenticité du fichier de contrôle&nbsp;:

```
$ gpg --verify openSUSE-Leap-15.4-DVD-x86_64-Media.iso.sha256.asc \
  openSUSE-Leap-15.4-DVD-x86_64-Media.iso.sha256
gpg: Signature made Mon 30 May 2022 07:05:44 PM CEST
gpg:                using RSA key B88B2FD43DBDC284
gpg: Good signature from "openSUSE Project Signing Key <opensuse@opensuse.org>"
...
```

Afficher la somme de contrôle&nbsp;:

```
$ cat openSUSE-Leap-15.4-DVD-x86_64-Media.iso.sha256
4683345f242397c7fd7d89a50731a120ffd60a24460e21d2634e783b3c169695 ...
```

Calculer la somme de contrôle de l'ISO&nbsp;:

```
$ sha256sum openSUSE-Leap-15.4-DVD-x86_64-Build243.2-Media.iso
4683345f242397c7fd7d89a50731a120ffd60a24460e21d2634e783b3c169695 ...
```

## Confectionner la clé USB d'installation

Le fichier ISO est hybride. On peut donc directement l'écrire sur la clé&nbsp;:

```
# dd if=openSUSE-Leap-15.4-DVD-x86_64-Build243.2-Media.iso of=/dev/sdX \
  bs=1M status=progress
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>



~                                                                                                                      
~                                                            
