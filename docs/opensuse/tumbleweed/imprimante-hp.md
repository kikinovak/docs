
# Configurer une imprimante/scanner HP sous OpenSUSE Tumbleweed


## Prérequis

Noter la référence de l'imprimante (par exemple `OfficeJet Pro 6970`) et
vérifier la version minimale requise pour HPLIP [sur le site de
HP](https://bit.ly/39MEuN4).

Comparer avec la version fournie par OpenSUSE&nbsp;:

```
# zypper info hplip | grep Version
Version        : 3.22.4-2.3
```

> Étant donné que Tumbleweed est une *rolling release*, les paquets HPLIP sont
> fournis dans une version raisonnablement récente. 

Vérifier si le serveur d'impression fonctionne&nbsp;:

```
# systemctl status cups
● cups.service - CUPS Scheduler
  Loaded: loaded (/usr/lib/systemd/system/cups.service; enabled; 
                  vendor preset: enabled)
  Active: active (running) ...
```


## Installation

Installer HPLIP&nbsp;:

```
# zypper install hplip
```

Installer le plug-in HP&nbsp;:

```
$ hp-plugin
```

> Même si l'outil `hp-plugin` demande de fournir le mot de passe `root`, il
> doit impérativement être lancé en tant qu'utilisateur simple.

* Sélectionner `Download and install the plug-in from an HP authorized server`
  et cliquer sur `Next`.

* Accepter les termes de la licence et cliquer sur `Next`.

* Fournir le mot de passe `root` et terminer en cliquant deux fois
  successivement sur `OK`.

```
HP Linux Imaging and Printing System (ver. 3.22.4)
Plugin Download and Install Utility ver. 2.1

Copyright (c) 2001-18 HP Development Company, LP
This software comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to distribute it
under certain conditions. See COPYING file for more details.

Plug-in version: 3.22.4
Installed HPLIP version: 3.22.4
Number of files to install: 64

...
 
Done.
 Plug-in installation successful 
```


## Configurer l'imprimante

Lancer l'outil de configuration YaST&nbsp;:

* `Matériel` > `Imprimante` > `Ajouter` > `Lancer hp-setup`.

* `Network/Ethernet/Wireless network` > `Show Advanced Options`.

* Cocher `Manual Discovery` et renseigner le nom d'hôte ou l'adresse IP de
  l'imprimante dans le réseau local.

* Vérifier si l'imprimante a bien été détectée et cliquer sur `Next`.

* `Description`&nbsp;: par exemple `HP OfficeJet Pro 6970`.

* `Location`&nbsp;: par exemple `Bureau`.

* Décocher `Fax Setup`.

* Ne pas cocher `Send test page to printer`. 

* Cliquer sur `Add Printer`.

* Sélectionner l'imprimante et cliquer sur `Modifier`.

* Définir la taille de papier `A4` par défaut.

* Éventuellement, cocher `Utiliser par défaut`.

* Terminer en cliquant sur `OK`.

* Imprimer la page de test.


## Configurer le scanner 

Toujours dans l'outil YaST&nbsp;:

* `Matériel` > `Scanneur` : 

* Confirmer le scanneur détecté en cliquant sur `OK`.

* Quitter YaST.

Ouvrir l'application de numérisation Simple Scan&nbsp;:

* `Préférences` > `Taille de la page`&nbsp;: `A4` 

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
