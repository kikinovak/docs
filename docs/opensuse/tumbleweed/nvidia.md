
# Configurer une carte graphique NVidia sous OpenSUSE Tumbleweed

Dans la configuration par défaut d'un poste de travail OpenSUSE, les cartes
graphiques NVidia sont gérées par le pilote libre `nouveau` en mode KMS
(*Kernel Mode Setting*), c'est-à-dire que le module en question est chargé très
tôt dans le processus de démarrage de la machine. Mes expériences avec ces
pilotes n'ont pas été concluantes, et je préfère les remplacer par les pilotes
propriétaires fournis par le fabricant.

## Identifier la carte et le pilote

Identifier la carte graphique&nbsp;:

```
# lspci | grep -i vga
01:00.0 VGA compatible controller: NVIDIA Corporation GK208B
[GeForce GT 710] (rev a1)
```

Rechercher la version du pilote pour cette carte [sur le site du
constructeur](https://www.nvidia.fr/Download/index.aspx).

Identifier le paquet correspondant&nbsp;:

```
# zypper search --details x11-video-nvidiaG0?
...
S | Name                | Type    | Version         | Arch   | Repository
--+---------------------+---------+-----------------+--------+-----------
  | x11-video-nvidiaG04 | package | 390.151-18.1    | x86_64 | nvidia
  | x11-video-nvidiaG04 | package | 390.151-18.1    | i586   | nvidia
  | x11-video-nvidiaG05 | package | 470.129.06-54.1 | x86_64 | nvidia
  | x11-video-nvidiaG06 | package | 515.57-12.1     | x86_64 | nvidia
```

## Installation

Basculer en mode console&nbsp;:

```
# systemctl set-default multi-user.target
# systemctl isolate multi-user.target
```

Le dépôt NVidia doit être configuré&nbsp;:

```
# cat /etc/zypp/repos.d/nvidia.repo
[nvidia]
enabled=1
autorefresh=0
baseurl=http://download.nvidia.com/opensuse/tumbleweed
priority=100
keeppackages=1
```

Installer le pilote&nbsp;:

```
# zypper install --auto-agree-with-licenses x11-video-nvidiaG05
```

> Vérifier en passant si le système est à jour.

Redémarrer&nbsp;:

```
# reboot
```

## Configuration

Générer le fichier `/etc/X11/xorg.conf`&nbsp;:

```
# nvidia-xconfig
```

Lancer YaST en ligne de commande&nbsp;:

```
# yast
```

Configurer le chargeur de démarrage GRUB&nbsp;: 

* Ouvrir `System` > `Boot Loader` > `Kernel Parameters`.

* Supprimer l'option `splash=silent`.

* Définir la résolution optimale de l'écran dans le *framebuffer*&nbsp;:
  `Graphical console` > `Console resolution`.

Redémarrer et tester la configuration&nbsp;:

```
# systemctl isolate graphical.target
```

Vérifier si le pilote `nvidia` est bien utilisé&nbsp;:

```
# grep nvidia /var/log/Xorg.0.log
[    36.609] (II) LoadModule: "nvidia"
[    36.609] (II) Loading /usr/lib64/xorg/modules/drivers/nvidia_drv.so
[    36.626] (II) Module nvidia: vendor="NVIDIA Corporation"
...
```

Rebasculer en mode graphique par défaut&nbsp;:

```
# systemctl set-default graphical.target
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>














