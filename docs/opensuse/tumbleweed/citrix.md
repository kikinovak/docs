
# Citrix Workspace sous OpenSUSE Tumbleweed

Citrix Workspace (anciennement Citrix Receiver) constitue la partie client du
Citrix Virtual Desktop. Ce type de configuration est populaire auprès des
cabinets comptables.


## Téléchargement

* Ouvrir [le site de Citrix](https://www.citrix.com).

* Suivre le lien **Produits** > [Télécharger Citrix Workspace
  App](https://www.citrix.com/fr-fr/products/receiver.html).

* Cliquer sur
  [Télécharger](https://www.citrix.com/fr-fr/downloads/workspace-app/).

* Suivre les liens **Workspace app for Linux** > **Citrix Workspace app 2205
  for Linux**.

* Repérer la section **Available Downloads** et suivre les liens **RPM
  Packages** > **SUSE Full Package** > **Citrix Workspace app for Linux
  (x86_64)** > **Download File**.

* Télécharger le fichier `ICAClient-suse-22.5.0.16-0.x86_64.rpm`.

> Lorsqu'on se retrouve confronté à un blocage inexplicable lors de la
> connexion initiale avec un message d'erreur du genre `Citrix received a
> corrupt ICA File`, on peut tenter tout simplement d'installer la précédente
> version de Citrix Workspace.


## Installation

Vérifier si les dépendances du paquet sont satisfaites&nbsp;:

```
# rpm -ivh --test ICAClient-suse-22.5.0.16-0.x86_64.rpm
Verifying...   ################################# [100%]
Preparing...   ################################# [100%]
```

Installer le paquet&nbsp;:

```
# zypper install ICAClient-suse-22.5.0.16-0.x86_64.rpm
```

> Tous les fichiers du paquet sont rangés dans `/opt/Citrix/ICAClient`.


## Tester l'installation

Le répertoire `/opt/Citrix/ICAClient/util` contient un script
`workspacecheck.sh` qui permet de tester les prérequis de l'environnement.

```
# cd /opt/Citrix/ICAClient/util/
# ./workspacecheck.sh
==========================================================
= Pre-requisite check for the Citrix Workspace for Linux =
==========================================================

-------------------------------
-- Client Information...      -
-------------------------------
Client version:
22.5.0.16
Install location:
/opt/Citrix/ICAClient

-------------------------------
-- Checking Kernel Version... -
-------------------------------
Success, a compatible kernel version(5.18.11) has been found.

--------------------------------------------
-- Checking gtk dependencies..   -
--------------------------------------------

Success! - libcanberra-gtk3-module is installed.
Success! - libcanberra-gtk-module or libcanberra-gtk2 is installed.
Success! - libgtk-x11-2.0 is installed.
...
```

> À part quelques avertissements relatifs à des bibliothèques Gstreamer
> obsolètes, tout semble bon a première vue.


## Fournir les certificats nécessaires

Dans la configuration par défaut, Citrix Receiver n'arrive pas à se connecter
parce qu'il ne dispose pas des bons certificats. La meilleure solution consiste
à supprimer en bloc la panoplie de certificats fournis par le paquet en créant
un lien symbolique vers les certificats du système&nbsp;:

```
# cd /opt/Citrix/ICAClient/keystore/
# rm -rf cacerts/
# ln -s /etc/ssl/certs cacerts
```

## L'utilisateur système `citrixlog`

L'installation du paquet `ICAClient` crée un utilisateur système `citrixlog` et un
groupe système du même nom. Le hic, c'est que l'UID et le GID de `citrixlog` se
situent dans la plage des utilisateurs en chair et en os, ce qui est une
aberration. Sans compter le fait que l'utilisateur système dispose d'un *shell*
de connexion `/bin/bash`&nbsp;:

```
# grep citrixlog /etc/group
citrixlog:!:1001:
# grep citrixlog /etc/passwd
citrixlog:x:1001:1001::/var/log/citrix:/bin/bash
```

En conséquence, on verra donc apparaître un nouvel utilisateur `citrixlog` dans
le gestionnaire de connexion SDDM. Pour remédier à cette situation, on peut
tout simplement rectifier le tir comme ceci&nbsp;:

```
# groupmod -g 999 citrixlog
# usermod -u 999 citrixlog
# usermod -s /bin/false citrixlog
# chown -R citrixlog:citrixlog /var/log/citrix
```

## Utilisation

À partir de la, l'application est opérationnelle. Lorsqu'on veut se connecter à
une application distante avec un navigateur web comme Firefox ou Chromium, on
récupère un fichier `.ica` qu'il faudra associer à Citrix Workspace le cas
échéant.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

