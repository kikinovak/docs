
# Vagrant sous OpenSUSE Tumbleweed

[VirtualBox](virtualbox.md) facilite considérablement la tâche lorsqu'on a
besoin d'une ou plusieurs machines virtuelles pour tester une configuration ou
pour des travaux pratiques lors d'une formation. Il n'en demeure pas moins que
l'installation et la configuration post-installation d'une machine virtuelle
reste une activité relativement chronophage. Pour installer une machine
virtuelle basée sur une distribution Linux minimale, il faut à chaque fois
récupérer le fichier ISO de l'installateur, créer et configurer une machine
virtuelle et suivre toutes les étapes du programme d'installation.

C'est là où [Vagrant](https://www.vagrantup.com/) entre en jeu. Vagrant est une
surcouche en ligne de commande à toute une panoplie d'hyperviseurs comme
HyperV, KVM, Parallels, VMWare et VirtualBox. Le grand avantage de Vagrant,
c'est qu'il permet la mise en place d'une machine virtuelle "jetable" en deux
minutes chrono. 

> Dans l'atelier pratique ci-dessous, nous utiliserons Vagrant conjointement
> avec VirtualBox.


## Installation

Vagrant est fourni par les dépôts officiels de la distribution&nbsp;:

```
# zypper install --no-recommends vagrant vagrant-vim vagrant-bash-completion
```


## Premier test

Je ne dispose pas encore de systèmes installables sur ma machine&nbsp;:

```
$ vagrant box list
There are no installed boxes! Use `vagrant box add` to add some.
```

Pour commencer, je vais installer une image d'Alpine Linux, un système léger
dont la taille réduite permet un téléchargement rapide&nbsp;:

```
$ vagrant box add generic/alpine312
==> box: Loading metadata for box 'roboxes/alpine312'
    box: URL: https://vagrantcloud.com/roboxes/alpine312
```

Les images diffèrent selon le système de virtualisation utilisé. Je récupère
l'image pour VirtualBox&nbsp;:

```
This box can work with multiple providers! The providers that it
can work with are listed below. Please review the list and choose
the provider you will be working with.

1) hyperv
2) libvirt
3) parallels
4) virtualbox
5) vmware_desktop

Enter your choice: 4
```

Je crée une arborescence de test dans mon répertoire utilisateur&nbsp;:

```
$ mkdir -pv ~/Vagrant/Alpine
mkdir: création du répertoire '/home/microlinux/Vagrant'
mkdir: création du répertoire '/home/microlinux/Vagrant/Alpine'
$ cd ~/Vagrant/Alpine/
```

J'initialise mon système Alpine Linux&nbsp;:

```
$ vagrant init generic/alpine312
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

La dernière commande a généré un fichier `Vagrantfile` dans mon répertoire
`~/Vagrant/Alpine`. Pour l’instant, ce fichier est constitué majoritairement
d'options commentées. Je peux l'éditer et le simplifier comme ceci&nbsp;:

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/alpine312"
end
```

Je lance ma machine virtuelle&nbsp;:

```
$ vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'generic/alpine312'...
==> default: Matching MAC address for NAT networking...
...
```

Une fois qu'elle a démarré, je peux ouvrir une session SSH comme ceci&nbsp;:

```
$ vagrant ssh
alpine312:~$
```

Je suis bien dans un système Alpine Linux&nbsp;:

```
alpine312:~$ cat /etc/os-release
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.12.12
PRETTY_NAME="Alpine Linux v3.12"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://bugs.alpinelinux.org/"
```

Dans cette machine virtuelle, je suis l'utilisateur `vagrant`&nbsp;:

```
alpine312:~$ whoami
vagrant
```

À partir de là, je peux invoquer `sudo` pour les tâches administratives
courantes&nbsp;:

```
alpine312:~$ sudo apk update
fetch https://mirrors.edge.kernel.org/alpine/...
...
OK: 12763 distinct packages available
```

Je quitte ma machine virtuelle comme n’importe quelle session SSH&nbsp;:

```
alpine312:~$ exit
logout
```

Il ne me reste plus qu’à arrêter la machine virtuelle&nbsp;:

```
$ vagrant halt
==> default: Attempting graceful shutdown of VM...
```

Si je n'ai plus besoin de cette machine, je peux la supprimer&nbsp;:

```
$ vagrant destroy
    default: Are you sure you want to destroy the 'default' VM? [y/N] y
==> default: Destroying VM and associated drives...
```

Et si je n'ai plus du tout l'intention de travailler avec Alpine Linux, je peux
même supprimer l'image correspondante de mon système&nbsp;:

```
$ vagrant box remove generic/alpine312
Removing box 'generic/alpine312' (v4.1.2) with provider 'virtualbox'...
```


## Trois OS en deux minutes

Imaginons que vous ayez besoin de vérifier le fonctionnement d'une commande
sous Red Hat Enterprise Linux&nbsp;8, Rocky Linux&nbsp;8 et Alma Linux&nbsp;8.
Pour commencer, récupérez les images des trois systèmes&nbsp;:

```
$ vagrant box add generic/rhel8
$ vagrant box add generic/rocky8
$ vagrant box add generic/alma8
```

> Ces images fonctionnent avec plusieurs hyperviseurs. Assurez-vous à chaque
> fois de sélectionner `virtualbox`.

Créez un répertoire pour chacun des systèmes virtualisés&nbsp;:

```
$ mkdir -pv ~/Vagrant/{RHEL,Rocky,Alma}
mkdir: création du répertoire '/home/microlinux/Vagrant/RHEL'
mkdir: création du répertoire '/home/microlinux/Vagrant/Rocky'
mkdir: création du répertoire '/home/microlinux/Vagrant/Alma'
```

Initialisez Red Hat Enterprise Linux&nbsp;:

```
$ cd ~/Vagrant/RHEL/
$ vagrant init generic/rhel8
```

Éditez éventuellement le fichier `Vagrantfile` pour le rendre plus
lisible&nbsp;:

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rhel8"
end
```

Procédez de même pour les machines virtuelles sous Rocky Linux et Alma Linux.

Partant de là, lancez chaque machine individuellement, connectez-vous, affichez
le contenu du fichier `/etc/os-release`, déconnectez-vous, arrêtez la machine
virtuelle et supprimez-la lorsque vous n'en avez plus besoin. 


## Trouver des machines

Le [site de Vagrant](https://www.vagrantup.com/) permet de rechercher et
d’essayer un nombre assez conséquent de machines virtuelles. Cliquez sur le
bouton [Find Boxes](https://app.vagrantup.com/boxes/search) sur la page
d’accueil du projet et cherchez le ou les systèmes dont vous avez besoin pour
vos tests.


## Monter un cluster virtuel

Dans l'atelier pratique ci-dessous, nous allons mettre en place trois machines
virtuelles dotées d'un système Rocky Linux&nbsp;8 minimal&nbsp;:

* Machine n° 1&nbsp;: `server-01` / `10.23.45.10`

* Machine n° 2&nbsp;: `server-02` / `10.23.45.20`

* Machine n° 3&nbsp;: `server-03` / `10.23.45.30`

Créez l'arborescence de répertoires pour les machines virtuelles&nbsp;:

```
$ mkdir -pv ~/Vagrant/Cluster/server-{01,02,03}
mkdir: création du répertoire '/home/microlinux/Vagrant/Cluster'
mkdir: création du répertoire '/home/microlinux/Vagrant/Cluster/server-01'
mkdir: création du répertoire '/home/microlinux/Vagrant/Cluster/server-02'
mkdir: création du répertoire '/home/microlinux/Vagrant/Cluster/server-03'
```

Éditez un fichier `/etc/vbox/networks.conf` pour autoriser la création de
réseaux privés virtuels, si ce n'est déjà fait&nbsp;:

```
# /etc/vbox/networks.conf
* 0.0.0.0/0 ::/0
```

Rendez-vous dans le répertoire correspondant à la première machine
virtuelle&nbsp;:

```
$ cd ~/Vagrant/Cluster/server-01/
```

Initialisez la machine virtuelle&nbsp;:

```
$ vagrant init generic/rocky8
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

Cette opération a créé un fichier `Vagrantfile` dans le répertoire courant.
Éditez ce fichier comme suit&nbsp;:

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky8"
  config.vm.hostname = "server-01"
  config.vm.network "private_network",
    ip: "10.23.45.10"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "server-01"
    vb.memory = 512
  end
end
```

> Notez qu'ici j'ai limité la RAM disponible pour la VM à 512 Mo, ce qui est
> largement suffisant pour un serveur Linux sans système graphique. Si vous
> disposez de plus de ressources sur le système hôte, vous pourrez remplacer
> 512 par 1024 ou 2048.

Lancez la machine `server-01`, connectez-vous et éditez le fichier `/etc/hosts`
en tant que `root`&nbsp;:

```
# /etc/hosts
127.0.0.1   localhost.localdomain localhost
10.23.45.10 server-01
10.23.45.20 server-02
10.23.45.30 server-03
```

Déconnectez-vous de la VM, rendez-vous dans le répertoire `Cluster/server-02`
et éditez le fichier `Vagrantfile` correspondant&nbsp;:

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky8"
  config.vm.hostname = "server-02"
  config.vm.network "private_network",
    ip: "10.23.45.20"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "server-02"
    vb.memory = 512
  end
end
```

Lancez la machine `server-02`, connectez-vous et éditez le fichier `/etc/hosts`
en tant que `root`&nbsp;:

```
# /etc/hosts
127.0.0.1   localhost.localdomain localhost
10.23.45.10 server-01
10.23.45.20 server-02
10.23.45.30 server-03
```

Déconnectez-vous de la VM, rendez-vous dans le répertoire `Cluster/server-03`
et éditez le fichier `Vagrantfile` correspondant&nbsp;:

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky8"
  config.vm.hostname = "server-03"
  config.vm.network "private_network",
    ip: "10.23.45.30"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "server-03"
    vb.memory = 512
  end
end
```

Lancez la machine `server-03`, connectez-vous et éditez le fichier `/etc/hosts`
en tant que `root`&nbsp;:

```
# /etc/hosts
127.0.0.1   localhost.localdomain localhost
10.23.45.10 server-01
10.23.45.20 server-02
10.23.45.30 server-03
```

Déconnectez-vous de la VM et affichez la vue d'ensemble&nbsp;:

```
$ vagrant global-status
id      name    provider   state   directory
-----------------------------------------------------------------------------
57d530f default virtualbox running /home/microlinux/Vagrant/Cluster/server-01
cb11736 default virtualbox running /home/microlinux/Vagrant/Cluster/server-02
34d7a55 default virtualbox running /home/microlinux/Vagrant/Cluster/server-03
```

Partant de là, connectez-vous successivement à chacune des machines virtuelles
et testez la connectivité avec les deux autres&nbsp;:

```
[vagrant@server-01 ~]$ hostname
server-01
[vagrant@server-01 ~]$ ping -c 1 server-02
PING server-02 (10.23.45.20) 56(84) bytes of data.
64 bytes from server-02 (10.23.45.20): icmp_seq=1 ttl=64 time=0.714 ms

--- server-02 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.714/0.714/0.714/0.000 ms
[vagrant@server-01 ~]$ ping -c 1 server-03
PING server-03 (10.23.45.30) 56(84) bytes of data.
64 bytes from server-03 (10.23.45.30): icmp_seq=1 ttl=64 time=1.29 ms

--- server-03 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.291/1.291/1.291/0.000 ms
```

Votre cluster local virtualisé est prêt à être utilisé.

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

