
# Mettre à jour OpenSUSE Leap vers Tumbleweed

Méthode testée avec succès sur&nbsp;:

* OpenSUSE Leap 15.2

* OpenSUSE Leap 15.3

* OpenSUSE Leap 15.4

> Ce n'est pas la peine d'effectuer des mises à jour intermédiaires.

Quitter l'environnment graphique&nbsp;:

```
# systemctl set-default multi-user.target
# systemctl isolate multi-user.target
```

Désactiver DeltaRPM&nbsp;:

```
# /etc/zypp/zypp.conf
...
download.use_deltarpm = false
...
```

Installer Git&nbsp;:

```
# zypper install --no-recommends git
```

Récupérer le script de mise à jour&nbsp;:

```
# git clone https://gitlab.com/kikinovak/opensuse
```

Lancer la mise à jour&nbsp;:

```
# cd opensuse
# ./upgrade.sh
```

Redémarrer au terme de la mise à jour&nbsp;:

```
# reboot
```

Tester et lancer l'environnement graphique&nbsp;:

```
# systemctl isolate graphical.target
# systemctl set-default graphical.target
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
