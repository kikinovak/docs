
# Télécharger OpenSUSE Tumbleweed

## Le choix de la distribution

La distribution OpenSUSE est proposée en deux moutures&nbsp;:

* la version stable Leap&nbsp;;

* la version de développement Tumbleweed.

OpenSUSE Tumbleweed est une distribution en publication continue ou *rolling
release*. Elle propose tous les logiciels dans leurs dernières versions
stables. 

> Cette version d'OpenSUSE est étonnamment stable pour une *rolling release*.


## Téléchargement HTTP/HTTPS

* Ouvrir la page de téléchargement
  [download.opensuse.org](https://download.opensuse.org). 

* Suivre successivement les liens
  [tumbleweed](https://download.opensuse.org/tumbleweed/) >
  [iso](https://download.opensuse.org/tumbleweed/iso/).

* Vers le bas de la page, repérer le fichier estampillé de la date, par
  exemple `openSUSE-Tumbleweed-DVD-x86_64-Snapshot20220718-Media.iso`.

* Télécharger l'ISO (4.3 Go).


## Vérifier l'intégrité du téléchargement

* Revenir à la page de téléchargement sur 
  [download.opensuse.org](https://download.opensuse.org/tumbleweed/iso/).

* Télécharger les fichiers `.iso.sha256` et `.iso.sha256.asc` qui
  correspondent à l'ISO.

```
$ ls
openSUSE-Tumbleweed-DVD-x86_64-Snapshot20220718-Media.iso
openSUSE-Tumbleweed-DVD-x86_64-Snapshot20220718-Media.iso.sha256
openSUSE-Tumbleweed-DVD-x86_64-Snapshot20220718-Media.iso.sha256.asc
```

Récupérer la clé GPG du projet OpenSUSE&nbsp;:

```
$ gpg --keyserver pgp.mit.edu --recv-keys \
  0x22C07BA534178CD02EFE22AAB88B2FD43DBDC284
```

Vérifier l'authenticité du fichier de contrôle&nbsp;:

```
$ gpg --verify openSUSE-*.asc openSUSE-*.sha256
gpg: Signature faite le mar. 19 juil. 2022 06:47:06 CEST
gpg: avec la clef RSA B88B2FD43DBDC284
gpg: Bonne signature de « openSUSE Project Signing Key <opensuse@opensuse.org> »
...
```

Vérifier l'intégrité du téléchargement&nbsp;:

```
$ sha256sum -c openSUSE-*.sha256
openSUSE-Tumbleweed-DVD-x86_64-Snapshot20220718-Media.iso: Réussi
```


## Confectionner la clé USB d'installation

Le fichier ISO est hybride. On peut donc directement l'écrire sur la clé&nbsp;:

```
# dd if=openSUSE-Tumbleweed-DVD-x86_64-Snapshot20220718-Media.iso of=/dev/sdX \
  bs=1M status=progress
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>



~                                                                                                                      
~                                                            
