
# Flatpak sous OpenSUSE Tumbleweed

Flatpak est un format de paquets qui permet de s'affranchir des gestionnaires
de paquets traditionnels sous Linux. Il permet typiquement d'installer des
applications de type *usines à gaz* et/ou propriétaires dans un bac à sable
sous `/var/lib/flatpak` sans pour autant spammer le système. L'inconvénient de
Flatpak, c'est son embonpoint en termes d'espace disque, étant donné que chaque
application installe son lot potentiellement redondant de dépendances.

## Installation

Flatpak est fourni par les dépôts officiels de la distribution&nbsp;:

```
# zypper install flatpak
```

## Configuration

Configurer le dépôt Flathub&nbsp;:

```
# flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo
```

> Dans un script on pourra utiliser l'option `--if-not-exists`.

Vérifier si le dépôt a bien été ajouté&nbsp;:

```
# flatpak remotes
  Name    Options
  flathub system
```

## Utilisation
  
Chercher une application Flatpak&nbsp;:

```
# flatpak search spotify
Name     Description           Application ID     Version     Branch  Remotes
Spotify  Online music service  com.spotify.Client 1.1.42.622  stable  flathub
...
```

Installer une application Flatpak&nbsp;:

```
# flatpak install com.spotify.Client
```

> L'entrée de menu s'affichera correctement après le prochain redémarrage.

Afficher la liste de tous les Flatpaks installés&nbsp;:

```
# flatpak list
```

Mettre à jour tous les Flatpaks&nbsp;:

```
# flatpak update
```

Supprimer un Flatpak&nbsp;:

```
# flatpak uninstall com.spotify.Client
```

## Applications

Voici quelques exemples d'applications que l'on installera typiquement dans ce
format&nbsp;:

  * Element (`im.riot.Riot`)

  * GeoGebra (`org.geogebra.GeoGebra`)

  * OmegaT (`org.omegat.OmegaT`)

  * Scratch (`edu.mit.Scratch`)

  * Signal (`org.signal.Signal`)

  * Skype (`com.skype.Client`)

  * Spotify (`com.spotify.Client`)

  * Teams (`com.microsoft.Teams`)

  * Zoom (`us.zoom.Zoom`)

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

