
# Configurer OpenSUSE Tumbleweed KDE

Installer Git&nbsp;:

```
# zypper install --no-recommends git
```

Récupérer le script de configuration automatique&nbsp;:

```
# cd
# git clone https://gitlab.com/kikinovak/opensuse
# cd opensuse
```

Configurer Bash, Vim et Xterm&nbsp;:
```
# ./setup.sh --shell
```

Configurer les dépôts de paquets officiels et tiers&nbsp;:

```
# ./setup.sh --repos
```

Mettre à jour le système avec des paquets améliorés&nbsp;:

```
# ./setup.sh --fresh
```

Supprimer les applications inutiles&nbsp;:

```
# ./setup.sh --strip
```

Installer les applications manquantes&nbsp;:

```
# ./setup.sh --extra
```

Installer les polices Microsoft et Eurostile&nbsp;:

```
# ./setup.sh --fonts
```

Personnaliser les entrées de menu&nbsp;:

```
# ./setup.sh --menus
```

Installer le profil par défaut de KDE&nbsp;:

```
# ./setup.sh --kderc
```

Appliquer le profil par défaut aux utilisateurs existants&nbsp;:

```
# ./setup.sh --users
```

Exécuter d'une traite toutes les opérations ci-dessus&nbsp;:

```
# ./setup.sh --setup
```

Installer les applications qui nécessitent un lecteur optique&nbsp;:

```
# ./setup.sh --dvdrw
```

Afficher l'aide sur les options&nbsp;:

```
# ./setup.sh --help
```

Afficher le détail des opérations dans un deuxième terminal&nbsp;:

```
# tail -f /var/log/setup.log
```

---

<p align="center"><small><em>Documentation rédigée par un <a
href="https://tinyurl.com/2p8tk495">informaticien heureux élevé en plein
air</a>.<br /> Offrez-lui un café en cliquant sur la tasse.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
