====
TODO
====


PBS 1.1
-------

L'utilisation initial doit pouvoir utiliser la commande sudo.


Oracle Linux 7
--------------

Manipuler les bases MySQL : sauvegarde, restauration, etc.


Articles du blog technique
--------------------------

* Vérifier la santé matérielle d'un disque dur avec les tests SMART

* Utiliser ProtonVPN sous OpenSUSE Leap 15.2

* Mettre en place un système de visioconférence avec BigBlueButton

* La traduction sous Linux avec OmegaT

* Vagrant et les Guest Additions de VirtualBox

* Prise en main de GitLab

* OpenSUSE Leap 15.2 et VirtualBox

* La supervision avec Icinga par la pratique (1) – Installation

* La supervision avec Icinga par la pratique (2) – Prise en main

* Formater un disque dur externe en 2020

* Débuter avec Vagrant sous OpenSUSE Leap

* Installation d’un poste de travail OpenSUSE Leap 15.2 KDE

* Configuration automatique d’un poste de travail OpenSUSE Leap 15.2 KDE

* Configurer un cache de paquets local pour OpenSUSE sous CentOS 7

* Monter son serveur mail de A à Z sous CentOS 7

* Filtrer les spams avec Postfix et les RBL

* Filtrer les spams avec SpamAssassin sous CentOS 7

* Surveiller les logs de Postfix sous CentOS 7

* Héberger un webmail simple avec Roundcube sous CentOS 7

* Postfix, SPF, DKIM et DMARC sous CentOS 7

* Sécuriser les connexions à Postfix et Dovecot sous CentOS 7

* Serveur IMAP avec Dovecot sous CentOS 7

* Serveur mail multi-domaines avec Postfix sous CentOS 7

* Connecter un poste client Windows 10 à un serveur Samba

* Installer un serveur Samba sous CentOS 7

* Installer CentOS 7 sur une carte PC Engines

* Filtrer le web avec SquidGuard sous CentOS 7

* Surveiller le trafic web avec SquidAnalyzer sous CentOS 7

* Squid et les exceptions pour les sites problématiques

* Gérer les connexions HTTPS avec Squid sous CentOS 7

* Serveur proxy cache HTTP avec Squid sous CentOS 7

* Authentification centralisée sous CentOS 7 et OpenSUSE Leap 15.1

* Connecter un poste client OpenSUSE Leap 15.1 à 389 Directory Server

* Sécuriser 389 Directory Server sous CentOS 7

* Rétrograder un paquet sous CentOS

* Gérer les utilisateurs avec 389 Directory Server sous CentOS 7

* Installer 389 Directory Server sous CentOS 7

* Serveur IRC avec NgIRCd et LetsEncrypt sous CentOS 7

* Configurer une imprimante/scanner Epson XP-215 sous OpenSUSE Leap 15.1

* Créer une autorité de certification TLS avec Easy-RSA

* Installer X11 et WindowMaker sous CentOS 7

* Configurer un client NIS sous OpenSUSE Leap

* Installer et configurer un serveur NIS sous CentOS 7

* Configurer un client NFS sous OpenSUSE Leap 

* Installer et configurer un serveur NFS sous CentOS 7

* Tester les performances de son réseau avec iPerf 

* Serveur DHCP + DNS local avec Dnsmasq sous CentOS 7 

* Installer OpenSUSE Leap 15.1 sur un portable ASUS Republic Of Gamers G553V 

* Tester les performances de sa carte graphique sous Linux

* Hébergement GEPI sous CentOS 7

* Installer Anaconda sous OpenSUSE Leap 15.1

* Hébergement Dolibarr sous CentOS 7

* Serveur de sauvegardes avec Rsnapshot sous CentOS 7

* Serveur FTP avec VsFTPd et Let’s Encrypt sous CentOS 7

* Gérer les mises à jour WordPress avec WP-CLI

* Hébergement WordPress sous CentOS 7

* Android « aux petits oignons »

* Gérer plus confortablement les certificats SSL/TLS Let’s Encrypt

* Certificats SSL/TLS avec Certbot sous CentOS 7

* Utiliser le système Live USB Slax 9.11

* OpenSUSE Leap et les cartes NVidia

* Migrer un site WordPress

* Flasher le BIOS d’un PC sans OS

* Refaire une jeunesse à un très vieux PC avec Q4OS

* Installer Citrix Receiver sous OpenSUSE Leap 15.0

* OpenSUSE Leap en configuration RAID 1


Documentation CherryTree
------------------------

* OpenSUSE Leap 15.2 : Flatpak

* OpenSUSE Leap 15.2 : Imprimante/scanner HP

* Oracle Linux 8 : Installer un serveur local

* Oracle Linux 8 : Configuration automatique

