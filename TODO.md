# TODO

On prend les catégories de server-world.info.

Linux
-----

Sécurité :

* [ ] Authentification par clé SSH

* [ ] Copier sa paire de clés GPG

Rocky Linux 8
-------------

Installation & Configuration :

* [ ] Utiliser la console de secours

* [ ] Chroot depuis un système de secours

* [ ] Clonage réseau avec dd et ssh

* [ ] Réinitialiser les disques

* [ ] Installer un serveur local

* [ ] Partitionner un disque 

* [ ] RAID 1 avec deux disques

* [ ] RAID 6 avec quatre disques

* [ ] Installer un serveur dédié

* [ ] Configuration automatique

* [ ] NetworkManager sur un serveur simple

* [ ] NetworkManager sur un routeur 

* [ ] NetworkManager sur un serveur dédié

Client/serveur NTP :

* [ ] Synchronisation NTP avec Chrony

Sécurité : 

* [ ] Activer SELinux sur un serveur dédié

* [ ] FirewallD sur un serveur local

* [ ] FirewallD sur un routeur

* [ ] FirewallD sur un serveur dédié

* [ ] Protéger SSH avec Fail2ban

* [ ] Protéger Postfix avec Fail2ban

* [ ] Mises à jour automatiques

Serveur mail :

* [ ] Postfix minimal sur un serveur local

* [ ] Postfix minimal sur un serveur dédié

Serveur DNS :

* [ ] Serveur DNS primaire avec BIND

Bases de données :

* [ ] Serveur MariaDB

Serveur web :

* [ ] Installer Apache et SSL

* [ ] Installer PHP

* [ ] Mettre à jour PHP

